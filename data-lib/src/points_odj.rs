use crate::commun::{CycleDeVie, EmptyChoice};
use crate::contexts::Context;
use crate::dossiers_legislatifs::DossierParlementaire;
use crate::serde_utils::{map_to_vec, str_to_bool, str_to_vec};

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct DossiersLegislatifsRefs {
    #[serde(deserialize_with = "str_to_vec", rename = "dossierRef")]
    pub dossiers_refs: Vec<String>,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct PodjReunion {
    #[serde(deserialize_with = "str_to_bool", rename = "comiteSecret")]
    pub comite_secret: bool,
    #[serde(rename = "cycleDeVie")]
    cycle_de_vie: CycleDeVie,
    #[serde(rename = "demandeurPoint")]
    pub demandeur_point: Option<EmptyChoice>,
    #[serde(rename = "dossiersLegislatifsRefs")]
    pub dossiers_legislatifs_refs: Option<DossiersLegislatifsRefs>,
    pub objet: String,
    pub procedure: Option<String>,
    #[serde(rename = "textesAssocies")]
    pub textes_associes: Option<EmptyChoice>,
    #[serde(rename = "typePointODJ")]
    type_point_odj: String,
    pub uid: String,
}

graphql_object!(PodjReunion: Context |&self| {
    field comite_secret() -> bool {
        self.comite_secret
    }

    field cycle_de_vie() -> &CycleDeVie {
        &self.cycle_de_vie
    }

    field dossiers_legislatifs(&executor) -> Vec<&DossierParlementaire> {
        match &self.dossiers_legislatifs_refs {
            None => vec![],
            Some(ref dossiers_legislatifs_refs) => {
                let context = &executor.context();
                dossiers_legislatifs_refs.dossiers_refs
                    .iter()
                    .filter_map(|ref dossier_ref| context.get_dossier_parlementaire_at_uid(&dossier_ref))
                    .collect()
            },
        }
    }

    field dossiers_legislatifs_refs() -> Vec<&str> {
        match &self.dossiers_legislatifs_refs {
            None => vec![],
            Some(ref dossiers_legislatifs_refs) => {
                dossiers_legislatifs_refs.dossiers_refs
                    .iter()
                    .map(|ref dossier_ref| dossier_ref.as_str())
                    .collect()
            },
        }
    }

    field objet() -> &str {
        &self.objet
    }

    field procedure() -> Option<&str> {
        match &self.procedure {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field type_point_odj() -> &str {
        &self.type_point_odj
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct PodjSeanceConfPres {
    #[serde(deserialize_with = "str_to_bool", rename = "comiteSecret")]
    pub comite_secret: bool,
    #[serde(rename = "cycleDeVie")]
    cycle_de_vie: CycleDeVie,
    #[serde(rename = "dateConfPres")]
    // TODO: Date<FixedOffset> type is not able to deserialize dates with timezone (`YYYY-MM-DD±HH:MM`).
    pub date_conf_pres: Option<String>,
    #[serde(rename = "dateLettreMinistre")]
    // TODO: Date<FixedOffset> type is not able to deserialize dates with timezone (`YYYY-MM-DD±HH:MM`).
    pub date_lettre_ministre: Option<String>,
    #[serde(rename = "demandeurPoint")]
    pub demandeur_point: Option<EmptyChoice>,
    #[serde(rename = "dossiersLegislatifsRefs")]
    pub dossiers_legislatifs_refs: Option<DossiersLegislatifsRefs>,
    #[serde(rename = "natureTravauxODJ")]
    nature_travaux_odj: String,
    pub objet: String,
    pub procedure: Option<String>,
    #[serde(rename = "textesAssocies")]
    pub textes_associes: Option<EmptyChoice>,
    #[serde(rename = "typePointODJ")]
    type_point_odj: String,
    pub uid: String,
}

graphql_object!(PodjSeanceConfPres: Context |&self| {
    field comite_secret() -> bool {
        self.comite_secret
    }

    field cycle_de_vie() -> &CycleDeVie {
        &self.cycle_de_vie
    }

    field date_conf_pres() -> Option<&str> {
        match &self.date_conf_pres {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field date_lettre_ministre() -> Option<&str> {
        match &self.date_lettre_ministre {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field dossiers_legislatifs(&executor) -> Vec<&DossierParlementaire> {
        match &self.dossiers_legislatifs_refs {
            None => vec![],
            Some(ref dossiers_legislatifs_refs) => {
                let context = &executor.context();
                dossiers_legislatifs_refs.dossiers_refs
                    .iter()
                    .filter_map(|ref dossier_ref| context.get_dossier_parlementaire_at_uid(&dossier_ref))
                    .collect()
            },
        }
    }

    field dossiers_legislatifs_refs() -> Vec<&str> {
        match &self.dossiers_legislatifs_refs {
            None => vec![],
            Some(ref dossiers_legislatifs_refs) => {
                dossiers_legislatifs_refs.dossiers_refs
                    .iter()
                    .map(|ref dossier_ref| dossier_ref.as_str())
                    .collect()
            },
        }
    }

    field nature_travaux_odj() -> &str {
        &self.nature_travaux_odj
    }

    field objet() -> &str {
        &self.objet
    }

    field procedure() -> Option<&str> {
        match &self.procedure {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field type_point_odj() -> &str {
        &self.type_point_odj
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Clone, Debug, Deserialize)]
#[serde(tag = "@xsi:type")]
pub enum PointOdj {
    #[serde(rename = "podjReunion_type")]
    PodjReunion(PodjReunion),
    #[serde(rename = "podjSeanceConfPres_type")]
    PodjSeanceConfPres(PodjSeanceConfPres),
}

impl PointOdj {
    //     pub fn comite_secret(&self) -> bool {
    //         match *self {
    //             PointOdj::PodjReunion(PodjReunion { comite_secret, .. })
    //             | PointOdj::PodjSeanceConfPres(PodjSeanceConfPres { comite_secret, .. }) => {
    //                 comite_secret
    //             }
    //         }
    //     }

    //     pub fn cycle_de_vie(&self) -> &CycleDeVie {
    //         match *self {
    //             PointOdj::PodjReunion(PodjReunion {
    //                 ref cycle_de_vie, ..
    //             })
    //             | PointOdj::PodjSeanceConfPres(PodjSeanceConfPres {
    //                 ref cycle_de_vie, ..
    //             }) => &cycle_de_vie,
    //         }
    //     }

    pub fn dossiers_legislatifs_refs(&self) -> Option<&DossiersLegislatifsRefs> {
        match *self {
            PointOdj::PodjReunion(PodjReunion {
                ref dossiers_legislatifs_refs,
                ..
            })
            | PointOdj::PodjSeanceConfPres(PodjSeanceConfPres {
                ref dossiers_legislatifs_refs,
                ..
            }) => match dossiers_legislatifs_refs {
                None => None,
                Some(dossiers_legislatifs_refs) => Some(&dossiers_legislatifs_refs),
            },
        }
    }

    //     pub fn objet(&self) -> &str {
    //         match *self {
    //             PointOdj::PodjReunion(PodjReunion { ref objet, .. })
    //             | PointOdj::PodjSeanceConfPres(PodjSeanceConfPres { ref objet, .. }) => &objet,
    //         }
    //     }

    //     pub fn procedure(&self) -> Option<&str> {
    //         match *self {
    //             PointOdj::PodjReunion(PodjReunion { ref procedure, .. })
    //             | PointOdj::PodjSeanceConfPres(PodjSeanceConfPres { ref procedure, .. }) => {
    //                 match &procedure {
    //                     None => None,
    //                     Some(ref procedure) => Some(&procedure),
    //                 }
    //             }
    //         }
    //     }

    //     pub fn type_point_odj(&self) -> &str {
    //         match *self {
    //             PointOdj::PodjReunion(PodjReunion {
    //                 ref type_point_odj, ..
    //             })
    //             | PointOdj::PodjSeanceConfPres(PodjSeanceConfPres {
    //                 ref type_point_odj, ..
    //             }) => &type_point_odj,
    //         }
    //     }

    pub fn uid(&self) -> &str {
        match *self {
            PointOdj::PodjReunion(PodjReunion { ref uid, .. })
            | PointOdj::PodjSeanceConfPres(PodjSeanceConfPres { ref uid, .. }) => &uid,
        }
    }
}

// Using graphql_union because instance_resolvers of graphql_interface does not work with Juniper 0.9.2.
// graphql_interface!(PointOdj: Context |&self| {
graphql_union!(PointOdj: Context |&self| {
    // field comite_secret() -> bool {
    //     self.comite_secret()
    // }

    // field cycle_de_vie() -> &CycleDeVie {
    //     self.cycle_de_vie()
    // }

    // field dossiers_legislatifs_refs() -> Option<&DossiersLegislatifsRefs> {
    //     self.dossiers_legislatifs_refs()
    // }

    // field objet() -> &str {
    //     self.objet()
    // }

    // field procedure() -> Option<&str> {
    //     self.procedure()
    // }

    // field type_point_odj() -> &str {
    //     self.type_point_odj()
    // }

    // field uid() -> &str {
    //     self.uid()
    // }

    instance_resolvers: |_| {
        &PodjReunion => match *self {
            PointOdj::PodjReunion(ref point_odj) => Some(point_odj),
            _ => None,
        },
        &PodjSeanceConfPres => match *self {
            PointOdj::PodjSeanceConfPres(ref point_odj) => Some(point_odj),
            _ => None,
        },
    }
});

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct PointsOdj {
    #[serde(deserialize_with = "map_to_vec", rename = "pointODJ")]
    pub points_odj: Vec<PointOdj>,
}
