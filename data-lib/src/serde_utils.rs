use serde::de::{self, Deserialize, Deserializer, MapAccess, SeqAccess, Visitor};
use std::fmt;
use std::marker::PhantomData;

pub fn map_to_vec<'de, D, T>(deserializer: D) -> Result<Vec<T>, D::Error>
where
    D: Deserializer<'de>,
    T: Deserialize<'de>,
{
    struct MapToVec<T>(PhantomData<T>);

    impl<'de, T> Visitor<'de> for MapToVec<T>
    where
        T: Deserialize<'de>,
    {
        type Value = Vec<T>;

        fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
            formatter.write_str("map or sequence of maps")
        }

        fn visit_seq<S>(self, mut seq: S) -> Result<Self::Value, S::Error>
        where
            S: SeqAccess<'de>,
        {
            // Loop over array elements
            let mut vec = Vec::new();
            while let Some(element) = seq.next_element()? {
                vec.push(element);
            }
            Ok(vec)
        }

        fn visit_map<M>(self, visitor: M) -> Result<Self::Value, M::Error>
        where
            M: MapAccess<'de>,
        {
            // Convert map to a vec containing only map.
            // `MapAccessDeserializer` is a wrapper that turns a `MapAccess`
            // into a `Deserializer`, allowing it to be used as the input to T's
            // `Deserialize` implementation. T then deserializes itself using
            // the entries from the map visitor.
            Deserialize::deserialize(serde::de::value::MapAccessDeserializer::new(visitor))
                .map(|map| vec![map])
        }
    }

    deserializer.deserialize_any(MapToVec(PhantomData))
}

pub fn str_to_bool<'de, D>(deserializer: D) -> Result<bool, D::Error>
where
    D: Deserializer<'de>,
{
    struct StrToBool(PhantomData<bool>);

    impl<'de> de::Visitor<'de> for StrToBool {
        type Value = bool;

        fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
            formatter.write_str("string containing either \"false\" or \"true\"")
        }

        fn visit_str<E>(self, value: &str) -> Result<Self::Value, E>
        where
            E: de::Error,
        {
            match value {
                "false" | "0" => Ok(false),
                "true" | "1" => Ok(true),
                _ => Err(E::custom(format!(
                    "Expected a string containing either \"false\" or \"true\", got: \"{}\".",
                    value
                ))),
            }
        }
    }

    deserializer.deserialize_any(StrToBool(PhantomData))
}

pub fn str_to_option_bool<'de, D>(deserializer: D) -> Result<Option<bool>, D::Error>
where
    D: Deserializer<'de>,
{
    struct StrToOptionBool(PhantomData<Option<bool>>);

    impl<'de> de::Visitor<'de> for StrToOptionBool {
        type Value = Option<bool>;

        fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
            formatter.write_str("optional string containing either \"false\" or \"true\"")
        }

        fn visit_none<E>(self) -> Result<Self::Value, E>
        where
            E: de::Error,
        {
            Ok(None)
        }

        fn visit_str<E>(self, value: &str) -> Result<Self::Value, E>
        where
            E: de::Error,
        {
            match value {
                "" => Ok(None),
                "false" => Ok(Some(false)),
                "true" => Ok(Some(true)),
                _ => Err(E::custom(format!(
                    "Expected an optional string containing either \"false\" or \"true\", got: \"{}\".",
                    value
                ))),
            }
        }
    }

    deserializer.deserialize_any(StrToOptionBool(PhantomData))
}

/// Cf https://stackoverflow.com/questions/41151080/deserialize-a-json-string-or-array-of-strings-into-a-vec
pub fn str_to_vec<'de, D>(deserializer: D) -> Result<Vec<String>, D::Error>
where
    D: Deserializer<'de>,
{
    struct StrToVec(PhantomData<Vec<String>>);

    impl<'de> de::Visitor<'de> for StrToVec {
        type Value = Vec<String>;

        fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
            formatter.write_str("string or sequence of strings")
        }

        fn visit_seq<S>(self, visitor: S) -> Result<Self::Value, S::Error>
        where
            S: de::SeqAccess<'de>,
        {
            Deserialize::deserialize(de::value::SeqAccessDeserializer::new(visitor))
        }

        fn visit_str<E>(self, value: &str) -> Result<Self::Value, E>
        where
            E: de::Error,
        {
            Ok(vec![value.to_owned()])
        }
    }

    deserializer.deserialize_any(StrToVec(PhantomData))
}
