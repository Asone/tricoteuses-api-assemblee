use crate::acteurs::Acteur;
use crate::commun::EmptyChoice;
use crate::contexts::Context;
use crate::organes::Organe;
use crate::serde_utils::{map_to_vec, str_to_bool, str_to_vec};
use crate::types_organes::CodeTypeOrgane;

#[derive(Clone, Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct Collaborateur {
    #[serde(rename = "dateDebut")]
    pub date_debut: Option<EmptyChoice>,
    #[serde(rename = "dateFin")]
    pub date_fin: Option<EmptyChoice>,
    nom: String,
    prenom: String,
    qualite: String,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Collaborateurs {
    #[serde(deserialize_with = "map_to_vec", rename = "collaborateur")]
    pub collaborateurs: Vec<Collaborateur>,
}

#[derive(Clone, Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct Election {
    #[serde(rename = "causeMandat")]
    pub cause_mandat: Option<String>,
    pub lieu: LieuElection,
    #[serde(rename = "refCirconscription")]
    pub ref_circonscription: Option<String>,
}

#[derive(Clone, Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct InfosHorsSian {
    #[serde(rename = "HATVP_URI")]
    pub hatvp_uri: Option<EmptyChoice>,
}

#[derive(Clone, Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct InfosQualite {
    #[serde(rename = "codeQualite")]
    pub code_qualite: Option<String>,
    #[serde(rename = "libQualite")]
    pub lib_qualite: String,
    #[serde(rename = "libQualiteSex")]
    pub lib_qualite_sex: Option<String>,
}

#[derive(Clone, Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct LieuElection {
    pub departement: Option<String>,
    #[serde(rename = "numCirco")]
    pub num_circo: Option<String>, // TODO: Convert to u8.
    #[serde(rename = "numDepartement")]
    pub num_departement: Option<String>,
    pub region: Option<String>,
    #[serde(rename = "regionType")]
    pub region_type: Option<String>,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(tag = "@xsi:type")]
pub enum Mandat {
    #[serde(rename = "MandatAvecSuppleant_Type")]
    MandatAvecSuppleant(MandatAvecSuppleant),
    #[serde(rename = "MandatMission_Type")]
    MandatMission(MandatMission),
    #[serde(rename = "MandatParlementaire_type")]
    MandatParlementaire(MandatParlementaire),
    #[serde(rename = "MandatSimple_Type")]
    MandatSimple(MandatSimple),
}

impl Mandat {
    //     pub fn acteur_ref(&self) -> &str {
    //         match *self {
    //             Mandat::MandatAvecSuppleant(MandatAvecSuppleant { ref acteur_ref, .. })
    //             | Mandat::MandatMission(MandatMission { ref acteur_ref, .. })
    //             | Mandat::MandatParlementaire(MandatParlementaire { ref acteur_ref, .. })
    //             | Mandat::MandatSimple(MandatSimple { ref acteur_ref, .. }) => &acteur_ref,
    //         }
    //     }

    pub fn date_debut(&self) -> &str {
        match *self {
            Mandat::MandatAvecSuppleant(MandatAvecSuppleant { ref date_debut, .. })
            | Mandat::MandatMission(MandatMission { ref date_debut, .. })
            | Mandat::MandatParlementaire(MandatParlementaire { ref date_debut, .. })
            | Mandat::MandatSimple(MandatSimple { ref date_debut, .. }) => &date_debut,
        }
    }

    pub fn date_fin(&self) -> Option<&str> {
        match *self {
            Mandat::MandatAvecSuppleant(MandatAvecSuppleant { ref date_fin, .. })
            | Mandat::MandatMission(MandatMission { ref date_fin, .. })
            | Mandat::MandatParlementaire(MandatParlementaire { ref date_fin, .. })
            | Mandat::MandatSimple(MandatSimple { ref date_fin, .. }) => match &date_fin {
                None => None,
                Some(ref date_fin) => Some(&date_fin),
            },
        }
    }

    //     pub fn date_publication(&self) -> Option<&str> {
    //         match *self {
    //             Mandat::MandatAvecSuppleant(MandatAvecSuppleant {
    //                 ref date_publication,
    //                 ..
    //             })
    //             | Mandat::MandatMission(MandatMission {
    //                 ref date_publication,
    //                 ..
    //             })
    //             | Mandat::MandatParlementaire(MandatParlementaire {
    //                 ref date_publication,
    //                 ..
    //             })
    //             | Mandat::MandatSimple(MandatSimple {
    //                 ref date_publication,
    //                 ..
    //             }) => match &date_publication {
    //                 None => None,
    //                 Some(ref date_publication) => Some(&date_publication),
    //             },
    //         }
    //     }

    //     pub fn infos_qualite(&self) -> &InfosQualite {
    //         match *self {
    //             Mandat::MandatAvecSuppleant(MandatAvecSuppleant {
    //                 ref infos_qualite, ..
    //             })
    //             | Mandat::MandatMission(MandatMission {
    //                 ref infos_qualite, ..
    //             })
    //             | Mandat::MandatParlementaire(MandatParlementaire {
    //                 ref infos_qualite, ..
    //             })
    //             | Mandat::MandatSimple(MandatSimple {
    //                 ref infos_qualite, ..
    //             }) => &infos_qualite,
    //         }
    //     }

    //     pub fn legislature(&self) -> Option<&str> {
    //         match *self {
    //             Mandat::MandatAvecSuppleant(MandatAvecSuppleant {
    //                 ref legislature, ..
    //             })
    //             | Mandat::MandatMission(MandatMission {
    //                 ref legislature, ..
    //             })
    //             | Mandat::MandatParlementaire(MandatParlementaire {
    //                 ref legislature, ..
    //             })
    //             | Mandat::MandatSimple(MandatSimple {
    //                 ref legislature, ..
    //             }) => match &legislature {
    //                 None => None,
    //                 Some(ref legislature) => Some(&legislature),
    //             },
    //         }
    //     }

    //     pub fn nomin_principale(&self) -> bool {
    //         match *self {
    //             Mandat::MandatAvecSuppleant(MandatAvecSuppleant {
    //                 nomin_principale,
    //                 ..
    //             })
    //             | Mandat::MandatMission(MandatMission {
    //                 nomin_principale,
    //                 ..
    //             })
    //             | Mandat::MandatParlementaire(MandatParlementaire {
    //                 nomin_principale,
    //                 ..
    //             })
    //             | Mandat::MandatSimple(MandatSimple {
    //                 nomin_principale,
    //                 ..
    //             }) => nomin_principale,
    //         }
    //     }

    pub fn organes<'a>(&self, context: &'a Context) -> Vec<&'a Organe> {
        match *self {
            Mandat::MandatAvecSuppleant(ref mandat) => mandat.organes(context),
            Mandat::MandatMission(ref mandat) => mandat.organes(context),
            Mandat::MandatParlementaire(ref mandat) => mandat.organes(context),
            Mandat::MandatSimple(ref mandat) => mandat.organes(context),
        }
    }

    //     pub fn preseance(&self) -> &Option<String> {
    //         match *self {
    //             Mandat::MandatAvecSuppleant(MandatAvecSuppleant { ref preseance, .. })
    //             | Mandat::MandatMission(MandatMission { ref preseance, .. })
    //             | Mandat::MandatParlementaire(MandatParlementaire { ref preseance, .. })
    //             | Mandat::MandatSimple(MandatSimple { ref preseance, .. }) => &preseance,
    //         }
    //     }

    pub fn type_organe(&self) -> CodeTypeOrgane {
        match *self {
            Mandat::MandatAvecSuppleant(MandatAvecSuppleant { type_organe, .. })
            | Mandat::MandatMission(MandatMission { type_organe, .. })
            | Mandat::MandatParlementaire(MandatParlementaire { type_organe, .. })
            | Mandat::MandatSimple(MandatSimple { type_organe, .. }) => type_organe,
        }
    }

    pub fn uid(&self) -> &str {
        match *self {
            Mandat::MandatAvecSuppleant(MandatAvecSuppleant { ref uid, .. })
            | Mandat::MandatMission(MandatMission { ref uid, .. })
            | Mandat::MandatParlementaire(MandatParlementaire { ref uid, .. })
            | Mandat::MandatSimple(MandatSimple { ref uid, .. }) => &uid,
        }
    }
}

// Using graphql_union because instance_resolvers of graphql_interface does not work with Juniper 0.9.2.
// graphql_interface!(Mandat: () |&self| {
graphql_union!(Mandat: Context |&self| {
    // field acteur(&executor) -> Option<&Acteur> {
    //     self.acteur(&executor.context())
    // }

    // field acteur_ref() -> &str {
    //     self.acteur_ref()
    // }

    // field date_debut() -> &str {
    //     self.date_debut()
    // }

    // field date_fin() -> Option<&str> {
    //     self.date_fin()
    // }

    // field date_publication() -> Option<&str> {
    //     self.date_publication()
    // }

    // field infos_qualite() -> &InfosQualite {
    //     self.infos_qualite()
    // }

    // field legislature() -> Option<&str> {
    //     self.legislature()
    // }

    // field nomin_principale() -> bool {
    //     self.nomin_principale()
    // }

    // field organes() -> &Vec<Organe> {
    //     self.organes()
    // }

    // field preseance() -> &Option<String> {
    //     self.preseance()
    // }

    // field type_organe() -> CodeTypeOrgane {
    //     self.type_organe()
    // }

    // field uid() -> &str {
    //     self.uid()
    // }

    instance_resolvers: |_| {
        &MandatAvecSuppleant => match *self {
            Mandat::MandatAvecSuppleant(ref mandat) => Some(mandat),
            _ => None,
        },
        &MandatMission => match *self {
            Mandat::MandatMission(ref mandat) => Some(mandat),
            _ => None,
        },
        &MandatParlementaire => match *self {
            Mandat::MandatParlementaire(ref mandat) => Some(mandat),
            _ => None,
        },
        &MandatSimple => match *self {
            Mandat::MandatSimple(ref mandat) => Some(mandat),
            _ => None,
        },
    }
});

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct MandatAvecSuppleant {
    #[serde(rename = "acteurRef")]
    pub acteur_ref: String,
    #[serde(rename = "dateDebut")]
    // TODO: Date<FixedOffset> type is not able to deserialize dates (`YYYY-MM-DD`).
    pub date_debut: String,
    #[serde(rename = "dateFin")]
    // TODO: Date<FixedOffset> type is not able to deserialize dates (`YYYY-MM-DD`).
    pub date_fin: Option<String>,
    #[serde(rename = "datePublication")]
    // TODO: Date<FixedOffset> type is not able to deserialize dates (`YYYY-MM-DD`).
    pub date_publication: Option<String>,
    #[serde(rename = "infosQualite")]
    pub infos_qualite: InfosQualite,
    pub legislature: Option<String>,
    #[serde(deserialize_with = "str_to_bool", rename = "nominPrincipale")]
    pub nomin_principale: bool,
    organes: OrganesMandat,
    pub preseance: Option<String>,
    suppleants: Option<Suppleants>,
    #[serde(rename = "typeOrgane")]
    pub type_organe: CodeTypeOrgane,
    pub uid: String,
}

impl MandatAvecSuppleant {
    pub fn suppleant(&self) -> Option<&Suppleant> {
        match &self.suppleants {
            None => None,
            Some(ref suppleants) => Some(&suppleants.suppleant),
        }
    }
}

impl MandatTrait for MandatAvecSuppleant {
    fn acteur_ref(&self) -> &str {
        &self.acteur_ref
    }

    fn organes_refs(&self) -> &Vec<String> {
        &self.organes.organes_refs
    }
}

graphql_object!(MandatAvecSuppleant: Context |&self| {
    field acteur(&executor) -> Option<&Acteur> {
        self.acteur(&executor.context())
    }

    field acteur_ref() -> &str {
        &self.acteur_ref
    }

    field date_debut() -> &str {
        &self.date_debut
    }

    field date_fin() -> &Option<String> {
        &self.date_fin
    }

    field date_publication() -> &Option<String> {
        &self.date_publication
    }

    field infos_qualite() -> &InfosQualite {
        &self.infos_qualite
    }

    field legislature() -> &Option<String> {
        &self.legislature
    }

    field nomin_principale() -> bool {
        self.nomin_principale
    }

    field organes(&executor) -> Vec<&Organe> {
        self.organes(&executor.context())
    }

    field organes_refs() -> &Vec<String> {
        self.organes_refs()
    }

    field preseance() -> &Option<String> {
        &self.preseance
    }

    field type_organe() -> CodeTypeOrgane {
        self.type_organe
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct MandatMission {
    #[serde(rename = "acteurRef")]
    pub acteur_ref: String,
    #[serde(rename = "dateDebut")]
    // TODO: Date<FixedOffset> type is not able to deserialize dates (`YYYY-MM-DD`).
    pub date_debut: String,
    #[serde(rename = "dateFin")]
    // TODO: Date<FixedOffset> type is not able to deserialize dates (`YYYY-MM-DD`).
    pub date_fin: Option<String>,
    #[serde(rename = "datePublication")]
    // TODO: Date<FixedOffset> type is not able to deserialize dates (`YYYY-MM-DD`).
    pub date_publication: Option<String>,
    #[serde(rename = "infosQualite")]
    pub infos_qualite: InfosQualite,
    pub legislature: Option<String>,
    pub libelle: Option<String>,
    #[serde(rename = "missionPrecedenteRef")]
    pub mission_precedente_ref: Option<String>,
    #[serde(rename = "missionSuivanteRef")]
    pub mission_suivante_ref: Option<String>,
    #[serde(deserialize_with = "str_to_bool", rename = "nominPrincipale")]
    pub nomin_principale: bool,
    organes: OrganesMandat,
    pub preseance: Option<String>,
    #[serde(rename = "typeOrgane")]
    pub type_organe: CodeTypeOrgane,
    pub uid: String,
}

impl MandatTrait for MandatMission {
    fn acteur_ref(&self) -> &str {
        &self.acteur_ref
    }

    fn organes_refs(&self) -> &Vec<String> {
        &self.organes.organes_refs
    }
}

graphql_object!(MandatMission: Context |&self| {
    field acteur(&executor) -> Option<&Acteur> {
        self.acteur(&executor.context())
    }

    field acteur_ref() -> &str {
        &self.acteur_ref
    }

    field date_debut() -> &str {
        &self.date_debut
    }

    field date_fin() -> &Option<String> {
        &self.date_fin
    }

    field date_publication() -> &Option<String> {
        &self.date_publication
    }

    field infos_qualite() -> &InfosQualite {
        &self.infos_qualite
    }

    field legislature() -> &Option<String> {
        &self.legislature
    }

    field libelle() -> &Option<String> {
        &self.libelle
    }

    field mission_precedente_ref() -> &Option<String> {
        &self.mission_precedente_ref
    }

    field mission_suivante_ref() -> &Option<String> {
        &self.mission_suivante_ref
    }

    field nomin_principale() -> bool {
        self.nomin_principale
    }

    field organes(&executor) -> Vec<&Organe> {
        self.organes(&executor.context())
    }

    field organes_refs() -> &Vec<String> {
        self.organes_refs()
    }

    field preseance() -> &Option<String> {
        &self.preseance
    }

    field type_organe() -> CodeTypeOrgane {
        self.type_organe
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct MandatParlementaire {
    #[serde(rename = "acteurRef")]
    pub acteur_ref: String,
    chambre: Option<EmptyChoice>,
    collaborateurs: Option<Collaborateurs>,
    #[serde(rename = "dateDebut")]
    // TODO: Date<FixedOffset> type is not able to deserialize dates (`YYYY-MM-DD`).
    pub date_debut: String,
    #[serde(rename = "dateFin")]
    // TODO: Date<FixedOffset> type is not able to deserialize dates (`YYYY-MM-DD`).
    pub date_fin: Option<String>,
    #[serde(rename = "datePublication")]
    // TODO: Date<FixedOffset> type is not able to deserialize dates (`YYYY-MM-DD`).
    pub date_publication: Option<String>,
    pub election: Election,
    #[serde(rename = "InfosHorsSIAN")]
    pub infos_hors_sian: Option<InfosHorsSian>,
    #[serde(rename = "infosQualite")]
    pub infos_qualite: InfosQualite,
    pub legislature: Option<String>,
    pub mandature: Mandature,
    #[serde(deserialize_with = "str_to_bool", rename = "nominPrincipale")]
    pub nomin_principale: bool,
    organes: OrganesMandat,
    pub preseance: Option<String>,
    suppleants: Option<Suppleants>,
    #[serde(rename = "typeOrgane")]
    pub type_organe: CodeTypeOrgane,
    pub uid: String,
}

impl MandatParlementaire {
    pub fn collaborateurs(&self) -> Option<&Vec<Collaborateur>> {
        match &self.collaborateurs {
            None => None,
            Some(ref collaborateurs) => Some(&collaborateurs.collaborateurs),
        }
    }

    pub fn suppleant(&self) -> Option<&Suppleant> {
        match &self.suppleants {
            None => None,
            Some(ref suppleants) => Some(&suppleants.suppleant),
        }
    }
}

impl MandatTrait for MandatParlementaire {
    fn acteur_ref(&self) -> &str {
        &self.acteur_ref
    }

    fn organes_refs(&self) -> &Vec<String> {
        &self.organes.organes_refs
    }
}

graphql_object!(MandatParlementaire: Context |&self| {
    field acteur(&executor) -> Option<&Acteur> {
        self.acteur(&executor.context())
    }

    field acteur_ref() -> &str {
        &self.acteur_ref
    }

    field collaborateurs() -> Option<&Vec<Collaborateur>> {
        self.collaborateurs()
    }

    field date_debut() -> &str {
        &self.date_debut
    }

    field date_fin() -> &Option<String> {
        &self.date_fin
    }

    field date_publication() -> &Option<String> {
        &self.date_publication
    }

    field election() -> &Election {
        &self.election
    }

    field infos_hors_sian() -> &Option<InfosHorsSian> {
        &self.infos_hors_sian
    }

    field infos_qualite() -> &InfosQualite {
        &self.infos_qualite
    }

    field legislature() -> &Option<String> {
        &self.legislature
    }

    field mandature() -> &Mandature {
        &self.mandature
    }

    field nomin_principale() -> bool {
        self.nomin_principale
    }

    field organes(&executor) -> Vec<&Organe> {
        self.organes(&executor.context())
    }

    field organes_refs() -> &Vec<String> {
        self.organes_refs()
    }

    field preseance() -> &Option<String> {
        &self.preseance
    }

    field type_organe() -> CodeTypeOrgane {
        self.type_organe
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Mandats {
    #[serde(deserialize_with = "map_to_vec", rename = "mandat")]
    pub mandats: Vec<Mandat>,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct MandatSimple {
    #[serde(rename = "acteurRef")]
    pub acteur_ref: String,
    #[serde(rename = "dateDebut")]
    // TODO: Date<FixedOffset> type is not able to deserialize dates (`YYYY-MM-DD`).
    pub date_debut: String,
    #[serde(rename = "dateFin")]
    // TODO: Date<FixedOffset> type is not able to deserialize dates (`YYYY-MM-DD`).
    pub date_fin: Option<String>,
    #[serde(rename = "datePublication")]
    // TODO: Date<FixedOffset> type is not able to deserialize dates (`YYYY-MM-DD`).
    pub date_publication: Option<String>,
    #[serde(rename = "infosQualite")]
    pub infos_qualite: InfosQualite,
    pub legislature: Option<String>,
    #[serde(deserialize_with = "str_to_bool", rename = "nominPrincipale")]
    pub nomin_principale: bool,
    organes: OrganesMandat,
    pub preseance: Option<String>,
    #[serde(rename = "typeOrgane")]
    pub type_organe: CodeTypeOrgane,
    pub uid: String,
}

impl MandatTrait for MandatSimple {
    fn acteur_ref(&self) -> &str {
        &self.acteur_ref
    }

    fn organes_refs(&self) -> &Vec<String> {
        &self.organes.organes_refs
    }
}

graphql_object!(MandatSimple: Context |&self| {
    field acteur(&executor) -> Option<&Acteur> {
        self.acteur(&executor.context())
    }

    field acteur_ref() -> &str {
        &self.acteur_ref
    }

    field date_debut() -> &str {
        &self.date_debut
    }

    field date_fin() -> &Option<String> {
        &self.date_fin
    }

    field date_publication() -> &Option<String> {
        &self.date_publication
    }

    field infos_qualite() -> &InfosQualite {
        &self.infos_qualite
    }

    field legislature() -> &Option<String> {
        &self.legislature
    }

    field nomin_principale() -> bool {
        self.nomin_principale
    }

    field organes(&executor) -> Vec<&Organe> {
        self.organes(&executor.context())
    }

    field organes_refs() -> &Vec<String> {
        self.organes_refs()
    }

    field preseance() -> &Option<String> {
        &self.preseance
    }

    field type_organe() -> CodeTypeOrgane {
        self.type_organe
    }

    field uid() -> &str {
        &self.uid
    }
});

pub trait MandatTrait {
    fn acteur<'a>(&self, context: &'a Context) -> Option<&'a Acteur> {
        context.get_acteur_at_uid(&self.acteur_ref())
    }

    fn acteur_ref(&self) -> &str;

    fn organes<'a>(&self, context: &'a Context) -> Vec<&'a Organe> {
        self.organes_refs()
            .iter()
            .filter_map(|organe_ref| context.get_organe_at_uid(organe_ref))
            .collect::<Vec<&Organe>>()
    }

    fn organes_refs(&self) -> &Vec<String>;
}

#[derive(Clone, Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct Mandature {
    #[serde(rename = "causeFin")]
    pub cause_fin: Option<String>,
    #[serde(rename = "datePriseFonction")]
    // TODO: Date<FixedOffset> type is not able to deserialize dates (`YYYY-MM-DD`).
    pub date_prise_fonction: Option<String>,
    #[serde(rename = "mandatRemplaceRef")]
    pub mandat_remplace_ref: Option<String>,
    #[serde(rename = "placeHemicycle")]
    pub place_hemicycle: Option<String>,
    #[serde(rename = "premiereElection")]
    pub premiere_election: Option<String>,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct OrganesMandat {
    #[serde(deserialize_with = "str_to_vec", rename = "organeRef")]
    pub organes_refs: Vec<String>,
}

#[derive(Clone, Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct Suppleant {
    #[serde(rename = "dateDebut")]
    // TODO: Date<FixedOffset> type is not able to deserialize dates (`YYYY-MM-DD`).
    pub date_debut: String,
    #[serde(rename = "dateFin")]
    // TODO: Date<FixedOffset> type is not able to deserialize dates (`YYYY-MM-DD`).
    pub date_fin: Option<String>,
    #[serde(rename = "suppleantRef")]
    pub suppleant_ref: String,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Suppleants {
    pub suppleant: Suppleant,
}
