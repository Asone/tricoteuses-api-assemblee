use chrono::{DateTime, FixedOffset};

use crate::acteurs::Acteur;
use crate::commun::{CycleDeVie, EmptyChoice, Ident, ListePays, Uid, XmlNamespace};
use crate::contexts::Context;
use crate::organes::Organe;
use crate::points_odj::{PointOdj, PointsOdj};
use crate::serde_utils::{map_to_vec, str_to_bool, str_to_option_bool, str_to_vec};

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct ActeurDemandeur {
    pub nom: String,
    #[serde(rename = "acteurRef")]
    pub acteur_ref: String,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct AgendaJsonWrapper {
    pub reunions: Reunions,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Demandeurs {
    #[serde(default, deserialize_with = "map_to_vec", rename = "acteur")]
    pub acteurs: Vec<ActeurDemandeur>,
    pub organe: Option<OrganeDemandeur>,
}

impl Demandeurs {
    pub fn acteurs<'a>(&self, context: &'a Context) -> Vec<&'a Acteur> {
        self.acteurs_refs()
            .iter()
            .filter_map(|acteur_ref| context.get_acteur_at_uid(acteur_ref))
            .collect()
    }

    pub fn acteurs_refs(&self) -> Vec<&str> {
        self.acteurs
            .iter()
            .map(|acteur| acteur.acteur_ref.as_ref())
            .collect()
    }

    pub fn organe<'a>(&self, context: &'a Context) -> Option<&'a Organe> {
        match self.organe_ref() {
            None => None,
            Some(ref organe_ref) => context.get_organe_at_uid(organe_ref),
        }
    }

    pub fn organe_ref(&self) -> Option<&str> {
        match self.organe {
            None => None,
            Some(ref organe) => Some(&organe.organe_ref),
        }
    }
}

graphql_object!(Demandeurs: Context |&self| {
    field acteurs(&executor) -> Vec<&Acteur> {
        self.acteurs(&executor.context())
    }

    field acteurs_refs() -> Vec<&str> {
        self.acteurs_refs()
    }

    field organe(&executor) -> Option<&Organe> {
        self.organe(&executor.context())
    }

    field organe_ref() -> Option<&str> {
        self.organe_ref()
    }
});

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct Identifiants {
    #[serde(rename = "DateSeance")]
    // TODO: Date<FixedOffset> type is not able to deserialize dates with timezone (`YYYY-MM-DD±HH:MM`).
    pub date_seance: String,
    #[serde(rename = "idJO")]
    pub id_jo: Option<String>,
    #[serde(rename = "numSeanceJO")]
    pub num_seance_jo: Option<String>,
    pub quantieme: String,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct InfosReunionsInternationale {
    #[serde(deserialize_with = "str_to_bool", rename = "estReunionInternationale")]
    pub est_reunion_internationale: bool,
    #[serde(rename = "informationsComplementaires")]
    pub informations_complementaires: Option<EmptyChoice>,
    #[serde(rename = "listePays")]
    pub liste_pays: Option<ListePays>,
}

graphql_object!(InfosReunionsInternationale: Context |&self| {
    field est_reunion_internationale(&executor) -> bool {
        self.est_reunion_internationale
    }

    field pays_refs() -> Vec<&str> {
        match &self.liste_pays {
            None => vec![],
            Some(ref liste_pays) => liste_pays.pays_ref
                .iter()
                .map(|ref pays_ref| pays_ref.as_str())
                .collect(),
        }
    }
});

#[derive(Clone, Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct LieuReunion {
    pub code: Option<String>,
    #[serde(rename = "libelleCourt")]
    pub libelle_court: Option<String>,
    #[serde(rename = "libelleLong")]
    pub libelle_long: Option<String>,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Odj {
    #[serde(rename = "convocationODJ")]
    pub convocation_odj: Option<UnstructuredPointsOdj>,
    #[serde(rename = "pointsODJ")]
    pub points_odj: Option<PointsOdj>,
    #[serde(rename = "resumeODJ")]
    pub resume_odj: Option<UnstructuredPointsOdj>,
}

graphql_object!(Odj: Context |&self| {
    field convocation_odj() -> Vec<&str> {
        match self.convocation_odj {
            None => vec![],
            Some(ref convocation_odj) => convocation_odj.items
                .iter()
                .map(|s| s.as_str())
                .collect()
        }
    }

    field points_odj() -> Vec<&PointOdj> {
        match self.points_odj {
            None => vec![],
            Some(ref points_odj) => points_odj.points_odj
                .iter()
                .collect()
        }
    }

    field resume_odj() -> Vec<&str> {
        match self.resume_odj {
            None => vec![],
            Some(ref resume_odj) => resume_odj.items
                .iter()
                .map(|s| s.as_str())
                .collect()
        }
    }
});

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct OrganeDemandeur {
    pub nom: String,
    #[serde(rename = "organeRef")]
    pub organe_ref: String,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct ParticipantInterne {
    #[serde(rename = "acteurRef")]
    pub acteur_ref: String,
    pub presence: String,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct Participants {
    #[serde(rename = "participantsInternes")]
    pub participants_internes: Option<ParticipantsInternes>,
    #[serde(rename = "personnesAuditionnees")]
    pub personnes_auditionnees: Option<PersonnesAuditionnees>,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct ParticipantsInternes {
    #[serde(deserialize_with = "map_to_vec", rename = "participantInterne")]
    pub participants_internes: Vec<ParticipantInterne>,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct PersonneAuditionnee {
    #[serde(rename = "dateNais")]
    // TODO: Date<FixedOffset> type is not able to deserialize dates (`YYYY-MM-DD`).
    pub date_nais: Option<String>,
    pub ident: Ident,
    pub uid: Uid,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct PersonnesAuditionnees {
    #[serde(deserialize_with = "map_to_vec", rename = "personneAuditionnee")]
    pub personnes_auditionnees: Vec<PersonneAuditionnee>,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Reunion {
    #[serde(rename = "compteRenduRef")]
    pub compte_rendu_ref: Option<String>,
    #[serde(rename = "cycleDeVie")]
    pub cycle_de_vie: CycleDeVie,
    // TODO: Sometimes field `demandeur` exists instead of `demandeurs`, but it is always null.
    demandeur: Option<EmptyChoice>,
    pub demandeurs: Option<Demandeurs>,
    #[serde(rename = "formatReunion")]
    pub format_reunion: Option<String>,
    pub identifiants: Option<Identifiants>,
    #[serde(rename = "infosReunionsInternationale")]
    pub infos_reunions_internationale: Option<InfosReunionsInternationale>,
    pub lieu: Option<LieuReunion>,
    #[serde(rename = "ODJ")]
    pub odj: Option<Odj>,
    #[serde(rename = "organeReuniRef")]
    pub organe_reuni_ref: Option<String>,
    #[serde(
        default,
        deserialize_with = "str_to_option_bool",
        rename = "ouverturePresse"
    )]
    pub ouverture_presse: Option<bool>,
    pub participants: Option<Participants>,
    #[serde(rename = "sessionRef")]
    pub session_ref: Option<String>,
    #[serde(rename = "timeStampDebut")]
    pub timestamp_debut: DateTime<FixedOffset>,
    // TODO: Some timestamps end after 23:59:59. For example: `2017-10-10T24:00:00.000+02:00`.
    #[serde(rename = "timeStampFin")]
    // pub timestamp_fin: DateTime<FixedOffset>,
    pub timestamp_fin: Option<String>,
    #[serde(rename = "typeReunion")]
    pub type_reunion: Option<String>,
    pub uid: String,
    #[serde(rename = "@xmlns:xsi")]
    xml_xsi: Option<XmlNamespace>,
}

impl Reunion {
    pub fn organe_reuni<'a>(&self, context: &'a Context) -> Option<&'a Organe> {
        match self.organe_reuni_ref {
            None => None,
            Some(ref organe_reuni_ref) => context.get_organe_at_uid(organe_reuni_ref),
        }
    }
}

graphql_object!(Reunion: Context |&self| {
    field compte_rendu_ref() -> &Option<String> {
        &self.compte_rendu_ref
    }

    field cycle_de_vie() -> &CycleDeVie {
        &self.cycle_de_vie
    }

    field demandeurs() -> &Option<Demandeurs> {
        &self.demandeurs
    }

    field format_reunion() -> &Option<String> {
        &self.format_reunion
    }

    field identifiants() -> &Option<Identifiants> {
        &self.identifiants
    }

    field infos_reunions_internationale() -> &Option<InfosReunionsInternationale> {
        &self.infos_reunions_internationale
    }

    field lieu() -> &Option<LieuReunion> {
        &self.lieu
    }

    field odj() -> &Option<Odj> {
        &self.odj
    }

    field organe_reuni(&executor) -> Option<&Organe> {
        match self.organe_reuni_ref {
            None => None,
            Some(ref organe_reuni_ref) => executor.context().get_organe_at_uid(organe_reuni_ref),
        }
    }

    field organe_reuni_ref() -> &Option<String> {
        &self.organe_reuni_ref
    }

    field ouverture_presse() -> Option<bool> {
        self.ouverture_presse
    }

    field participants() -> &Option<Participants> {
        &self.participants
    }

    field session_ref() -> &Option<String> {
        &self.session_ref
    }

    field timestamp_debut() -> &DateTime<FixedOffset> {
        &self.timestamp_debut
    }

    field timestamp_fin() -> &Option<String> {
        &self.timestamp_fin
    }

    field type_reunion() -> &Option<String> {
        &self.type_reunion
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Reunions {
    #[serde(rename = "reunion")]
    pub reunions: Vec<Reunion>,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct UnstructuredPointsOdj {
    #[serde(deserialize_with = "str_to_vec", rename = "item")]
    pub items: Vec<String>,
}
