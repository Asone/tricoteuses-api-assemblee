#![feature(plugin)]

extern crate chrono;
#[macro_use]
extern crate juniper;
#[macro_use]
extern crate juniper_codegen;
#[macro_use]
extern crate lazy_static;
extern crate regex;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;
extern crate tricoteuses_api_assemblee_config as config;

mod actes_legislatifs;
mod acteurs;
mod acteurs_et_organes;
mod agendas;
mod amendements;
mod checks;
mod commun;
mod contexts;
mod dossiers_legislatifs;
mod mandats;
mod organes;
mod photos;
mod points_odj;
mod scrutins;
mod serde_utils;
mod textes_legislatifs;
mod types_organes;

use chrono::prelude::Utc;
use regex::Regex;
use std::collections::HashMap;
use std::fs::{self, File};
use std::path::Path;

pub use crate::acteurs::Acteur;
pub use crate::agendas::Reunion;
pub use crate::amendements::{Amendement, TexteLeg, TextesEtAmendements};
use crate::config::{Config, Verbosity};
pub use crate::contexts::{Context, Statut};
pub use crate::dossiers_legislatifs::DossierParlementaire;
pub use crate::mandats::Mandat;
pub use crate::organes::Organe;
pub use crate::photos::PhotoDepute;
pub use crate::scrutins::Scrutin;
pub use crate::textes_legislatifs::Document;
pub use crate::types_organes::{CodeTypeOrgane, FonctionOrgane, FONCTIONS_ORGANES};

pub struct EnabledDatasets {
    pub acteurs_et_organes: bool,
    pub agendas: bool,
    pub amendements: bool,
    pub dossiers_legislatifs: bool,
    pub photos: bool,
    pub scrutins: bool,
}

pub const ALL_DATASETS: EnabledDatasets = EnabledDatasets {
    acteurs_et_organes: true,
    agendas: true,
    amendements: true,
    dossiers_legislatifs: true,
    photos: true,
    scrutins: true,
};

pub const NO_DATASET: EnabledDatasets = EnabledDatasets {
    acteurs_et_organes: false,
    agendas: false,
    amendements: false,
    dossiers_legislatifs: false,
    photos: false,
    scrutins: false,
};

pub fn load(
    config: &Config,
    config_dir: &Path,
    enabled_datasets: &EnabledDatasets,
    verbosity: &Verbosity,
) -> Context {
    let data_dir = config_dir.join(&config.data.dir);

    let mut acteurs_et_organes_wrappers: Vec<acteurs_et_organes::ExportJsonWrapper> = Vec::new();
    if enabled_datasets.acteurs_et_organes {
        for dataset in &config.acteurs_et_organes {
            if dataset.ignore_for_web {
                continue;
            }

            // Import JSON open data files.
            let json_file_path = fs::canonicalize(data_dir.join(&dataset.filename)).unwrap();
            if *verbosity != Verbosity::Verbosity0 {
                println!(
                    "Loading \"acteurs et organes\" file: {}...",
                    json_file_path.to_string_lossy()
                );
            }
            let json_file = File::open(json_file_path).expect("JSON file not found");
            let export_wrapper: acteurs_et_organes::ExportJsonWrapper =
                serde_json::from_reader(json_file).unwrap();

            // Check data integrity.
            // checks::check_data(&export_wrapper);

            acteurs_et_organes_wrappers.push(export_wrapper);
        }
    }

    let mut agendas_wrappers: Vec<agendas::AgendaJsonWrapper> = Vec::new();
    if enabled_datasets.agendas {
        for dataset in &config.agendas {
            if dataset.ignore_for_web {
                continue;
            }

            // Import JSON open data files.
            let json_file_path = fs::canonicalize(data_dir.join(&dataset.filename)).unwrap();
            if *verbosity != Verbosity::Verbosity0 {
                println!(
                    "Loading \"agenda\" file: {}...",
                    json_file_path.to_string_lossy()
                );
            }
            let json_file = File::open(json_file_path).expect("JSON file not found");
            let export_wrapper: agendas::AgendaJsonWrapper =
                serde_json::from_reader(json_file).unwrap();

            // Check data integrity.
            // checks::check_data(&export_wrapper);

            agendas_wrappers.push(export_wrapper);
        }
    }

    let mut amendements_wrappers: Vec<amendements::AmendementsJsonWrapper> = Vec::new();
    if enabled_datasets.amendements {
        for dataset in &config.amendements {
            if dataset.ignore_for_web {
                continue;
            }

            // Import JSON open data files.
            let json_file_path = fs::canonicalize(data_dir.join(&dataset.filename)).unwrap();
            if *verbosity != Verbosity::Verbosity0 {
                println!(
                    "Loading \"amendements\" file: {}...",
                    json_file_path.to_string_lossy()
                );
            }
            let json_file = File::open(json_file_path).expect("JSON file not found");
            let export_wrapper: amendements::AmendementsJsonWrapper =
                serde_json::from_reader(json_file).unwrap();

            // Check data integrity.
            // checks::check_data(&export_wrapper);

            amendements_wrappers.push(export_wrapper);
        }
    }

    let mut dossiers_legislatifs_wrappers: Vec<dossiers_legislatifs::ExportJsonWrapper> =
        Vec::new();
    if enabled_datasets.dossiers_legislatifs {
        for dataset in &config.dossiers_legislatifs {
            if dataset.ignore_for_web {
                continue;
            }

            // Import JSON open data files.
            let json_file_path = fs::canonicalize(data_dir.join(&dataset.filename)).unwrap();
            if *verbosity != Verbosity::Verbosity0 {
                println!(
                    "Loading \"dossier législatif\" file: {}...",
                    json_file_path.to_string_lossy()
                );
            }
            let json_file = File::open(json_file_path).expect("JSON file not found");
            let export_wrapper: dossiers_legislatifs::ExportJsonWrapper =
                serde_json::from_reader(json_file).unwrap();

            // Check data integrity.
            checks::check_data(&export_wrapper);

            dossiers_legislatifs_wrappers.push(export_wrapper);
        }
    }

    if enabled_datasets.photos {
        for photos_file_path in &["photos_deputes_15/deputes.json"] {
            // Import JSON open data files.
            let json_file_path = fs::canonicalize(data_dir.join(&photos_file_path)).unwrap();
            if *verbosity != Verbosity::Verbosity0 {
                println!(
                    "Loading \"photos\" file: {}...",
                    json_file_path.to_string_lossy()
                );
            }
            let json_file = File::open(json_file_path).expect("JSON file not found");
            let photo_by_depute_id: HashMap<String, PhotoDepute> =
                serde_json::from_reader(json_file).unwrap();

            // Add photos to deputes.
            for wrapper in &mut acteurs_et_organes_wrappers {
                for acteur in &mut wrapper.export.acteurs.acteurs {
                    if let Some(photo) = photo_by_depute_id.get(acteur.uid()) {
                        acteur.photo = Some(photo.clone());
                    }
                }
            }
        }
    }

    let mut scrutins_wrappers: Vec<scrutins::ScrutinsJsonWrapper> = Vec::new();
    if enabled_datasets.scrutins {
        for dataset in &config.scrutins {
            if dataset.ignore_for_web {
                continue;
            }

            // Import JSON open data files.
            let json_file_path = fs::canonicalize(data_dir.join(&dataset.filename)).unwrap();
            if *verbosity != Verbosity::Verbosity0 {
                println!(
                    "Loading \"scrutins\" file: {}...",
                    json_file_path.to_string_lossy()
                );
            }
            let json_file = File::open(json_file_path).expect("JSON file not found");
            let export_wrapper: scrutins::ScrutinsJsonWrapper =
                serde_json::from_reader(json_file).unwrap();

            // Check data integrity.
            // checks::check_data(&export_wrapper);

            scrutins_wrappers.push(export_wrapper);
        }
    }

    // Index recursive structures to allow them to be flattened for GraphQL transmission.
    for wrapper in &mut dossiers_legislatifs_wrappers {
        for document in &mut wrapper.export.textes_legislatifs.documents {
            let mut index: i32 = 0;
            document.index_document(&mut index);
        }

        for dossier in &mut wrapper.export.dossiers_legislatifs.dossiers {
            dossier.dossier_parlementaire.index_actes_legislatifs();
        }
    }

    let mut acteur_by_uid: HashMap<String, *const Acteur> = HashMap::new();
    for wrapper in &acteurs_et_organes_wrappers {
        for acteur in &wrapper.export.acteurs.acteurs {
            // Keep the existing "acteur", because the first wrapper contains only the current "députés",
            // but it has the most details on them.
            acteur_by_uid
                .entry(acteur.uid().to_string())
                .or_insert(acteur);
        }
    }

    let mut amendement_by_uid: HashMap<String, *const Amendement> = HashMap::new();
    let mut texte_leg_by_uid: HashMap<String, *const TexteLeg> = HashMap::new();
    for wrapper in &amendements_wrappers {
        match &wrapper.textes_et_amendements {
            None => (),
            Some(ref textes_et_amendements) => {
                for texte_leg in &textes_et_amendements.texte_leg {
                    match &texte_leg.ref_texte_legislatif {
                        None => (),
                        Some(ref ref_texte_legislatif) => {
                            texte_leg_by_uid.insert(ref_texte_legislatif.clone(), texte_leg);
                        }
                    }
                    match &texte_leg.amendements {
                        None => (),
                        Some(ref amendements) => {
                            for amendement in &amendements.amendements {
                                amendement_by_uid.insert(amendement.uid.clone(), amendement);
                            }
                        }
                    }
                }
            }
        }
    }

    let mut document_by_uid: HashMap<String, *const Document> = HashMap::new();
    let mut dossier_parlementaire_by_segment: HashMap<String, *const DossierParlementaire> =
        HashMap::new();
    let mut dossier_parlementaire_by_signet_senat: HashMap<String, *const DossierParlementaire> =
        HashMap::new();
    let mut dossier_parlementaire_by_uid: HashMap<String, *const DossierParlementaire> =
        HashMap::new();
    lazy_static! {
        static ref senat_chemin_regex: Regex =
            Regex::new(r"^https?://www.senat.fr/dossier-legislatif/(.*).html$").unwrap();
    }
    for wrapper in &dossiers_legislatifs_wrappers {
        for document in &wrapper.export.textes_legislatifs.documents {
            document_by_uid.insert(document.uid().to_string(), document);
        }

        for dossier in &wrapper.export.dossiers_legislatifs.dossiers {
            let dossier_parlementaire = &dossier.dossier_parlementaire;
            let titre_dossier = dossier_parlementaire.titre_dossier();
            match titre_dossier.senat_chemin {
                None => (),
                Some(ref senat_chemin) => {
                    let captures = senat_chemin_regex.captures(&senat_chemin).unwrap();
                    dossier_parlementaire_by_signet_senat
                        .insert(captures[1].to_string(), dossier_parlementaire);
                }
            }
            match titre_dossier.titre_chemin {
                None => (),
                Some(ref titre_chemin) => {
                    dossier_parlementaire_by_segment
                        .insert(titre_chemin.to_string(), dossier_parlementaire);
                }
            }
            dossier_parlementaire_by_uid.insert(
                dossier_parlementaire.uid().to_string(),
                dossier_parlementaire,
            );
        }
    }

    let mut organe_by_uid: HashMap<String, *const Organe> = HashMap::new();
    for wrapper in &acteurs_et_organes_wrappers {
        for organe in &wrapper.export.organes.organes {
            // Keep the first existing "organe", because the first wrapper is the most complete.
            organe_by_uid
                .entry(organe.uid().to_string())
                .or_insert(organe);
        }
    }

    let mut reunion_by_uid: HashMap<String, *const Reunion> = HashMap::new();
    for wrapper in &agendas_wrappers {
        for reunion in &wrapper.reunions.reunions {
            reunion_by_uid.insert(reunion.uid.clone(), reunion);
        }
    }

    let mut reunions_by_date: HashMap<String, Vec<*const Reunion>> = HashMap::new();
    for wrapper in &agendas_wrappers {
        for reunion in &wrapper.reunions.reunions {
            reunions_by_date
                .entry(format!("{}", reunion.timestamp_debut.format("%Y-%m-%d")))
                .and_modify(|reunions| reunions.push(reunion))
                .or_insert_with(|| vec![reunion]);
        }
    }

    let mut reunions_by_dossier_uid: HashMap<String, Vec<*const Reunion>> = HashMap::new();
    for wrapper in &agendas_wrappers {
        for reunion in &wrapper.reunions.reunions {
            if let Some(ref odj) = reunion.odj {
                if let Some(ref points_odj) = odj.points_odj {
                    for point_odj in &points_odj.points_odj {
                        if let Some(ref dossiers_legislatifs_refs) =
                            point_odj.dossiers_legislatifs_refs()
                        {
                            for dossier_ref in &dossiers_legislatifs_refs.dossiers_refs {
                                reunions_by_dossier_uid
                                    .entry(dossier_ref.clone())
                                    .and_modify(|reunions| reunions.push(reunion))
                                    .or_insert_with(|| vec![reunion]);
                            }
                        }
                    }
                }
            }
        }
    }

    let mut scrutin_by_uid: HashMap<String, *const Scrutin> = HashMap::new();
    for wrapper in &scrutins_wrappers {
        for scrutin in &wrapper.scrutins.scrutins {
            scrutin_by_uid.insert(scrutin.uid.clone(), scrutin);
        }
    }

    Context {
        acteur_by_uid,
        acteurs_et_organes_wrappers,
        agendas_wrappers,
        amendement_by_uid,
        amendements_wrappers,
        document_by_uid,
        dossier_parlementaire_by_segment,
        dossier_parlementaire_by_signet_senat,
        dossier_parlementaire_by_uid,
        dossiers_legislatifs_wrappers,
        organe_by_uid,
        reunion_by_uid,
        reunions_by_date,
        reunions_by_dossier_uid,
        scrutin_by_uid,
        scrutins_wrappers,
        statut: Statut {
            date_mise_a_jour_donnees: Utc::now(),
        },
        texte_leg_by_uid,
    }
}
