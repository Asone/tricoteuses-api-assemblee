#[derive(Clone, Debug, Deserialize, GraphQLObject, Serialize)]
#[serde(deny_unknown_fields)]
pub struct PhotoDepute {
    pub chemin: String,
    #[serde(rename = "cheminMosaique")]
    pub chemin_mosaique: String,
    pub hauteur: i32,
    pub largeur: i32,
    #[serde(rename = "xMosaique")]
    pub x_mosaique: i32,
    #[serde(rename = "yMosaique")]
    pub y_mosaique: i32,
}
