use crate::commun::{EmptyChoice, ListePays};
use crate::contexts::Context;
use crate::types_organes::CodeTypeOrgane;

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct GroupePolitique {
    chambre: Option<EmptyChoice>,
    #[serde(rename = "codeType")]
    pub code_type: CodeTypeOrgane,
    pub legislature: Option<String>, // TODO: Convert to u8.
    pub libelle: String,
    #[serde(rename = "libelleAbrege")]
    pub libelle_abrege: String,
    #[serde(rename = "libelleAbrev")]
    pub libelle_abrev: String,
    #[serde(rename = "libelleEdition")]
    pub libelle_edition: Option<String>,
    #[serde(rename = "organeParent")]
    pub organe_parent: Option<String>,
    #[serde(rename = "positionPolitique")]
    pub position_politique: Option<String>,
    pub regime: Option<String>,
    pub secretariat: Secretariat,
    pub uid: String,
    #[serde(rename = "viMoDe")]
    pub vimode: Vimode,
}

graphql_object!(GroupePolitique: Context |&self| {
    field code_type() -> &CodeTypeOrgane {
        &self.code_type
    }

    field legislature() -> Option<&str> {
        // TODO: Convert to u8.
        match &self.legislature {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field libelle() -> &str {
        &self.libelle
    }

    field libelle_abrege() -> &str {
        &self.libelle_abrege
    }

    field libelle_abrev() -> &str {
        &self.libelle_abrev
    }

    field libelle_edition() -> Option<&str> {
        match &self.libelle_edition {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field organe_parent(&executor) -> Option<&Organe> {
        match &self.organe_parent {
            None => None,
            Some(ref organe_parent) => {
                let context = &executor.context();
                context.get_organe_at_uid(organe_parent)
            },
        }
    }

    field organe_parent_ref() -> Option<&str> {
        match &self.organe_parent {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field position_politique() -> Option<&str> {
        match &self.position_politique {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field regime() -> Option<&str> {
        match &self.regime {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field secretariat() -> &Secretariat {
        &self.secretariat
    }

    field uid() -> &str {
        &self.uid
    }

    field vimode() -> &Vimode {
        &self.vimode
    }
});

#[derive(Clone, Debug, Deserialize)]
#[serde(tag = "@xsi:type")]
pub enum Organe {
    #[serde(rename = "GroupePolitique_type")]
    GroupePolitique(GroupePolitique),
    #[serde(rename = "OrganeExterne_Type")]
    OrganeExterne(OrganeExterne),
    #[serde(rename = "OrganeExtraParlementaire_type")]
    OrganeExtraParlementaire(OrganeExtraParlementaire),
    #[serde(rename = "OrganeParlementaire_Type")]
    OrganeParlementaire(OrganeParlementaire),
    #[serde(rename = "OrganeParlementaireInternational")]
    OrganeParlementaireInternational(OrganeParlementaireInternational),
}

impl Organe {
    //     pub fn code_type(&self) -> CodeTypeOrgane {
    //         match *self {
    //             Organe::GroupePolitique(GroupePolitique { code_type, .. })
    //             | Organe::OrganeExterne(OrganeExterne { code_type, .. })
    //             | Organe::OrganeExtraParlementaire(OrganeExtraParlementaire { code_type, .. })
    //             | Organe::OrganeParlementaire(OrganeParlementaire { code_type, .. })
    //             | Organe::OrganeParlementaireInternational(OrganeParlementaireInternational { code_type, .. }) => code_type,
    //         }
    //     }

    //     pub fn libelle(&self) -> &str {
    //         match *self {
    //             Organe::GroupePolitique(GroupePolitique { ref libelle, .. })
    //             | Organe::OrganeExterne(OrganeExterne { ref libelle, .. })
    //             | Organe::OrganeExtraParlementaire(OrganeExtraParlementaire { ref libelle, .. })
    //             | Organe::OrganeParlementaire(OrganeParlementaire { ref libelle, .. })
    //             | Organe::OrganeParlementaireInternational(OrganeParlementaireInternational { ref libelle, .. }) => &libelle,
    //         }
    //     }

    //     pub fn libelle_abrege(&self) -> &str {
    //         match *self {
    //             Organe::GroupePolitique(GroupePolitique {
    //                 ref libelle_abrege, ..
    //             })
    //             | Organe::OrganeExterne(OrganeExterne {
    //                 ref libelle_abrege, ..
    //             })
    //             | Organe::OrganeExtraParlementaire(OrganeExtraParlementaire {
    //                 ref libelle_abrege,
    //                 ..
    //             })
    //             | Organe::OrganeParlementaire(OrganeParlementaire {
    //                 ref libelle_abrege, ..
    //             })
    //             | Organe::OrganeParlementaireInternational(OrganeParlementaireInternational {
    //                 ref libelle_abrege, ..
    //             }) => &libelle_abrege,
    //         }
    //     }

    //     pub fn libelle_abrev(&self) -> &str {
    //         match *self {
    //             Organe::GroupePolitique(GroupePolitique {
    //                 ref libelle_abrev, ..
    //             })
    //             | Organe::OrganeExterne(OrganeExterne {
    //                 ref libelle_abrev, ..
    //             })
    //             | Organe::OrganeExtraParlementaire(OrganeExtraParlementaire {
    //                 ref libelle_abrev,
    //                 ..
    //             })
    //             | Organe::OrganeParlementaire(OrganeParlementaire {
    //                 ref libelle_abrev, ..
    //             })
    //             | Organe::OrganeParlementaireInternational(OrganeParlementaireInternational {
    //                 ref libelle_abrev, ..
    //             }) => &libelle_abrev,
    //         }
    //     }

    //     pub fn libelle_edition(&self) -> Option<&str> {
    //         match *self {
    //             Organe::GroupePolitique(GroupePolitique {
    //                 ref libelle_edition,
    //                 ..
    //             })
    //             | Organe::OrganeExterne(OrganeExterne {
    //                 ref libelle_edition,
    //                 ..
    //             })
    //             | Organe::OrganeExtraParlementaire(OrganeExtraParlementaire {
    //                 ref libelle_edition,
    //                 ..
    //             })
    //             | Organe::OrganeParlementaire(OrganeParlementaire {
    //                 ref libelle_edition,
    //                 ..
    //             })
    //             | Organe::OrganeParlementaireInternational(OrganeParlementaireInternational {
    //                 ref libelle_edition,
    //                 ..
    //             }) => match &libelle_edition {
    //                 None => None,
    //                 Some(ref libelle_edition) => Some(&libelle_edition),
    //             },
    //         }
    //     }

    //     pub fn organe_parent(&self) -> Option<&str> {
    //         match *self {
    //             Organe::GroupePolitique(GroupePolitique {
    //                 ref organe_parent,
    //                 ..
    //             })
    //             | Organe::OrganeExterne(OrganeExterne {
    //                 ref organe_parent,
    //                 ..
    //             })
    //             | Organe::OrganeExtraParlementaire(OrganeExtraParlementaire {
    //                 ref organe_parent,
    //                 ..
    //             })
    //             | Organe::OrganeParlementaire(OrganeParlementaire {
    //                 ref organe_parent,
    //                 ..
    //             })
    //             | Organe::OrganeParlementaireInternational(OrganeParlementaireInternational {
    //                 ref organe_parent,
    //                 ..
    //             }) => match &organe_parent {
    //                 None => None,
    //                 Some(ref organe_parent) => Some(&organe_parent),
    //             },
    //         }
    //     }

    pub fn uid(&self) -> &str {
        match *self {
            Organe::GroupePolitique(GroupePolitique { ref uid, .. })
            | Organe::OrganeExterne(OrganeExterne { ref uid, .. })
            | Organe::OrganeExtraParlementaire(OrganeExtraParlementaire { ref uid, .. })
            | Organe::OrganeParlementaire(OrganeParlementaire { ref uid, .. })
            | Organe::OrganeParlementaireInternational(OrganeParlementaireInternational {
                ref uid,
                ..
            }) => &uid,
        }
    }

    //     pub fn vimode(&self) -> &Vimode {
    //         match *self {
    //             Organe::GroupePolitique(GroupePolitique { ref vimode, .. })
    //             | Organe::OrganeExterne(OrganeExterne { ref vimode, .. })
    //             | Organe::OrganeExtraParlementaire(OrganeExtraParlementaire { ref vimode, .. })
    //             | Organe::OrganeParlementaire(OrganeParlementaire { ref vimode, .. })
    //             | Organe::OrganeParlementaireInternational(OrganeParlementaireInternational { ref vimode, .. }) => &vimode,
    //         }
    //     }
}

// Using graphql_union because instance_resolvers of graphql_interface does not work with Juniper 0.9.2.
// graphql_interface!(Organe: Context |&self| {
graphql_union!(Organe: Context |&self| {
    // field code_type() -> CodeTypeOrgane {
    //     self.code_type()
    // }

    // field libelle() -> &str {
    //     self.libelle()
    // }

    // field libelle_abrege() -> &str {
    //     self.libelle_abrege()
    // }

    // field libelle_abrev() -> &str {
    //     self.libelle_abrev()
    // }

    // field libelle_edition() -> Option<&str> {
    //     self.libelle_edition()
    // }

    // field organe_parent(&executor) -> Option<&Organe> {
    //     match &self.organe_parent {
    //         None => None,
    //         Some(ref organe_parent) => {
    //             let context = &executor.context();
    //             context.get_organe_at_uid(organe_parent)
    //         },
    //     }
    // }

    // field organe_parent_ref() -> Option<&str> {
    //     match &self.organe_parent {
    //         None => None,
    //         Some(ref s) => Some(s.as_str()),
    //     }
    // }

    // field uid() -> &str {
    //     self.uid()
    // }

    // field vimode() -> &Vimode {
    //     self.vimode()
    // }

    instance_resolvers: |_| {
        &GroupePolitique => match *self {
            Organe::GroupePolitique(ref organe) => Some(organe),
            _ => None,
        },
        &OrganeExterne => match *self {
            Organe::OrganeExterne(ref organe) => Some(organe),
            _ => None,
        },
        &OrganeExtraParlementaire => match *self {
            Organe::OrganeExtraParlementaire(ref organe) => Some(organe),
            _ => None,
        },
        &OrganeParlementaire => match *self {
            Organe::OrganeParlementaire(ref organe) => Some(organe),
            _ => None,
        },
        &OrganeParlementaireInternational => match *self {
            Organe::OrganeParlementaireInternational(ref organe) => Some(organe),
            _ => None,
        },
    }
});

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct OrganeExterne {
    #[serde(rename = "codeType")]
    pub code_type: CodeTypeOrgane,
    pub libelle: String,
    #[serde(rename = "libelleAbrege")]
    pub libelle_abrege: String,
    #[serde(rename = "libelleAbrev")]
    pub libelle_abrev: String,
    #[serde(rename = "libelleEdition")]
    pub libelle_edition: Option<String>,
    #[serde(rename = "organeParent")]
    pub organe_parent: Option<String>,
    pub uid: String,
    #[serde(rename = "viMoDe")]
    pub vimode: Vimode,
}

graphql_object!(OrganeExterne: Context |&self| {
    field code_type() -> &CodeTypeOrgane {
        &self.code_type
    }

    field libelle() -> &str {
        &self.libelle
    }

    field libelle_abrege() -> &str {
        &self.libelle_abrege
    }

    field libelle_abrev() -> &str {
        &self.libelle_abrev
    }

    field libelle_edition() -> Option<&str> {
        match &self.libelle_edition {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field organe_parent(&executor) -> Option<&Organe> {
        match &self.organe_parent {
            None => None,
            Some(ref organe_parent) => {
                let context = &executor.context();
                context.get_organe_at_uid(organe_parent)
            },
        }
    }

    field organe_parent_ref() -> Option<&str> {
        match &self.organe_parent {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field uid() -> &str {
        &self.uid
    }

    field vimode() -> &Vimode {
        &self.vimode
    }
});

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct OrganeExtraParlementaire {
    chambre: Option<EmptyChoice>,
    #[serde(rename = "codeType")]
    pub code_type: CodeTypeOrgane,
    pub legislature: Option<EmptyChoice>, // TODO: Convert to u8.
    pub libelle: String,
    #[serde(rename = "libelleAbrege")]
    pub libelle_abrege: String,
    #[serde(rename = "libelleAbrev")]
    pub libelle_abrev: String,
    #[serde(rename = "libelleEdition")]
    pub libelle_edition: Option<String>,
    #[serde(rename = "nombreReunionsAnnuelles")]
    pub nombre_reunions_annuelles: Option<String>, // TODO: Convert to Option<u32>.
    #[serde(rename = "organeParent")]
    pub organe_parent: Option<String>,
    pub regime: Option<String>,
    #[serde(rename = "regimeJuridique")]
    pub regime_juridique: Option<String>,
    #[serde(rename = "siteInternet")]
    pub site_internet: Option<String>,
    pub uid: String,
    #[serde(rename = "viMoDe")]
    pub vimode: Vimode,
}

graphql_object!(OrganeExtraParlementaire: Context |&self| {
    field code_type() -> &CodeTypeOrgane {
        &self.code_type
    }

    field libelle() -> &str {
        &self.libelle
    }

    field libelle_abrege() -> &str {
        &self.libelle_abrege
    }

    field libelle_abrev() -> &str {
        &self.libelle_abrev
    }

    field libelle_edition() -> Option<&str> {
        match &self.libelle_edition {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field nombre_reunions_annuelles() -> Option<&str> {
        match &self.nombre_reunions_annuelles {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field organe_parent(&executor) -> Option<&Organe> {
        match &self.organe_parent {
            None => None,
            Some(ref organe_parent) => {
                let context = &executor.context();
                context.get_organe_at_uid(organe_parent)
            },
        }
    }

    field organe_parent_ref() -> Option<&str> {
        match &self.organe_parent {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field regime() -> Option<&str> {
        match &self.regime {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field regime_juridique() -> Option<&str> {
        match &self.regime_juridique {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field site_internet() -> Option<&str> {
        match &self.site_internet {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field uid() -> &str {
        &self.uid
    }

    field vimode() -> &Vimode {
        &self.vimode
    }
});

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct OrganeParlementaire {
    chambre: Option<EmptyChoice>,
    #[serde(rename = "codeType")]
    pub code_type: CodeTypeOrgane,
    pub legislature: Option<String>, // TODO: Convert to u8.
    pub libelle: String,
    #[serde(rename = "libelleAbrege")]
    pub libelle_abrege: String,
    #[serde(rename = "libelleAbrev")]
    pub libelle_abrev: String,
    #[serde(rename = "libelleEdition")]
    pub libelle_edition: Option<String>,
    #[serde(rename = "organeParent")]
    pub organe_parent: Option<String>,
    pub regime: Option<String>,
    pub secretariat: Secretariat,
    pub uid: String,
    #[serde(rename = "viMoDe")]
    pub vimode: Vimode,
}

graphql_object!(OrganeParlementaire: Context |&self| {
    field code_type() -> &CodeTypeOrgane {
        &self.code_type
    }

    field legislature() -> Option<&str> {
        // TODO: Convert to u8.
        match &self.legislature {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field libelle() -> &str {
        &self.libelle
    }

    field libelle_abrege() -> &str {
        &self.libelle_abrege
    }

    field libelle_abrev() -> &str {
        &self.libelle_abrev
    }

    field libelle_edition() -> Option<&str> {
        match &self.libelle_edition {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field organe_parent(&executor) -> Option<&Organe> {
        match &self.organe_parent {
            None => None,
            Some(ref organe_parent) => {
                let context = &executor.context();
                context.get_organe_at_uid(organe_parent)
            },
        }
    }

    field organe_parent_ref() -> Option<&str> {
        match &self.organe_parent {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field regime() -> Option<&str> {
        match &self.regime {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field secretariat() -> &Secretariat {
        &self.secretariat
    }

    field uid() -> &str {
        &self.uid
    }

    field vimode() -> &Vimode {
        &self.vimode
    }
});

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct OrganeParlementaireInternational {
    chambre: Option<EmptyChoice>,
    #[serde(rename = "codeType")]
    pub code_type: CodeTypeOrgane,
    pub legislature: Option<String>, // TODO: Convert to u8.
    pub libelle: String,
    #[serde(rename = "libelleAbrege")]
    pub libelle_abrege: String,
    #[serde(rename = "libelleAbrev")]
    pub libelle_abrev: String,
    #[serde(rename = "libelleEdition")]
    pub libelle_edition: Option<String>,
    #[serde(rename = "listePays")]
    pub liste_pays: Option<ListePays>,
    #[serde(rename = "organeParent")]
    pub organe_parent: Option<String>,
    pub regime: Option<String>,
    pub secretariat: Secretariat,
    pub uid: String,
    #[serde(rename = "viMoDe")]
    pub vimode: Vimode,
}

graphql_object!(OrganeParlementaireInternational: Context |&self| {
    field code_type() -> &CodeTypeOrgane {
        &self.code_type
    }

    field legislature() -> Option<&str> {
        // TODO: Convert to u8.
        match &self.legislature {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field libelle() -> &str {
        &self.libelle
    }

    field libelle_abrege() -> &str {
        &self.libelle_abrege
    }

    field libelle_abrev() -> &str {
        &self.libelle_abrev
    }

    field libelle_edition() -> Option<&str> {
        match &self.libelle_edition {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field organe_parent(&executor) -> Option<&Organe> {
        match &self.organe_parent {
            None => None,
            Some(ref organe_parent) => {
                let context = &executor.context();
                context.get_organe_at_uid(organe_parent)
            },
        }
    }

    field organe_parent_ref() -> Option<&str> {
        match &self.organe_parent {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field pays_refs() -> Vec<&str> {
        match &self.liste_pays {
            None => vec![],
            Some(ref liste_pays) => liste_pays.pays_ref
                .iter()
                .map(|ref pays_ref| pays_ref.as_str())
                .collect(),
        }
    }

    field regime() -> Option<&str> {
        match &self.regime {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field secretariat() -> &Secretariat {
        &self.secretariat
    }

    field uid() -> &str {
        &self.uid
    }

    field vimode() -> &Vimode {
        &self.vimode
    }
});

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Organes {
    #[serde(rename = "organe")]
    pub organes: Vec<Organe>,
}

#[derive(Clone, Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct Secretariat {
    pub secretaire01: Option<String>,
    pub secretaire02: Option<String>,
}

#[derive(Clone, Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
/// Vie Mort et Décomposition
pub struct Vimode {
    #[serde(rename = "dateAgrement")]
    // TODO: Date<FixedOffset> type is not able to deserialize dates (`YYYY-MM-DD`).
    pub date_agrement: Option<String>,
    #[serde(rename = "dateDebut")]
    // TODO: Date<FixedOffset> type is not able to deserialize dates (`YYYY-MM-DD`).
    pub date_debut: Option<String>,
    #[serde(rename = "dateFin")]
    // TODO: Date<FixedOffset> type is not able to deserialize dates (`YYYY-MM-DD`).
    pub date_fin: Option<String>,
}
