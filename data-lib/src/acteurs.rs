use crate::commun::{Ident, Uid};
use crate::contexts::Context;
use crate::mandats::{Mandat, MandatTrait, Mandats};
use crate::organes::{GroupePolitique, Organe, OrganeParlementaire};
use crate::photos::PhotoDepute;
use crate::types_organes::CodeTypeOrgane;

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Acteur {
    #[serde(rename = "etatCivil")]
    pub etat_civil: EtatCivil,
    pub mandats: Mandats,
    // The field photo doesn't come from Assemblee open data. It is added after.
    #[serde(rename = "photoDepute", skip_serializing)]
    pub photo: Option<PhotoDepute>,
    pub profession: Option<Profession>,
    pub uid: Uid,
    pub uri_hatvp: Option<String>,
}

impl Acteur {
    pub fn commission_permanente<'a>(
        &self,
        context: &'a Context,
        date: &str,
    ) -> Option<&'a OrganeParlementaire> {
        for mandat in self.mandats(Some(date)) {
            if let Mandat::MandatSimple(mandat_simple) = mandat {
                if mandat_simple.type_organe == CodeTypeOrgane::CommissionPermanente {
                    for organe in mandat_simple.organes(context) {
                        if let Organe::OrganeParlementaire(organe_parlementaire) = organe {
                            return Some(organe_parlementaire);
                        }
                    }
                }
            }
        }
        None
    }

    pub fn groupe_politique<'a>(
        &self,
        context: &'a Context,
        date: &str,
    ) -> Option<&'a GroupePolitique> {
        for mandat in self.mandats(Some(date)) {
            if let Mandat::MandatSimple(mandat_simple) = mandat {
                if mandat_simple.type_organe == CodeTypeOrgane::GroupePolitique {
                    for organe in mandat_simple.organes(context) {
                        if let Organe::GroupePolitique(groupe_politique) = organe {
                            return Some(groupe_politique);
                        }
                    }
                }
            }
        }
        None
    }

    pub fn mandats(&self, date: Option<&str>) -> Vec<&Mandat> {
        match date {
            None => self.mandats.mandats.iter().collect::<Vec<&Mandat>>(),
            Some(date) => self
                .mandats
                .mandats
                .iter()
                .filter(|mandat| {
                    let date: &str = date;
                    if date >= mandat.date_debut() {
                        match mandat.date_fin() {
                            None => true,
                            Some(ref date_fin) if date <= date_fin => true,
                            _ => false,
                        }
                    } else {
                        false
                    }
                })
                .collect::<Vec<&Mandat>>(),
        }
    }

    pub fn organes<'a>(&self, context: &'a Context, date: Option<&str>) -> Vec<&'a Organe> {
        let mut organes: Vec<&Organe> = Vec::new();
        for mandat in self.mandats(date) {
            organes.extend(mandat.organes(context))
        }
        organes.sort_unstable_by_key(|organe| organe.uid());
        organes.dedup_by_key(|organe| organe.uid());
        organes
    }

    pub fn uid(&self) -> &str {
        &self.uid.text
    }
}

graphql_object!(Acteur: Context |&self| {
    field commission_permanente(&executor, date: String) -> Option<&OrganeParlementaire> {
        self.commission_permanente(&executor.context(), &date)
    }

    field etat_civil() -> &EtatCivil {
        &self.etat_civil
    }

    field groupe_politique(&executor, date: String) -> Option<&GroupePolitique> {
        self.groupe_politique(&executor.context(), &date)
    }

    field mandats(date: Option<String>) -> Vec<&Mandat> {
        let date = match date {
            None => None,
            Some(ref date) => Some(date.as_ref()),
        };
        self.mandats(date)
    }

    field photo() -> Option<&PhotoDepute> {
        self.photo.as_ref()
    }

    field profession() -> Option<&Profession> {
        self.profession.as_ref()
    }

    field uid() -> &str {
        self.uid()
    }

    field uri_hatvp() -> Option<&str> {
        match &self.uri_hatvp {
            None => None,
            Some(ref uri_hatvp) => Some(uri_hatvp.as_ref()),
        }
    }
});

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Acteurs {
    #[serde(rename = "acteur")]
    pub acteurs: Vec<Acteur>,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct EtatCivil {
    #[serde(rename = "dateDeces")]
    // TODO: Date<FixedOffset> type is not able to deserialize dates (`YYYY-MM-DD`).
    pub date_deces: Option<String>,
    pub ident: Ident,
    #[serde(rename = "infoNaissance")]
    pub info_naissance: InfoNaissance,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct InfoNaissance {
    #[serde(rename = "dateNais")]
    // TODO: Date<FixedOffset> type is not able to deserialize dates (`YYYY-MM-DD`).
    pub date_nais: String,
    #[serde(rename = "depNais")]
    pub dep_nais: Option<String>,
    #[serde(rename = "paysNais")]
    pub pays_nais: Option<String>,
    #[serde(rename = "villeNais")]
    pub ville_nais: Option<String>,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct Profession {
    #[serde(rename = "libelleCourant")]
    pub libelle_courant: Option<String>,
    #[serde(rename = "socProcINSEE")]
    pub soc_pro_insee: SocProInsee,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct SocProInsee {
    #[serde(rename = "catSocPro")]
    pub cat_soc_pro: Option<String>,
    #[serde(rename = "famSocPro")]
    pub fam_soc_pro: Option<String>,
}
