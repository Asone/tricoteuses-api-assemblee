use chrono::{DateTime, FixedOffset};

use crate::acteurs::Acteur;
use crate::commun::{EmptyChoice, Indexation};
use crate::contexts::Context;
use crate::dossiers_legislatifs::DossierParlementaire;
use crate::mandats::Mandat;
use crate::organes::Organe;
use crate::serde_utils::{map_to_vec, str_to_bool};

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct AccordInternational {
    pub auteurs: AuteursLeg,
    pub classification: Classification,
    pub correction: Option<Correction>,
    #[serde(rename = "cycleDeVie")]
    pub cycle_de_vie: CycleDeVieDocument,
    #[serde(rename = "denominationStructurelle")]
    pub denomination_structurelle: String,
    pub divisions: Option<Divisions>,
    // Field documents_indexes is not present in Assemblée's JSON. It is added later.
    #[serde(default)]
    pub documents_indexes: Vec<i32>,
    #[serde(rename = "dossierRef")]
    pub dossier_ref: String,
    pub imprimerie: Option<Imprimerie>,
    pub indexation: Option<Indexation>,
    pub legislature: Option<String>, // TODO: Convert to u8.
    pub notice: Notice,
    pub provenance: Option<String>,
    pub redacteur: Option<EmptyChoice>,
    pub titres: Titres,
    pub uid: String,
}

impl AccordInternational {
    pub fn index_divisions(&mut self, index: &mut i32, documents_indexes: &mut Vec<i32>) {
        if let Some(ref mut divisions) = self.divisions {
            for division in &mut divisions.divisions {
                documents_indexes.push(*index);
                division.index_document(index);
            }
        }
        self.documents_indexes = documents_indexes.to_owned();
    }
}

graphql_object!(AccordInternational: Context |&self| {
    field auteurs() -> &Vec<AuteurLeg> {
        &self.auteurs.auteurs
    }

    field classification() -> &Classification {
        &self.classification
    }

    field correction() -> &Option<Correction> {
        &self.correction
    }

    field cycle_de_vie() -> &CycleDeVieDocument {
        &self.cycle_de_vie
    }

    field denomination_structurelle() -> &str {
        &self.denomination_structurelle
    }

    // Field `divisions` is not used, because document is flattened before being sent with GraphQL.
    // field divisions() -> Vec<&Document> {
    //     match &self.divisions {
    //         None => vec![],
    //         Some(ref divisions) => divisions.divisions.iter().collect(),
    //     }
    // }

    field documents_indexes() -> &Vec<i32> {
        &self.documents_indexes
    }

    field dossier(&executor) -> Option<&DossierParlementaire> {
        executor.context().get_dossier_parlementaire_at_uid(&self.dossier_ref)
    }

    field dossier_ref() -> &str {
        &self.dossier_ref
    }

    field imprimerie() -> &Option<Imprimerie> {
        &self.imprimerie
    }

    field indexation() -> &Option<Indexation> {
        &self.indexation
    }

    field legislature() -> Option<&str> {
        match &self.legislature {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field notice() -> &Notice {
        &self.notice
    }

    field provenance() -> Option<&str> {
        match &self.provenance {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field titres() -> &Titres {
        &self.titres
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct ActeurCosignataire {
    #[serde(rename = "acteurRef")]
    acteur_ref: String,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct ActeurLeg {
    #[serde(rename = "acteurRef")]
    acteur_ref: String,
    #[serde(rename = "mandatRef")]
    mandat_ref: Option<String>,
    qualite: String,
}

graphql_object!(ActeurLeg: Context |&self| {
    field acteur(&executor) -> Option<&Acteur> {
        let context = &executor.context();
        context.get_acteur_at_uid(&self.acteur_ref)
    }

    field acteur_ref() -> &str {
        &self.acteur_ref
    }

    field mandat(&executor) -> Option<&Mandat> {
        match &self.mandat_ref {
            None => None,
            Some(ref mandat_ref) => {
                let context = &executor.context();
                match context.get_acteur_at_uid(&self.acteur_ref) {
                    None => None,
                    Some(ref acteur) => {
                        for mandat in &acteur.mandats.mandats {
                            if mandat_ref == mandat.uid() {
                                return Some(mandat)
                            }
                        }
                        None
                    },
                }
            },
        }
    }

    field mandat_ref() -> Option<&str> {
        match &self.mandat_ref {
            None => None,
            Some(ref mandat_ref) => Some(mandat_ref.as_str()),
        }
    }

    field qualite() -> &str {
        &self.qualite
    }
});

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct AmendementsCommission {
    #[serde(deserialize_with = "map_to_vec", rename = "commission")]
    pub commissions: Vec<Commission>,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct AmendementsSeance {
    #[serde(deserialize_with = "str_to_bool")]
    pub amendable: bool,
    #[serde(rename = "dateLimiteDepot")]
    pub date_limite_depot: Option<EmptyChoice>,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct AuteurLeg {
    acteur: Option<ActeurLeg>,
    organe: Option<OrganeLeg>,
}

graphql_object!(AuteurLeg: Context |&self| {
    field acteur() -> &Option<ActeurLeg> {
        &self.acteur
    }

    field organe(&executor) -> Option<&Organe> {
        match &self.organe {
            None => None,
            Some(ref organe_leg) => {
                let context = &executor.context();
                context.get_organe_at_uid(&organe_leg.organe_ref)
            },
        }
    }

    field organe_ref() -> Option<&str> {
        match &self.organe {
            None => None,
            Some(ref organe_leg) => Some(organe_leg.organe_ref.as_str()),
        }
    }
});

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct AuteursLeg {
    #[serde(deserialize_with = "map_to_vec", rename = "auteur")]
    auteurs: Vec<AuteurLeg>,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct AvisConseilEtat {
    pub auteurs: AuteursLeg,
    pub classification: Classification,
    pub correction: Option<Correction>,
    #[serde(rename = "cycleDeVie")]
    pub cycle_de_vie: CycleDeVieDocument,
    #[serde(rename = "denominationStructurelle")]
    pub denomination_structurelle: String,
    pub divisions: Option<Divisions>,
    // Field documents_indexes is not present in Assemblée's JSON. It is added later.
    #[serde(default)]
    pub documents_indexes: Vec<i32>,
    #[serde(rename = "dossierRef")]
    pub dossier_ref: String,
    pub imprimerie: Option<Imprimerie>,
    pub indexation: Option<Indexation>,
    pub legislature: Option<String>, // TODO: Convert to u8.
    pub notice: Notice,
    pub provenance: Option<String>,
    pub redacteur: Option<EmptyChoice>,
    pub titres: Titres,
    pub uid: String,
}

impl AvisConseilEtat {
    pub fn index_divisions(&mut self, index: &mut i32, documents_indexes: &mut Vec<i32>) {
        if let Some(ref mut divisions) = self.divisions {
            for division in &mut divisions.divisions {
                documents_indexes.push(*index);
                division.index_document(index);
            }
        }
        self.documents_indexes = documents_indexes.to_owned();
    }
}

graphql_object!(AvisConseilEtat: Context |&self| {
    field auteurs() -> &Vec<AuteurLeg> {
        &self.auteurs.auteurs
    }

    field classification() -> &Classification {
        &self.classification
    }

    field correction() -> &Option<Correction> {
        &self.correction
    }

    field cycle_de_vie() -> &CycleDeVieDocument {
        &self.cycle_de_vie
    }

    field denomination_structurelle() -> &str {
        &self.denomination_structurelle
    }

    // Field `divisions` is not used, because document is flattened before being sent with GraphQL.
    // field divisions() -> Vec<&Document> {
    //     match &self.divisions {
    //         None => vec![],
    //         Some(ref divisions) => divisions.divisions.iter().collect(),
    //     }
    // }

    field documents_indexes() -> &Vec<i32> {
        &self.documents_indexes
    }

    field dossier(&executor) -> Option<&DossierParlementaire> {
        executor.context().get_dossier_parlementaire_at_uid(&self.dossier_ref)
    }

    field dossier_ref() -> &str {
        &self.dossier_ref
    }

    field imprimerie() -> &Option<Imprimerie> {
        &self.imprimerie
    }

    field indexation() -> &Option<Indexation> {
        &self.indexation
    }

    field legislature() -> Option<&str> {
        match &self.legislature {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field notice() -> &Notice {
        &self.notice
    }

    field provenance() -> Option<&str> {
        match &self.provenance {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field titres() -> &Titres {
        &self.titres
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct ChronoDocument {
    #[serde(rename = "dateCreation")]
    date_creation: Option<DateTime<FixedOffset>>,
    #[serde(rename = "dateDepot")]
    date_depot: Option<DateTime<FixedOffset>>,
    #[serde(rename = "datePublication")]
    date_publication: Option<DateTime<FixedOffset>>,
    #[serde(rename = "datePublicationWeb")]
    date_publication_web: Option<DateTime<FixedOffset>>,
}

#[derive(Clone, Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct ClasseDocument {
    pub code: CodeClasseDocument,
    pub libelle: String,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct Classification {
    pub famille: Option<Famille>,
    #[serde(rename = "sousType")]
    pub sous_type: Option<SousType>,
    #[serde(rename = "statutAdoption")]
    pub statut_adoption: Option<StatutAdoption>,
    #[serde(rename = "type")]
    pub type_classification: TypeDocument,
}

#[derive(Clone, Copy, Debug, Deserialize, GraphQLEnum)]
pub enum CodeClasseDocument {
    /// Avis
    AVIS,
    /// Proposition de loi
    PIONLOI,
    /// Proposition de résolution
    PIONRES,
    /// Projet de loi
    PRJLOI,
    /// Rapport d'information
    RAPINF,
    /// Rapport
    RAPPORT,
}

#[derive(Clone, Copy, Debug, Deserialize, GraphQLEnum)]
pub enum CodeDepotDocument {
    /// Initiative en 1er Dépôt
    INITDEP,
    /// Initiative en Navette
    INITNAV,
    /// Rapport autonome
    RAPAUT,
    /// Rapport sur une initiative
    RAPINIT,
    /// Rapport sur un texte de commission
    RAPTACOM,
}

#[derive(Clone, Copy, Debug, Deserialize, GraphQLEnum)]
pub enum CodeEspeceDocument {
    /// en application de l'article 151-5 du règlement
    APPART1515,
    /// en application de Article 34-1 de la Constitution
    APPART341,
    /// sur l'application des lois
    APPLOI,
    /// sur des actes de l'Union européenne
    AUE,
    /// autorisant la ratification d'une convention
    AUTRATCONV,
    /// tendant à la création d'une commission d'enquête
    COMENQ,
    /// constitutionnelle
    CONST,
    /// divers
    DIVERS,
    /// d'enquête
    ENQU,
    /// de finances
    FIN,
    /// de finances rectificative
    FINRECT,
    /// de financement de la sécurité sociale
    FINSSOC,
    /// d'une mission d'information constituée au sein d'une commission permanente
    MINFOCOMPER,
    /// modifiant le Règlement de l'Assemblée nationale
    MODREGLTAN,
    /// des offices parlementaires ou délégations mixtes
    OFFPARL,
    /// organique
    ORG,
    /// tel quel
    PRPDIT,
    /// de règlement du budget et d'approbation des comptes
    RGLTBUDG,
    /// sur les travaux conduits par les institutions européennes
    TVXINSTITEUROP,
}

#[derive(Clone, Copy, Debug, Deserialize, GraphQLEnum)]
pub enum CodeSousTypeDocument {
    /// en application de l'article 151-5 du règlement
    APPART1515,
    /// en application de Article 34-1 de la Constitution
    APPART341,
    /// sur l'application des lois
    APPLOI,
    /// sur des actes de l'Union européenne
    AUE,
    /// autorisant la ratification d'une convention
    AUTRATCONV,
    /// tendant à la création d'une commission d'enquête
    COMENQ,
    /// Texte comparatif
    COMPA,
    /// constitutionnelle
    CONST,
    /// divers
    DIVERS,
    /// d'enquête
    ENQU,
    /// de finances
    FIN,
    /// de finances rectificative
    FINRECT,
    /// de financement de la sécurité sociale
    FINSSOC,
    /// d'une mission d'information constituée au sein d'une commission permanente
    MINFOCOMPER,
    /// modifiant le Règlement de l'Assemblée nationale
    MODREGLTAN,
    /// des offices parlementaires ou délégations mixtes
    OFFPARL,
    /// organique
    ORG,
    /// tel quel
    PRPDIT,
    /// de règlement du budget et d'approbation des comptes
    RGLTBUDG,
    /// sur les travaux conduits par les institutions européennes
    TVXINSTITEUROP,
}

#[derive(Clone, Copy, Debug, Deserialize, GraphQLEnum)]
pub enum CodeTypeDocument {
    /// Accord international
    ACIN,
    /// Avis
    AVIS,
    /// Avis du Conseil d'Etat
    AVCE,
    /// Etude d'impact
    ETDI,
    /// Proposition de loi
    PION,
    /// Proposition de résolution
    PNRE,
    /// Projet de loi
    PRJL,
    /// Rapport
    RAPP,
    /// Rapport d'information
    RINF,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Commission {
    #[serde(deserialize_with = "str_to_bool")]
    pub amendable: bool,
    #[serde(rename = "dateLimiteDepot")]
    pub date_limite_depot: Option<EmptyChoice>,
    #[serde(rename = "organeRef")]
    pub organe_ref: String,
}

graphql_object!(Commission: Context |&self| {
    field amendable() -> bool {
        self.amendable
    }

    field organe(&executor) -> Option<&Organe> {
        let context = &executor.context();
        context.get_organe_at_uid(&self.organe_ref)
    }

    field organe_ref() -> &str {
        &self.organe_ref
    }
});

#[derive(Clone, Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct Correction {
    #[serde(rename = "niveauCorrection")]
    pub niveau_correction: Option<String>,
    #[serde(rename = "typeCorrection")]
    pub type_correction: String,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Cosignataire {
    pub acteur: Option<ActeurCosignataire>,
    #[serde(rename = "dateCosignature")]
    // TODO: Date<FixedOffset> type is not able to deserialize dates with timezone (`YYYY-MM-DD±HH:MM`).
    pub date_cosignature: String,
    #[serde(rename = "dateRetraitCosignature")]
    // TODO: Date<FixedOffset> type is not able to deserialize dates with timezone (`YYYY-MM-DD±HH:MM`).
    pub date_retrait_cosignature: Option<String>,
    #[serde(deserialize_with = "str_to_bool")]
    pub edite: bool,
    pub organe: Option<OrganeCosignataire>,
}

graphql_object!(Cosignataire: Context |&self| {
    field acteur(&executor) -> Option<&Acteur> {
        match &self.acteur {
            None => None,
            Some(ref acteur) => {
                let context = &executor.context();
                context.get_acteur_at_uid(&acteur.acteur_ref)
            },
        }
    }

    field acteur_ref() -> Option<&str> {
        match &self.acteur {
            None => None,
            Some(ref acteur) => Some(acteur.acteur_ref.as_str()),
        }
    }

    field date_cosignature() -> &str {
        &self.date_cosignature
    }

    field date_retrait_cosignature() -> Option<&str> {
        match &self.date_retrait_cosignature {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field edite() -> bool {
        self.edite
    }

    field organe() -> &Option<OrganeCosignataire> {
        &self.organe
    }
});

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Cosignataires {
    #[serde(deserialize_with = "map_to_vec", rename = "coSignataire")]
    pub cosignataires: Vec<Cosignataire>,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct CycleDeVieDocument {
    pub chrono: ChronoDocument,
}

#[derive(Clone, Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct DepotDocument {
    pub code: CodeDepotDocument,
    pub libelle: String,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct DepotAmendements {
    #[serde(rename = "amendementsCommission")]
    pub amendements_commission: Option<AmendementsCommission>,
    #[serde(rename = "amendementsSeance")]
    pub amendements_seance: AmendementsSeance,
}

graphql_object!(DepotAmendements: Context |&self| {
    field amendements_commission() -> Vec<&Commission> {
        match &self.amendements_commission {
            None => vec![],
            Some(ref amendements_commission) => amendements_commission.commissions.iter().collect(),
        }
    }

    field amendements_seance() -> &AmendementsSeance {
        &self.amendements_seance
    }
});

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Divisions {
    #[serde(deserialize_with = "map_to_vec", rename = "division")]
    pub divisions: Vec<Document>,
}

#[derive(Debug, Deserialize)]
#[serde(tag = "@xsi:type")]
pub enum Document {
    #[serde(rename = "accordInternational_Type")]
    AccordInternational(AccordInternational),
    #[serde(rename = "avisConseilEtat_Type")]
    AvisConseilEtat(AvisConseilEtat),
    #[serde(rename = "documentEtudeImpact_Type")]
    DocumentEtudeImpact(DocumentEtudeImpact),
    #[serde(rename = "rapportParlementaire_Type")]
    RapportParlementaire(RapportParlementaire),
    #[serde(rename = "texteLoi_Type")]
    TexteLoi(TexteLoi),
}

impl Document {
    //     pub fn auteurs(&self) -> &Vec<AuteurLeg> {
    //         match *self {
    //             Document::AccordInternational(AccordInternational { ref auteurs, .. })
    //             | Document::AvisConseilEtat(AvisConseilEtat { ref auteurs, .. })
    //             | Document::DocumentEtudeImpact(DocumentEtudeImpact { ref auteurs, .. })
    //             | Document::RapportParlementaire(RapportParlementaire { ref auteurs, .. })
    //             | Document::TexteLoi(TexteLoi { ref auteurs, .. }) => &auteurs.auteurs,
    //         }
    //     }

    //     pub fn classification(&self) -> &Classification {
    //         match *self {
    //             Document::AccordInternational(AccordInternational {
    //                 ref classification, ..
    //             })
    //             | Document::AvisConseilEtat(AvisConseilEtat {
    //                 ref classification, ..
    //             })
    //             | Document::DocumentEtudeImpact(DocumentEtudeImpact {
    //                 ref classification, ..
    //             })
    //             | Document::RapportParlementaire(RapportParlementaire {
    //                 ref classification, ..
    //             })
    //             | Document::TexteLoi(TexteLoi {
    //                 ref classification, ..
    //             }) => &classification,
    //         }
    //     }

    //     pub fn correction(&self) -> &Option<Correction> {
    //         match *self {
    //             Document::AccordInternational(AccordInternational { ref correction, .. })
    //             | Document::AvisConseilEtat(AvisConseilEtat { ref correction, .. })
    //             | Document::DocumentEtudeImpact(DocumentEtudeImpact { ref correction, .. })
    //             | Document::RapportParlementaire(RapportParlementaire { ref correction, .. })
    //             | Document::TexteLoi(TexteLoi { ref correction, .. }) => &correction,
    //         }
    //     }

    //     pub fn cycle_de_vie(&self) -> &CycleDeVieDocument {
    //         match *self {
    //             Document::AccordInternational(AccordInternational {
    //                 ref cycle_de_vie, ..
    //             })
    //             | Document::AvisConseilEtat(AvisConseilEtat {
    //                 ref cycle_de_vie, ..
    //             })
    //             | Document::DocumentEtudeImpact(DocumentEtudeImpact {
    //                 ref cycle_de_vie, ..
    //             })
    //             | Document::RapportParlementaire(RapportParlementaire {
    //                 ref cycle_de_vie, ..
    //             })
    //             | Document::TexteLoi(TexteLoi {
    //                 ref cycle_de_vie, ..
    //             }) => &cycle_de_vie,
    //         }
    //     }

    //     pub fn denomination_structurelle(&self) -> &str {
    //         match *self {
    //             Document::AccordInternational(AccordInternational {
    //                 ref denomination_structurelle,
    //                 ..
    //             })
    //             | Document::AvisConseilEtat(AvisConseilEtat {
    //                 ref denomination_structurelle,
    //                 ..
    //             })
    //             | Document::DocumentEtudeImpact(DocumentEtudeImpact {
    //                 ref denomination_structurelle,
    //                 ..
    //             })
    //             | Document::RapportParlementaire(RapportParlementaire {
    //                 ref denomination_structurelle,
    //                 ..
    //             })
    //             | Document::TexteLoi(TexteLoi {
    //                 ref denomination_structurelle,
    //                 ..
    //             }) => &denomination_structurelle,
    //         }
    //     }

    pub fn divisions(&self) -> &Option<Divisions> {
        match *self {
            Document::AccordInternational(AccordInternational { ref divisions, .. })
            | Document::AvisConseilEtat(AvisConseilEtat { ref divisions, .. })
            | Document::DocumentEtudeImpact(DocumentEtudeImpact { ref divisions, .. })
            | Document::RapportParlementaire(RapportParlementaire { ref divisions, .. })
            | Document::TexteLoi(TexteLoi { ref divisions, .. }) => &divisions,
        }
    }

    //     pub fn dossier_ref(&self) -> &str {
    //         match *self {
    //             Document::AccordInternational(AccordInternational {
    //                 ref dossier_ref, ..
    //             })
    //             | Document::AvisConseilEtat(AvisConseilEtat {
    //                 ref dossier_ref, ..
    //             })
    //             | Document::DocumentEtudeImpact(DocumentEtudeImpact {
    //                 ref dossier_ref, ..
    //             })
    //             | Document::RapportParlementaire(RapportParlementaire {
    //                 ref dossier_ref, ..
    //             })
    //             | Document::TexteLoi(TexteLoi {
    //                 ref dossier_ref, ..
    //             }) => &dossier_ref,
    //         }
    //     }

    pub fn flatten_document(&self) -> Vec<&Document> {
        let mut documents: Vec<&Document> = vec![&self];
        if let Some(ref divisions) = self.divisions() {
            for division in &divisions.divisions {
                documents.extend(division.flatten_document());
            }
        }
        documents
    }

    //     pub fn imprimerie(&self) -> &Option<Imprimerie> {
    //         match *self {
    //             Document::AccordInternational(AccordInternational { ref imprimerie, .. })
    //             | Document::AvisConseilEtat(AvisConseilEtat { ref imprimerie, .. })
    //             | Document::DocumentEtudeImpact(DocumentEtudeImpact { ref imprimerie, .. })
    //             | Document::RapportParlementaire(RapportParlementaire { ref imprimerie, .. })
    //             | Document::TexteLoi(TexteLoi { ref imprimerie, .. }) => &imprimerie,
    //         }
    //     }

    pub fn index_document(&mut self, index: &mut i32) {
        let mut documents_indexes: Vec<i32> = vec![*index];
        *index += 1;
        match *self {
            Document::AccordInternational(ref mut document) => {
                document.index_divisions(index, &mut documents_indexes)
            }
            Document::AvisConseilEtat(ref mut document) => {
                document.index_divisions(index, &mut documents_indexes)
            }
            Document::DocumentEtudeImpact(ref mut document) => {
                document.index_divisions(index, &mut documents_indexes)
            }
            Document::RapportParlementaire(ref mut document) => {
                document.index_divisions(index, &mut documents_indexes)
            }
            Document::TexteLoi(ref mut document) => {
                document.index_divisions(index, &mut documents_indexes)
            }
        }
    }

    //     pub fn indexation(&self) -> &Option<Indexation> {
    //         match *self {
    //             Document::AccordInternational(AccordInternational { ref indexation, .. })
    //             | Document::AvisConseilEtat(AvisConseilEtat { ref indexation, .. })
    //             | Document::DocumentEtudeImpact(DocumentEtudeImpact { ref indexation, .. })
    //             | Document::RapportParlementaire(RapportParlementaire { ref indexation, .. })
    //             | Document::TexteLoi(TexteLoi { ref indexation, .. }) => &indexation,
    //         }
    //     }

    //     pub fn legislature(&self) -> Option<&str> {
    //         match *self {
    //             Document::AccordInternational(AccordInternational {
    //                 ref legislature, ..
    //             })
    //             | Document::AvisConseilEtat(AvisConseilEtat {
    //                 ref legislature, ..
    //             })
    //             | Document::DocumentEtudeImpact(DocumentEtudeImpact {
    //                 ref legislature, ..
    //             })
    //             | Document::RapportParlementaire(RapportParlementaire {
    //                 ref legislature, ..
    //             })
    //             | Document::TexteLoi(TexteLoi {
    //                 ref legislature, ..
    //             }) => match legislature {
    //                 None => None,
    //                 Some(ref legislature) => Some(&legislature),
    //             },
    //         }
    //     }

    //     pub fn notice(&self) -> &Notice {
    //         match *self {
    //             Document::AccordInternational(AccordInternational { ref notice, .. })
    //             | Document::AvisConseilEtat(AvisConseilEtat { ref notice, .. })
    //             | Document::DocumentEtudeImpact(DocumentEtudeImpact { ref notice, .. })
    //             | Document::RapportParlementaire(RapportParlementaire { ref notice, .. })
    //             | Document::TexteLoi(TexteLoi { ref notice, .. }) => &notice,
    //         }
    //     }

    //     pub fn provenance(&self) -> Option<&str> {
    //         match *self {
    //             Document::AccordInternational(AccordInternational { ref provenance, .. })
    //             | Document::AvisConseilEtat(AvisConseilEtat { ref provenance, .. })
    //             | Document::DocumentEtudeImpact(DocumentEtudeImpact { ref provenance, .. })
    //             | Document::RapportParlementaire(RapportParlementaire { ref provenance, .. })
    //             | Document::TexteLoi(TexteLoi { ref provenance, .. }) => match provenance {
    //                 None => None,
    //                 Some(ref provenance) => Some(&provenance),
    //             },
    //         }
    //     }

    //     pub fn titres(&self) -> &Titres {
    //         match *self {
    //             Document::AccordInternational(AccordInternational { ref titres, .. })
    //             | Document::AvisConseilEtat(AvisConseilEtat { ref titres, .. })
    //             | Document::DocumentEtudeImpact(DocumentEtudeImpact { ref titres, .. })
    //             | Document::RapportParlementaire(RapportParlementaire { ref titres, .. })
    //             | Document::TexteLoi(TexteLoi { ref titres, .. }) => &titres,
    //         }
    //     }

    pub fn uid(&self) -> &str {
        match *self {
            Document::AccordInternational(AccordInternational { ref uid, .. })
            | Document::AvisConseilEtat(AvisConseilEtat { ref uid, .. })
            | Document::DocumentEtudeImpact(DocumentEtudeImpact { ref uid, .. })
            | Document::RapportParlementaire(RapportParlementaire { ref uid, .. })
            | Document::TexteLoi(TexteLoi { ref uid, .. }) => &uid,
        }
    }
}

// Using graphql_union because instance_resolvers of graphql_interface does not work with Juniper 0.9.2.
// graphql_interface!(Document: Context |&self| {
graphql_union!(Document: Context |&self| {
    // field auteurs() -> &Vec<AuteurLeg> {
    //     self.auteurs()
    // }

    // field classification() -> &Classification {
    //     self.classification()
    // }

    // field correction() -> &Option<Correction> {
    //     self.correction()
    // }

    // field cycle_de_vie() -> &CycleDeVieDocument {
    //     self.cycle_de_vie()
    // }

    // field denomination_structurelle() -> &str {
    //     self.denomination_structurelle()
    // }

    // field documents_indexes() -> &Vec<i32> {
    //     &self.documents_indexes
    // }

    // field dossier(&executor) -> Option<&DossierParlementaire> {
    //     executor.context().get_dossier_parlementaire_at_uid(&self.dossier_ref)
    // }

    // field dossier_ref() -> &str {
    //     self.dossier_ref()
    // }

    // field imprimerie() -> &Option<Imprimerie> {
    //     self.imprimerie()
    // }

    // field indexation() -> &Option<Indexation> {
    //     self.indexation()
    // }

    // field legislature() -> Option<&str> {
    //     self.legislature()
    // }

    // field notice() -> &Notice {
    //     self.notice()
    // }

    // field provenance() -> Option<&str> {
    //     self.provenance()
    // }

    // field titres() -> &Titres {
    //     self.titres()
    // }

    // field uid() -> &str {
    //     self.uid()
    // }

    instance_resolvers: |_| {
        &AccordInternational => match *self {
            Document::AccordInternational(ref document) => Some(document),
            _ => None,
        },
        &AvisConseilEtat => match *self {
            Document::AvisConseilEtat(ref document) => Some(document),
            _ => None,
        },
        &DocumentEtudeImpact => match *self {
            Document::DocumentEtudeImpact(ref document) => Some(document),
            _ => None,
        },
        &RapportParlementaire => match *self {
            Document::RapportParlementaire(ref document) => Some(document),
            _ => None,
        },
        &TexteLoi => match *self {
            Document::TexteLoi(ref document) => Some(document),
            _ => None,
        },
    }
});

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct DocumentEtudeImpact {
    pub auteurs: AuteursLeg,
    pub classification: Classification,
    pub correction: Option<Correction>,
    #[serde(rename = "cycleDeVie")]
    pub cycle_de_vie: CycleDeVieDocument,
    #[serde(rename = "denominationStructurelle")]
    pub denomination_structurelle: String,
    pub divisions: Option<Divisions>,
    // Field documents_indexes is not present in Assemblée's JSON. It is added later.
    #[serde(default)]
    pub documents_indexes: Vec<i32>,
    #[serde(rename = "dossierRef")]
    pub dossier_ref: String,
    pub imprimerie: Option<Imprimerie>,
    pub indexation: Option<Indexation>,
    pub legislature: Option<String>, // TODO: Convert to u8.
    pub notice: Notice,
    pub provenance: Option<String>,
    pub redacteur: Option<EmptyChoice>,
    pub titres: Titres,
    pub uid: String,
}

impl DocumentEtudeImpact {
    pub fn index_divisions(&mut self, index: &mut i32, documents_indexes: &mut Vec<i32>) {
        if let Some(ref mut divisions) = self.divisions {
            for division in &mut divisions.divisions {
                documents_indexes.push(*index);
                division.index_document(index);
            }
        }
        self.documents_indexes = documents_indexes.to_owned();
    }
}

graphql_object!(DocumentEtudeImpact: Context |&self| {
    field auteurs() -> &Vec<AuteurLeg> {
        &self.auteurs.auteurs
    }

    field classification() -> &Classification {
        &self.classification
    }

    field correction() -> &Option<Correction> {
        &self.correction
    }

    field cycle_de_vie() -> &CycleDeVieDocument {
        &self.cycle_de_vie
    }

    field denomination_structurelle() -> &str {
        &self.denomination_structurelle
    }

    // Field `divisions` is not used, because document is flattened before being sent with GraphQL.
    // field divisions() -> Vec<&Document> {
    //     match &self.divisions {
    //         None => vec![],
    //         Some(ref divisions) => divisions.divisions.iter().collect(),
    //     }
    // }

    field documents_indexes() -> &Vec<i32> {
        &self.documents_indexes
    }

    field dossier(&executor) -> Option<&DossierParlementaire> {
        executor.context().get_dossier_parlementaire_at_uid(&self.dossier_ref)
    }

    field dossier_ref() -> &str {
        &self.dossier_ref
    }

    field imprimerie() -> &Option<Imprimerie> {
        &self.imprimerie
    }

    field indexation() -> &Option<Indexation> {
        &self.indexation
    }

    field legislature() -> Option<&str> {
        match &self.legislature {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field notice() -> &Notice {
        &self.notice
    }

    field provenance() -> Option<&str> {
        match &self.provenance {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field titres() -> &Titres {
        &self.titres
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Clone, Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct EspeceDocument {
    pub code: CodeEspeceDocument,
    pub libelle: String,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct Famille {
    pub classe: ClasseDocument,
    pub depot: DepotDocument,
    pub espece: Option<EspeceDocument>,
}

#[derive(Clone, Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct Imprimerie {
    pub prix: String,
}

#[derive(Clone, Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct Notice {
    #[serde(deserialize_with = "str_to_bool", rename = "adoptionConforme")]
    pub adoption_conforme: bool,
    pub formule: Option<String>,
    #[serde(rename = "numNotice")]
    pub num_notice: Option<String>,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct OrganeCosignataire {
    #[serde(deserialize_with = "str_to_bool", rename = "etApparentes")]
    pub et_apparentes: bool,
    #[serde(rename = "organeRef")]
    organe_ref: String,
}

graphql_object!(OrganeCosignataire: Context |&self| {
    field et_apparentes() -> bool {
        self.et_apparentes
    }

    field organe(&executor) -> Option<&Organe> {
        let context = &executor.context();
        context.get_organe_at_uid(&self.organe_ref)
    }

    field organe_ref() -> &str {
        &self.organe_ref
    }
});

#[derive(Clone, Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct OrganeLeg {
    #[serde(rename = "organeRef")]
    pub organe_ref: String,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct RapportParlementaire {
    pub auteurs: AuteursLeg,
    pub classification: Classification,
    pub correction: Option<Correction>,
    #[serde(rename = "cycleDeVie")]
    pub cycle_de_vie: CycleDeVieDocument,
    #[serde(rename = "denominationStructurelle")]
    pub denomination_structurelle: String,
    pub divisions: Option<Divisions>,
    // Field documents_indexes is not present in Assemblée's JSON. It is added later.
    #[serde(default)]
    pub documents_indexes: Vec<i32>,
    #[serde(rename = "dossierRef")]
    pub dossier_ref: String,
    pub imprimerie: Option<Imprimerie>,
    pub indexation: Option<Indexation>,
    pub legislature: Option<String>, // TODO: Convert to u8.
    pub notice: Notice,
    pub provenance: Option<String>,
    pub redacteur: Option<EmptyChoice>,
    pub titres: Titres,
    pub uid: String,
}

impl RapportParlementaire {
    pub fn index_divisions(&mut self, index: &mut i32, documents_indexes: &mut Vec<i32>) {
        if let Some(ref mut divisions) = self.divisions {
            for division in &mut divisions.divisions {
                documents_indexes.push(*index);
                division.index_document(index);
            }
        }
        self.documents_indexes = documents_indexes.to_owned();
    }
}

graphql_object!(RapportParlementaire: Context |&self| {
    field auteurs() -> &Vec<AuteurLeg> {
        &self.auteurs.auteurs
    }

    field classification() -> &Classification {
        &self.classification
    }

    field correction() -> &Option<Correction> {
        &self.correction
    }

    field cycle_de_vie() -> &CycleDeVieDocument {
        &self.cycle_de_vie
    }

    field denomination_structurelle() -> &str {
        &self.denomination_structurelle
    }

    // Field `divisions` is not used, because document is flattened before being sent with GraphQL.
    // field divisions() -> Vec<&Document> {
    //     match &self.divisions {
    //         None => vec![],
    //         Some(ref divisions) => divisions.divisions.iter().collect(),
    //     }
    // }

    field documents_indexes() -> &Vec<i32> {
        &self.documents_indexes
    }

    field dossier(&executor) -> Option<&DossierParlementaire> {
        executor.context().get_dossier_parlementaire_at_uid(&self.dossier_ref)
    }

    field dossier_ref() -> &str {
        &self.dossier_ref
    }

    field imprimerie() -> &Option<Imprimerie> {
        &self.imprimerie
    }

    field indexation() -> &Option<Indexation> {
        &self.indexation
    }

    field legislature() -> Option<&str> {
        match &self.legislature {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field notice() -> &Notice {
        &self.notice
    }

    field provenance() -> Option<&str> {
        match &self.provenance {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field titres() -> &Titres {
        &self.titres
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Clone, Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct SousType {
    pub code: CodeSousTypeDocument,
    pub libelle: Option<String>,
    #[serde(rename = "libelleEdition")]
    pub libelle_edition: Option<String>,
}

#[derive(Clone, Copy, Debug, Deserialize, GraphQLEnum)]
pub enum StatutAdoption {
    /// Adopté en commission
    ADOPTCOM,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct TexteLoi {
    pub auteurs: AuteursLeg,
    pub classification: Classification,
    pub correction: Option<Correction>,
    #[serde(rename = "coSignataires")]
    pub cosignataires: Option<Cosignataires>,
    #[serde(rename = "cycleDeVie")]
    pub cycle_de_vie: CycleDeVieDocument,
    #[serde(rename = "denominationStructurelle")]
    pub denomination_structurelle: String,
    #[serde(rename = "depotAmendements")]
    pub depot_amendements: Option<DepotAmendements>,
    pub divisions: Option<Divisions>,
    // Field documents_indexes is not present in Assemblée's JSON. It is added later.
    #[serde(default)]
    pub documents_indexes: Vec<i32>,
    #[serde(rename = "dossierRef")]
    pub dossier_ref: String,
    pub imprimerie: Option<Imprimerie>,
    pub indexation: Option<Indexation>,
    pub legislature: Option<String>, // TODO: Convert to u8.
    pub notice: Notice,
    pub provenance: Option<String>,
    pub redacteur: Option<EmptyChoice>,
    pub titres: Titres,
    pub uid: String,
}

impl TexteLoi {
    pub fn index_divisions(&mut self, index: &mut i32, documents_indexes: &mut Vec<i32>) {
        if let Some(ref mut divisions) = self.divisions {
            for division in &mut divisions.divisions {
                documents_indexes.push(*index);
                division.index_document(index);
            }
        }
        self.documents_indexes = documents_indexes.to_owned();
    }
}

graphql_object!(TexteLoi: Context |&self| {
    field auteurs() -> &Vec<AuteurLeg> {
        &self.auteurs.auteurs
    }

    field classification() -> &Classification {
        &self.classification
    }

    field correction() -> &Option<Correction> {
        &self.correction
    }

    field cosignataires() -> Vec<&Cosignataire> {
        match &self.cosignataires {
            None => vec![],
            Some(ref cosignataires) => cosignataires.cosignataires.iter().collect(),
        }
    }

    field cycle_de_vie() -> &CycleDeVieDocument {
        &self.cycle_de_vie
    }

    field denomination_structurelle() -> &str {
        &self.denomination_structurelle
    }

    field depot_amendements() -> &Option<DepotAmendements> {
        &self.depot_amendements
    }

    // Field `divisions` is not used, because document is flattened before being sent with GraphQL.
    // field divisions() -> Vec<&Document> {
    //     match &self.divisions {
    //         None => vec![],
    //         Some(ref divisions) => divisions.divisions.iter().collect(),
    //     }
    // }

    field documents_indexes() -> &Vec<i32> {
        &self.documents_indexes
    }

    field dossier(&executor) -> Option<&DossierParlementaire> {
        executor.context().get_dossier_parlementaire_at_uid(&self.dossier_ref)
    }

    field dossier_ref() -> &str {
        &self.dossier_ref
    }

    field imprimerie() -> &Option<Imprimerie> {
        &self.imprimerie
    }

    field indexation() -> &Option<Indexation> {
        &self.indexation
    }

    field legislature() -> Option<&str> {
        match &self.legislature {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field notice() -> &Notice {
        &self.notice
    }

    field provenance() -> Option<&str> {
        match &self.provenance {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field titres() -> &Titres {
        &self.titres
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct TextesLegislatifs {
    #[serde(rename = "document")]
    pub documents: Vec<Document>,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct Titres {
    #[serde(rename = "titrePrincipal")]
    titre_principal: String,
    #[serde(rename = "titrePrincipalCourt")]
    titre_principal_court: String,
}

#[derive(Clone, Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct TypeDocument {
    pub code: CodeTypeDocument,
    pub libelle: String,
}
