use chrono::{Date, DateTime, FixedOffset};
use std::cmp;

use crate::acteurs::Acteur;
use crate::agendas::Reunion;
use crate::commun::{Codier, EmptyChoice, InfoJo, Rapporteur};
use crate::contexts::Context;
use crate::mandats::Mandat;
use crate::organes::Organe;
use crate::points_odj::PointOdj;
use crate::scrutins::Scrutin;
use crate::serde_utils::{map_to_vec, str_to_vec};
use crate::textes_legislatifs::Document;

#[derive(Clone, Debug, Deserialize)]
#[serde(tag = "@xsi:type")]
pub enum ActeLegislatif {
    #[serde(rename = "Adoption_Europe_Type")]
    AdoptionEurope(AdoptionEurope),
    #[serde(rename = "ConclusionEtapeCC_Type")]
    ConclusionEtapeCc(ConclusionEtapeCc),
    #[serde(rename = "CreationOrganeTemporaire_Type")]
    CreationOrganeTemporaire(CreationOrganeTemporaire),
    #[serde(rename = "Decision_Type")]
    Decision(Decision),
    #[serde(rename = "DecisionMotionCensure_Type")]
    DecisionMotionCensure(DecisionMotionCensure),
    #[serde(rename = "DecisionRecevabiliteBureau_Type")]
    DecisionRecevabiliteBureau(DecisionRecevabiliteBureau),
    #[serde(rename = "DeclarationGouvernement_Type")]
    DeclarationGouvernement(DeclarationGouvernement),
    #[serde(rename = "DepotAccordInternational_Type")]
    DepotAccordInternational(DepotAccordInternational),
    #[serde(rename = "DepotAvisConseilEtat_Type")]
    DepotAvisConseilEtat(DepotAvisConseilEtat),
    #[serde(rename = "DepotInitiative_Type")]
    DepotInitiative(DepotInitiative),
    #[serde(rename = "DepotInitiativeNavette_Type")]
    DepotInitiativeNavette(DepotInitiativeNavette),
    #[serde(rename = "DepotLettreRectificative_Type")]
    DepotLettreRectificative(DepotLettreRectificative),
    #[serde(rename = "DepotMotionCensure_Type")]
    DepotMotionCensure(DepotMotionCensure),
    #[serde(rename = "DepotMotionReferendaire_Type")]
    DepotMotionReferendaire(DepotMotionReferendaire),
    #[serde(rename = "DepotRapport_Type")]
    DepotRapport(DepotRapport),
    #[serde(rename = "DiscussionCommission_Type")]
    DiscussionCommission(DiscussionCommission),
    #[serde(rename = "DiscussionSeancePublique_Type")]
    DiscussionSeancePublique(DiscussionSeancePublique),
    #[serde(rename = "Etape_Type")]
    Etape(Etape),
    #[serde(rename = "EtudeImpact_Type")]
    EtudeImpact(EtudeImpact),
    #[serde(rename = "MotionProcedure_Type")]
    MotionProcedure(MotionProcedure),
    #[serde(rename = "NominRapporteurs_Type")]
    NominRapporteurs(NominRapporteurs),
    #[serde(rename = "ProcedureAccelere_Type")]
    ProcedureAcceleree(ProcedureAcceleree),
    #[serde(rename = "Promulgation_Type")]
    Promulgation(Promulgation),
    #[serde(rename = "RenvoiCMP_Type")]
    RenvoiCmp(RenvoiCmp),
    #[serde(rename = "RenvoiPrealable_Type")]
    RenvoiPrealable(RenvoiPrealable),
    #[serde(rename = "RetraitInitiative_Type")]
    RetraitInitiative(RetraitInitiative),
    #[serde(rename = "SaisieComAvis_Type")]
    SaisieComAvis(SaisieComAvis),
    #[serde(rename = "SaisieComFond_Type")]
    SaisieComFond(SaisieComFond),
    #[serde(rename = "SaisineConseilConstit_Type")]
    SaisineConseilConstit(SaisineConseilConstit),
}

impl ActeLegislatif {
    pub fn actes_legislatifs(&self) -> Option<&[ActeLegislatif]> {
        match *self {
            ActeLegislatif::Etape(Etape {
                ref actes_legislatifs,
                ..
            }) => match &actes_legislatifs {
                None => None,
                Some(actes_legislatifs) => Some(&actes_legislatifs.actes),
            },
            _ => None,
        }
    }

    //     pub fn code_acte(&self) -> &str {
    //         match *self {
    //             ActeLegislatif::AdoptionEurope(AdoptionEurope { ref code_acte, .. })
    //             | ActeLegislatif::ConclusionEtapeCc(ConclusionEtapeCc { ref code_acte, .. })
    //             | ActeLegislatif::CreationOrganeTemporaire(CreationOrganeTemporaire {
    //                 ref code_acte,
    //                 ..
    //             })
    //             | ActeLegislatif::Decision(Decision { ref code_acte, .. })
    //             | ActeLegislatif::DecisionMotionCensure(DecisionMotionCensure {
    //                 ref code_acte, ..
    //             })
    //             | ActeLegislatif::DecisionRecevabiliteBureau(DecisionRecevabiliteBureau {
    //                 ref code_acte,
    //                 ..
    //             })
    //             | ActeLegislatif::DeclarationGouvernement(DeclarationGouvernement {
    //                 ref code_acte,
    //                 ..
    //             })
    //             | ActeLegislatif::DepotAccordInternational(DepotAccordInternational {
    //                 ref code_acte,
    //                 ..
    //             })
    //             | ActeLegislatif::DepotAvisConseilEtat(DepotAvisConseilEtat {
    //                 ref code_acte, ..
    //             })
    //             | ActeLegislatif::DepotInitiative(DepotInitiative { ref code_acte, .. })
    //             | ActeLegislatif::DepotInitiativeNavette(DepotInitiativeNavette {
    //                 ref code_acte,
    //                 ..
    //             })
    //             | ActeLegislatif::DepotLettreRectificative(DepotLettreRectificative {
    //                 ref code_acte,
    //                 ..
    //             })
    //             | ActeLegislatif::DepotMotionCensure(DepotMotionCensure { ref code_acte, .. })
    //             | ActeLegislatif::DepotMotionReferendaire(DepotMotionReferendaire {
    //                 ref code_acte,
    //                 ..
    //             })
    //             | ActeLegislatif::DepotRapport(DepotRapport { ref code_acte, .. })
    //             | ActeLegislatif::DiscussionCommission(DiscussionCommission {
    //                 ref code_acte, ..
    //             })
    //             | ActeLegislatif::DiscussionSeancePublique(DiscussionSeancePublique {
    //                 ref code_acte,
    //                 ..
    //             })
    //             | ActeLegislatif::Etape(Etape { ref code_acte, .. })
    //             | ActeLegislatif::EtudeImpact(EtudeImpact { ref code_acte, .. })
    //             | ActeLegislatif::MotionProcedure(MotionProcedure { ref code_acte, .. })
    //             | ActeLegislatif::NominRapporteurs(NominRapporteurs { ref code_acte, .. })
    //             | ActeLegislatif::ProcedureAcceleree(ProcedureAcceleree { ref code_acte, .. })
    //             | ActeLegislatif::Promulgation(Promulgation { ref code_acte, .. })
    //             | ActeLegislatif::RenvoiCmp(RenvoiCmp { ref code_acte, .. })
    //             | ActeLegislatif::RenvoiPrealable(RenvoiPrealable { ref code_acte, .. })
    //             | ActeLegislatif::RetraitInitiative(RetraitInitiative { ref code_acte, .. })
    //             | ActeLegislatif::SaisieComAvis(SaisieComAvis { ref code_acte, .. })
    //             | ActeLegislatif::SaisieComFond(SaisieComFond { ref code_acte, .. })
    //             | ActeLegislatif::SaisineConseilConstit(SaisineConseilConstit {
    //                 ref code_acte, ..
    //             }) => &code_acte,
    //         }
    //     }

    pub fn date_acte(&self) -> Option<&DateTime<FixedOffset>> {
        match *self {
            ActeLegislatif::AdoptionEurope(AdoptionEurope { ref date_acte, .. })
            | ActeLegislatif::ConclusionEtapeCc(ConclusionEtapeCc { ref date_acte, .. })
            | ActeLegislatif::CreationOrganeTemporaire(CreationOrganeTemporaire {
                ref date_acte,
                ..
            })
            | ActeLegislatif::Decision(Decision { ref date_acte, .. })
            | ActeLegislatif::DecisionMotionCensure(DecisionMotionCensure {
                ref date_acte, ..
            })
            | ActeLegislatif::DecisionRecevabiliteBureau(DecisionRecevabiliteBureau {
                ref date_acte,
                ..
            })
            | ActeLegislatif::DeclarationGouvernement(DeclarationGouvernement {
                ref date_acte,
                ..
            })
            | ActeLegislatif::DepotAccordInternational(DepotAccordInternational {
                ref date_acte,
                ..
            })
            | ActeLegislatif::DepotAvisConseilEtat(DepotAvisConseilEtat {
                ref date_acte, ..
            })
            | ActeLegislatif::DepotInitiative(DepotInitiative { ref date_acte, .. })
            | ActeLegislatif::DepotInitiativeNavette(DepotInitiativeNavette {
                ref date_acte,
                ..
            })
            | ActeLegislatif::DepotLettreRectificative(DepotLettreRectificative {
                ref date_acte,
                ..
            })
            | ActeLegislatif::DepotMotionCensure(DepotMotionCensure { ref date_acte, .. })
            | ActeLegislatif::DepotMotionReferendaire(DepotMotionReferendaire {
                ref date_acte,
                ..
            })
            | ActeLegislatif::DepotRapport(DepotRapport { ref date_acte, .. })
            | ActeLegislatif::DiscussionCommission(DiscussionCommission {
                ref date_acte, ..
            })
            | ActeLegislatif::DiscussionSeancePublique(DiscussionSeancePublique {
                ref date_acte,
                ..
            })
            | ActeLegislatif::EtudeImpact(EtudeImpact { ref date_acte, .. })
            | ActeLegislatif::MotionProcedure(MotionProcedure { ref date_acte, .. })
            | ActeLegislatif::NominRapporteurs(NominRapporteurs { ref date_acte, .. })
            | ActeLegislatif::ProcedureAcceleree(ProcedureAcceleree { ref date_acte, .. })
            | ActeLegislatif::Promulgation(Promulgation { ref date_acte, .. })
            | ActeLegislatif::RenvoiCmp(RenvoiCmp { ref date_acte, .. })
            | ActeLegislatif::RenvoiPrealable(RenvoiPrealable { ref date_acte, .. })
            | ActeLegislatif::RetraitInitiative(RetraitInitiative { ref date_acte, .. })
            | ActeLegislatif::SaisieComAvis(SaisieComAvis { ref date_acte, .. })
            | ActeLegislatif::SaisieComFond(SaisieComFond { ref date_acte, .. })
            | ActeLegislatif::SaisineConseilConstit(SaisineConseilConstit {
                ref date_acte, ..
            }) => match &date_acte {
                None => None,
                Some(ref date_acte) => Some(&date_acte),
            },
            ActeLegislatif::Etape(_) => None,
        }
    }

    pub fn distance_min_date_actes_legislatifs_futurs(
        &self,
        date: Date<FixedOffset>,
    ) -> Option<i64> {
        let mut min: Option<i64> = match self.date_acte() {
            None => None,
            Some(date_time) => {
                let days = -date.signed_duration_since(date_time.date()).num_days();
                if days >= 0 {
                    Some(days)
                } else {
                    None
                }
            }
        };
        if let Some(actes_legislatifs) = self.actes_legislatifs() {
            for acte_legislatif in actes_legislatifs {
                let min_acte = acte_legislatif.distance_min_date_actes_legislatifs_futurs(date);
                min = match (min, min_acte) {
                    (Some(min), Some(min_acte)) => Some(cmp::min(min, min_acte)),
                    (None, Some(min_acte)) => Some(min_acte),
                    _ => min,
                }
            }
        }
        min
    }

    pub fn distance_min_date_actes_legislatifs_passes(
        &self,
        date: Date<FixedOffset>,
    ) -> Option<i64> {
        let mut min: Option<i64> = match self.date_acte() {
            None => None,
            Some(date_time) => {
                let days = date.signed_duration_since(date_time.date()).num_days();
                if days >= 0 {
                    Some(days)
                } else {
                    None
                }
            }
        };
        if let Some(actes_legislatifs) = self.actes_legislatifs() {
            for acte_legislatif in actes_legislatifs {
                let min_acte = acte_legislatif.distance_min_date_actes_legislatifs_passes(date);
                min = match (min, min_acte) {
                    (Some(min), Some(min_acte)) => Some(cmp::min(min, min_acte)),
                    (None, Some(min_acte)) => Some(min_acte),
                    _ => min,
                }
            }
        }
        min
    }

    //     pub fn libelle_acte(&self) -> &LibelleActe {
    //         match *self {
    //             ActeLegislatif::AdoptionEurope(AdoptionEurope {
    //                 ref libelle_acte, ..
    //             })
    //             | ActeLegislatif::ConclusionEtapeCc(ConclusionEtapeCc {
    //                 ref libelle_acte, ..
    //             })
    //             | ActeLegislatif::CreationOrganeTemporaire(CreationOrganeTemporaire {
    //                 ref libelle_acte,
    //                 ..
    //             })
    //             | ActeLegislatif::Decision(Decision {
    //                 ref libelle_acte, ..
    //             })
    //             | ActeLegislatif::DecisionMotionCensure(DecisionMotionCensure {
    //                 ref libelle_acte,
    //                 ..
    //             })
    //             | ActeLegislatif::DecisionRecevabiliteBureau(DecisionRecevabiliteBureau {
    //                 ref libelle_acte,
    //                 ..
    //             })
    //             | ActeLegislatif::DeclarationGouvernement(DeclarationGouvernement {
    //                 ref libelle_acte,
    //                 ..
    //             })
    //             | ActeLegislatif::DepotAccordInternational(DepotAccordInternational {
    //                 ref libelle_acte,
    //                 ..
    //             })
    //             | ActeLegislatif::DepotAvisConseilEtat(DepotAvisConseilEtat {
    //                 ref libelle_acte, ..
    //             })
    //             | ActeLegislatif::DepotInitiative(DepotInitiative {
    //                 ref libelle_acte, ..
    //             })
    //             | ActeLegislatif::DepotInitiativeNavette(DepotInitiativeNavette {
    //                 ref libelle_acte,
    //                 ..
    //             })
    //             | ActeLegislatif::DepotLettreRectificative(DepotLettreRectificative {
    //                 ref libelle_acte,
    //                 ..
    //             })
    //             | ActeLegislatif::DepotMotionCensure(DepotMotionCensure {
    //                 ref libelle_acte, ..
    //             })
    //             | ActeLegislatif::DepotMotionReferendaire(DepotMotionReferendaire {
    //                 ref libelle_acte,
    //                 ..
    //             })
    //             | ActeLegislatif::DepotRapport(DepotRapport {
    //                 ref libelle_acte, ..
    //             })
    //             | ActeLegislatif::DiscussionCommission(DiscussionCommission {
    //                 ref libelle_acte, ..
    //             })
    //             | ActeLegislatif::DiscussionSeancePublique(DiscussionSeancePublique {
    //                 ref libelle_acte,
    //                 ..
    //             })
    //             | ActeLegislatif::Etape(Etape {
    //                 ref libelle_acte, ..
    //             })
    //             | ActeLegislatif::EtudeImpact(EtudeImpact {
    //                 ref libelle_acte, ..
    //             })
    //             | ActeLegislatif::MotionProcedure(MotionProcedure {
    //                 ref libelle_acte, ..
    //             })
    //             | ActeLegislatif::NominRapporteurs(NominRapporteurs {
    //                 ref libelle_acte, ..
    //             })
    //             | ActeLegislatif::ProcedureAcceleree(ProcedureAcceleree {
    //                 ref libelle_acte, ..
    //             })
    //             | ActeLegislatif::Promulgation(Promulgation {
    //                 ref libelle_acte, ..
    //             })
    //             | ActeLegislatif::RenvoiCmp(RenvoiCmp {
    //                 ref libelle_acte, ..
    //             })
    //             | ActeLegislatif::RenvoiPrealable(RenvoiPrealable {
    //                 ref libelle_acte, ..
    //             })
    //             | ActeLegislatif::RetraitInitiative(RetraitInitiative {
    //                 ref libelle_acte, ..
    //             })
    //             | ActeLegislatif::SaisieComAvis(SaisieComAvis {
    //                 ref libelle_acte, ..
    //             })
    //             | ActeLegislatif::SaisieComFond(SaisieComFond {
    //                 ref libelle_acte, ..
    //             })
    //             | ActeLegislatif::SaisineConseilConstit(SaisineConseilConstit {
    //                 ref libelle_acte,
    //                 ..
    //             }) => &libelle_acte,
    //         }
    //     }

    //     pub fn organe_ref(&self) -> Option<&str> {
    //         match *self {
    //             ActeLegislatif::AdoptionEurope(_) => None,
    //             ActeLegislatif::ConclusionEtapeCc(ConclusionEtapeCc { ref organe_ref, .. })
    //             | ActeLegislatif::CreationOrganeTemporaire(CreationOrganeTemporaire {
    //                 ref organe_ref,
    //                 ..
    //             })
    //             | ActeLegislatif::Decision(Decision { ref organe_ref, .. })
    //             | ActeLegislatif::DecisionMotionCensure(DecisionMotionCensure {
    //                 ref organe_ref, ..
    //             })
    //             | ActeLegislatif::DecisionRecevabiliteBureau(DecisionRecevabiliteBureau {
    //                 ref organe_ref,
    //                 ..
    //             })
    //             | ActeLegislatif::DeclarationGouvernement(DeclarationGouvernement {
    //                 ref organe_ref,
    //                 ..
    //             })
    //             | ActeLegislatif::DepotAccordInternational(DepotAccordInternational {
    //                 ref organe_ref,
    //                 ..
    //             })
    //             | ActeLegislatif::DepotAvisConseilEtat(DepotAvisConseilEtat {
    //                 ref organe_ref, ..
    //             })
    //             | ActeLegislatif::DepotInitiative(DepotInitiative { ref organe_ref, .. })
    //             | ActeLegislatif::DepotInitiativeNavette(DepotInitiativeNavette {
    //                 ref organe_ref,
    //                 ..
    //             })
    //             | ActeLegislatif::DepotLettreRectificative(DepotLettreRectificative {
    //                 ref organe_ref,
    //                 ..
    //             })
    //             | ActeLegislatif::DepotMotionCensure(DepotMotionCensure { ref organe_ref, .. })
    //             | ActeLegislatif::DepotMotionReferendaire(DepotMotionReferendaire {
    //                 ref organe_ref,
    //                 ..
    //             })
    //             | ActeLegislatif::DepotRapport(DepotRapport { ref organe_ref, .. })
    //             | ActeLegislatif::DiscussionCommission(DiscussionCommission {
    //                 ref organe_ref, ..
    //             })
    //             | ActeLegislatif::DiscussionSeancePublique(DiscussionSeancePublique {
    //                 ref organe_ref,
    //                 ..
    //             })
    //             | ActeLegislatif::EtudeImpact(EtudeImpact { ref organe_ref, .. })
    //             | ActeLegislatif::MotionProcedure(MotionProcedure { ref organe_ref, .. })
    //             | ActeLegislatif::NominRapporteurs(NominRapporteurs { ref organe_ref, .. })
    //             | ActeLegislatif::ProcedureAcceleree(ProcedureAcceleree { ref organe_ref, .. })
    //             | ActeLegislatif::Promulgation(Promulgation { ref organe_ref, .. })
    //             | ActeLegislatif::RenvoiCmp(RenvoiCmp { ref organe_ref, .. })
    //             | ActeLegislatif::RenvoiPrealable(RenvoiPrealable { ref organe_ref, .. })
    //             | ActeLegislatif::RetraitInitiative(RetraitInitiative { ref organe_ref, .. })
    //             | ActeLegislatif::SaisieComAvis(SaisieComAvis { ref organe_ref, .. })
    //             | ActeLegislatif::SaisieComFond(SaisieComFond { ref organe_ref, .. })
    //             | ActeLegislatif::SaisineConseilConstit(SaisineConseilConstit {
    //                 ref organe_ref, ..
    //             }) => Some(&organe_ref),
    //             ActeLegislatif::Etape(Etape { ref organe_ref, .. }) => match &organe_ref {
    //                 None => None,
    //                 Some(ref organe_ref) => Some(&organe_ref),
    //             },
    //         }
    //     }

    pub fn uid(&self) -> &str {
        match *self {
            ActeLegislatif::AdoptionEurope(AdoptionEurope { ref uid, .. })
            | ActeLegislatif::ConclusionEtapeCc(ConclusionEtapeCc { ref uid, .. })
            | ActeLegislatif::CreationOrganeTemporaire(CreationOrganeTemporaire {
                ref uid, ..
            })
            | ActeLegislatif::Decision(Decision { ref uid, .. })
            | ActeLegislatif::DecisionMotionCensure(DecisionMotionCensure { ref uid, .. })
            | ActeLegislatif::DecisionRecevabiliteBureau(DecisionRecevabiliteBureau {
                ref uid,
                ..
            })
            | ActeLegislatif::DeclarationGouvernement(DeclarationGouvernement {
                ref uid, ..
            })
            | ActeLegislatif::DepotAccordInternational(DepotAccordInternational {
                ref uid, ..
            })
            | ActeLegislatif::DepotAvisConseilEtat(DepotAvisConseilEtat { ref uid, .. })
            | ActeLegislatif::DepotInitiative(DepotInitiative { ref uid, .. })
            | ActeLegislatif::DepotInitiativeNavette(DepotInitiativeNavette { ref uid, .. })
            | ActeLegislatif::DepotLettreRectificative(DepotLettreRectificative {
                ref uid, ..
            })
            | ActeLegislatif::DepotMotionCensure(DepotMotionCensure { ref uid, .. })
            | ActeLegislatif::DepotMotionReferendaire(DepotMotionReferendaire {
                ref uid, ..
            })
            | ActeLegislatif::DepotRapport(DepotRapport { ref uid, .. })
            | ActeLegislatif::DiscussionCommission(DiscussionCommission { ref uid, .. })
            | ActeLegislatif::DiscussionSeancePublique(DiscussionSeancePublique {
                ref uid, ..
            })
            | ActeLegislatif::Etape(Etape { ref uid, .. })
            | ActeLegislatif::EtudeImpact(EtudeImpact { ref uid, .. })
            | ActeLegislatif::MotionProcedure(MotionProcedure { ref uid, .. })
            | ActeLegislatif::NominRapporteurs(NominRapporteurs { ref uid, .. })
            | ActeLegislatif::ProcedureAcceleree(ProcedureAcceleree { ref uid, .. })
            | ActeLegislatif::Promulgation(Promulgation { ref uid, .. })
            | ActeLegislatif::RenvoiCmp(RenvoiCmp { ref uid, .. })
            | ActeLegislatif::RenvoiPrealable(RenvoiPrealable { ref uid, .. })
            | ActeLegislatif::RetraitInitiative(RetraitInitiative { ref uid, .. })
            | ActeLegislatif::SaisieComAvis(SaisieComAvis { ref uid, .. })
            | ActeLegislatif::SaisieComFond(SaisieComFond { ref uid, .. })
            | ActeLegislatif::SaisineConseilConstit(SaisineConseilConstit { ref uid, .. }) => &uid,
        }
    }
}

// Using graphql_union because instance_resolvers of graphql_interface does not work with Juniper 0.9.2.
// graphql_interface!(ActeLegislatif: () |&self| {
graphql_union!(ActeLegislatif: Context |&self| {
    // field actes_legislatifs() -> Option<&[ActeLegislatif]> {
    //     self.actes_legislatifs()
    // }

    // field code_acte() -> &str {
    //     self.code_acte()
    // }

    // field date_acte() -> Option<&DateTime<FixedOffset>> {
    //     self.date_acte()
    // }

    // field libelle_acte() -> &LibelleActe {
    //     self.libelle_acte()
    // }

    // field organe(&executor) -> Option<&Organe> {
    //     let context = &executor.context();
    //     context.get_organe_at_uid(&self.organe_ref())
    // }

    // field organe_ref() -> Option<&str> {
    //     self.organe_ref()
    // }

    // field uid() -> &str {
    //     self.uid()
    // }

    instance_resolvers: |_| {
        &AdoptionEurope => match *self {
            ActeLegislatif::AdoptionEurope(ref acte) => Some(acte),
            _ => None,
        },
        &ConclusionEtapeCc => match *self {
            ActeLegislatif::ConclusionEtapeCc(ref acte) => Some(acte),
            _ => None,
        },
        &CreationOrganeTemporaire => match *self {
            ActeLegislatif::CreationOrganeTemporaire(ref acte) => Some(acte),
            _ => None,
        },
        &Decision => match *self {
            ActeLegislatif::Decision(ref acte) => Some(acte),
            _ => None,
        },
        &DecisionMotionCensure => match *self {
            ActeLegislatif::DecisionMotionCensure(ref acte) => Some(acte),
            _ => None,
        },
        &DecisionRecevabiliteBureau => match *self {
            ActeLegislatif::DecisionRecevabiliteBureau(ref acte) => Some(acte),
            _ => None,
        },
        &DeclarationGouvernement => match *self {
            ActeLegislatif::DeclarationGouvernement(ref acte) => Some(acte),
            _ => None,
        },
        &DepotAccordInternational => match *self {
            ActeLegislatif::DepotAccordInternational(ref acte) => Some(acte),
            _ => None,
        },
        &DepotAvisConseilEtat => match *self {
            ActeLegislatif::DepotAvisConseilEtat(ref acte) => Some(acte),
            _ => None,
        },
        &DepotInitiative => match *self {
            ActeLegislatif::DepotInitiative(ref acte) => Some(acte),
            _ => None,
        },
        &DepotInitiativeNavette => match *self {
            ActeLegislatif::DepotInitiativeNavette(ref acte) => Some(acte),
            _ => None,
        },
        &DepotLettreRectificative => match *self {
            ActeLegislatif::DepotLettreRectificative(ref acte) => Some(acte),
            _ => None,
        },
        &DepotMotionCensure => match *self {
            ActeLegislatif::DepotMotionCensure(ref acte) => Some(acte),
            _ => None,
        },
        &DepotMotionReferendaire => match *self {
            ActeLegislatif::DepotMotionReferendaire(ref acte) => Some(acte),
            _ => None,
        },
        &DepotRapport => match *self {
            ActeLegislatif::DepotRapport(ref acte) => Some(acte),
            _ => None,
        },
        &DiscussionCommission => match *self {
            ActeLegislatif::DiscussionCommission(ref acte) => Some(acte),
            _ => None,
        },
        &DiscussionSeancePublique => match *self {
            ActeLegislatif::DiscussionSeancePublique(ref acte) => Some(acte),
            _ => None,
        },
        &Etape => match *self {
            ActeLegislatif::Etape(ref acte) => Some(acte),
            _ => None,
        },
        &EtudeImpact => match *self {
            ActeLegislatif::EtudeImpact(ref acte) => Some(acte),
            _ => None,
        },
        &MotionProcedure => match *self {
            ActeLegislatif::MotionProcedure(ref acte) => Some(acte),
            _ => None,
        },
        &NominRapporteurs => match *self {
            ActeLegislatif::NominRapporteurs(ref acte) => Some(acte),
            _ => None,
        },
        &ProcedureAcceleree => match *self {
            ActeLegislatif::ProcedureAcceleree(ref acte) => Some(acte),
            _ => None,
        },
        &Promulgation => match *self {
            ActeLegislatif::Promulgation(ref acte) => Some(acte),
            _ => None,
        },
        &RenvoiCmp => match *self {
            ActeLegislatif::RenvoiCmp(ref acte) => Some(acte),
            _ => None,
        },
        &RenvoiPrealable => match *self {
            ActeLegislatif::RenvoiPrealable(ref acte) => Some(acte),
            _ => None,
        },
        &RetraitInitiative => match *self {
            ActeLegislatif::RetraitInitiative(ref acte) => Some(acte),
            _ => None,
        },
        &SaisieComAvis => match *self {
            ActeLegislatif::SaisieComAvis(ref acte) => Some(acte),
            _ => None,
        },
        &SaisieComFond => match *self {
            ActeLegislatif::SaisieComFond(ref acte) => Some(acte),
            _ => None,
        },
        &SaisineConseilConstit => match *self {
            ActeLegislatif::SaisineConseilConstit(ref acte) => Some(acte),
            _ => None,
        },
    }
});

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct ActesLegislatifs {
    #[serde(deserialize_with = "map_to_vec", rename = "acteLegislatif")]
    pub actes: Vec<ActeLegislatif>,
    // Field actes_indexes is not present in Assemblée's JSON. It is added later.
    #[serde(default)]
    pub actes_indexes: Vec<i32>,
}

graphql_object!(ActesLegislatifs: Context |&self| {
    field actes() -> Vec<&ActeLegislatif> {
        // Generate a flat array of "actes" instead of a tree, because a tree is not adapted to GraphQL.
        let mut actes: Vec<&ActeLegislatif> = Vec::new();
        for acte in &self.actes {
            actes.push(acte);
            if let ActeLegislatif::Etape(etape) = acte {
                actes.extend(etape.flatten_actes_legislatifs());
            }
        }
        actes
    }

    field actes_indexes() -> &Vec<i32> {
        &self.actes_indexes
    }
});

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct ActeurInitiateur {
    #[serde(rename = "acteurRef")]
    pub acteur_ref: String,
    #[serde(rename = "mandatRef")]
    pub mandat_ref: String,
}

graphql_object!(ActeurInitiateur: Context |&self| {
    field acteur(&executor) -> Option<&Acteur> {
        let context = &executor.context();
        context.get_acteur_at_uid(&self.acteur_ref)
    }

    field acteur_ref() -> &str {
        &self.acteur_ref
    }

    field mandat(&executor) -> Option<&Mandat> {
        let context = &executor.context();
        match context.get_acteur_at_uid(&self.acteur_ref) {
            None => None,
            Some(ref acteur) => {
                for mandat in &acteur.mandats.mandats {
                    if mandat.uid() == self.mandat_ref {
                        return Some(mandat)
                    }
                }
                None
            },
        }
    }

    field mandat_ref() -> &str {
        &self.mandat_ref
    }
});

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct ActeursInitiateur {
    #[serde(deserialize_with = "map_to_vec", rename = "acteur")]
    pub acteurs: Vec<ActeurInitiateur>,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct AdoptionEurope {
    #[serde(rename = "actesLegislatifs")]
    pub actes_legislatifs: Option<EmptyChoice>,
    #[serde(rename = "codeActe")]
    pub code_acte: String,
    #[serde(rename = "dateActe")]
    pub date_acte: Option<DateTime<FixedOffset>>,
    #[serde(rename = "infoJOCE")]
    pub info_joce: InfoJoce,
    #[serde(rename = "libelleActe")]
    pub libelle_acte: LibelleActe,
    #[serde(rename = "statutAdoption")]
    pub statut_adoption: Codier,
    #[serde(rename = "texteEuropeen")]
    pub texte_europeen: TexteEuropeen,
    pub uid: String,
}

graphql_object!(AdoptionEurope: Context |&self| {
    field code_acte() -> &str {
        &self.code_acte
    }

    field date_acte() -> Option<DateTime<FixedOffset>> {
        self.date_acte
    }

    field info_joce() -> &InfoJoce {
        &self.info_joce
    }

    field libelle_acte() -> &LibelleActe {
        &self.libelle_acte
    }

    field statut_adoption() -> &Codier {
        &self.statut_adoption
    }

    field texte_europeen() -> &TexteEuropeen {
        &self.texte_europeen
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct ConclusionEtapeCc {
    #[serde(rename = "actesLegislatifs")]
    pub actes_legislatifs: Option<EmptyChoice>,
    #[serde(rename = "anneeDecision")]
    pub annee_decision: String,
    #[serde(rename = "codeActe")]
    pub code_acte: String,
    #[serde(rename = "dateActe")]
    pub date_acte: Option<DateTime<FixedOffset>>,
    #[serde(rename = "libelleActe")]
    pub libelle_acte: LibelleActe,
    #[serde(rename = "numDecision")]
    pub num_decision: String,
    #[serde(rename = "organeRef")]
    pub organe_ref: String,
    #[serde(rename = "statutConclusion")]
    pub statut_conclusion: Codier,
    pub uid: String,
    #[serde(rename = "urlConclusion")]
    pub url_conclusion: String,
}

graphql_object!(ConclusionEtapeCc: Context |&self| {
    field annee_decision() -> &str {
        &self.annee_decision
    }

    field code_acte() -> &str {
        &self.code_acte
    }

    field date_acte() -> Option<DateTime<FixedOffset>> {
        self.date_acte
    }

    field libelle_acte() -> &LibelleActe {
        &self.libelle_acte
    }

    field num_decision() -> &str {
        &self.num_decision
    }

    field organe(&executor) -> Option<&Organe> {
        let context = &executor.context();
        context.get_organe_at_uid(&self.organe_ref)
    }

    field organe_ref() -> Option<&str> {
        Some(self.organe_ref.as_str())
    }

    field statut_conclusion() -> &Codier {
        &self.statut_conclusion
    }

    field uid() -> &str {
        &self.uid
    }

    field url_conclusion() -> &str {
        &self.url_conclusion
    }
});

#[derive(Clone, Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct ContributionInternaute {
    #[serde(rename = "dateFermeture")]
    // TODO: Date<FixedOffset> type is not able to deserialize dates with timezone (`YYYY-MM-DD±HH:MM`).
    pub date_fermeture: Option<String>,
    #[serde(rename = "dateOuverture")]
    // TODO: Date<FixedOffset> type is not able to deserialize dates with timezone (`YYYY-MM-DD±HH:MM`).
    pub date_ouverture: Option<String>,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct CreationOrganeTemporaire {
    #[serde(rename = "actesLegislatifs")]
    pub actes_legislatifs: Option<EmptyChoice>,
    #[serde(rename = "codeActe")]
    pub code_acte: String,
    #[serde(rename = "dateActe")]
    pub date_acte: Option<DateTime<FixedOffset>>,
    pub initiateurs: Option<InitiateursCreationOrganeTemporaire>,
    #[serde(rename = "libelleActe")]
    pub libelle_acte: LibelleActe,
    #[serde(rename = "organeRef")]
    pub organe_ref: String,
    pub uid: String,
}

graphql_object!(CreationOrganeTemporaire: Context |&self| {
    field code_acte() -> &str {
        &self.code_acte
    }

    field date_acte() -> Option<DateTime<FixedOffset>> {
        self.date_acte
    }

    field initiateurs(&executor) -> Option<&Organe> {
        match &self.initiateurs {
            None => None,
            Some(ref initiateurs) => {
                let context = &executor.context();
                context.get_organe_at_uid(&initiateurs.organe_ref)
            },
        }
    }

    field initiateurs_ref() -> Option<&str> {
        match &self.initiateurs {
            None => None,
            Some(ref initiateurs) => Some(initiateurs.organe_ref.as_str()),
        }
    }

    field libelle_acte() -> &LibelleActe {
        &self.libelle_acte
    }

    field organe(&executor) -> Option<&Organe> {
        let context = &executor.context();
        context.get_organe_at_uid(&self.organe_ref)
    }

    field organe_ref() -> Option<&str> {
        Some(self.organe_ref.as_str())
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Decision {
    #[serde(rename = "actesLegislatifs")]
    pub actes_legislatifs: Option<EmptyChoice>,
    #[serde(rename = "codeActe")]
    pub code_acte: String,
    #[serde(rename = "dateActe")]
    pub date_acte: Option<DateTime<FixedOffset>>,
    #[serde(rename = "libelleActe")]
    pub libelle_acte: LibelleActe,
    #[serde(rename = "organeRef")]
    pub organe_ref: String,
    #[serde(rename = "reunionRef")]
    pub reunion_ref: Option<String>,
    #[serde(rename = "statutConclusion")]
    pub statut_conclusion: Codier,
    #[serde(rename = "textesAssocies")]
    pub textes_associes: Option<TextesAssocies>,
    pub uid: String,
    #[serde(rename = "voteRefs")]
    pub vote_refs: Option<VoteRefs>,
}

graphql_object!(Decision: Context |&self| {
    field code_acte() -> &str {
        &self.code_acte
    }

    field date_acte() -> Option<DateTime<FixedOffset>> {
        self.date_acte
    }

    field libelle_acte() -> &LibelleActe {
        &self.libelle_acte
    }

    field organe(&executor) -> Option<&Organe> {
        let context = &executor.context();
        context.get_organe_at_uid(&self.organe_ref)
    }

    field organe_ref() -> Option<&str> {
        Some(self.organe_ref.as_str())
    }

    field reunion(&executor) -> Option<&Reunion> {
        match &self.reunion_ref {
            None => None,
            Some(ref reunion_ref) => {
                let context = &executor.context();
                context.get_reunion_at_uid(&reunion_ref)
            },
        }
    }

    field reunion_ref() -> Option<&str> {
        match &self.reunion_ref {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field statut_conclusion() -> &Codier {
        &self.statut_conclusion
    }

    field textes_associes() -> Vec<&TexteAssocie> {
        match &self.textes_associes {
            None => vec![],
            Some(ref textes_associes) => {
                textes_associes.textes_associes
                    .iter()
                    .collect()
            },
        }
    }

    field uid() -> &str {
        &self.uid
    }

    field vote_refs() -> Vec<&str> {
        match &self.vote_refs {
            None => vec![],
            Some(ref vote_refs) => {
                vote_refs.vote_refs
                    .iter()
                    .map(|ref vote_ref| vote_ref.as_str())
                    .collect()
            },
        }
    }

    field votes(&executor) -> Vec<&Scrutin> {
        match &self.vote_refs {
            None => vec![],
            Some(ref vote_refs) => {
                let context = &executor.context();
                vote_refs.vote_refs
                    .iter()
                    .filter_map(|ref vote_ref| context.get_scrutin_at_uid(&vote_ref))
                    .collect()
            },
        }
    }
});

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct DecisionMotionCensure {
    #[serde(rename = "actesLegislatifs")]
    pub actes_legislatifs: Option<EmptyChoice>,
    #[serde(rename = "codeActe")]
    pub code_acte: String,
    #[serde(rename = "dateActe")]
    pub date_acte: Option<DateTime<FixedOffset>>,
    pub decision: Codier,
    #[serde(rename = "libelleActe")]
    pub libelle_acte: LibelleActe,
    #[serde(rename = "odSeancejRef")]
    pub odj_seance_ref: Option<EmptyChoice>,
    #[serde(rename = "organeRef")]
    pub organe_ref: String,
    pub uid: String,
    #[serde(rename = "voteRefs")]
    pub vote_refs: Option<VoteRefs>,
}

graphql_object!(DecisionMotionCensure: Context |&self| {
    field code_acte() -> &str {
        &self.code_acte
    }

    field date_acte() -> Option<DateTime<FixedOffset>> {
        self.date_acte
    }

    field decision() -> &Codier {
        &self.decision
    }

    field libelle_acte() -> &LibelleActe {
        &self.libelle_acte
    }

    field organe(&executor) -> Option<&Organe> {
        let context = &executor.context();
        context.get_organe_at_uid(&self.organe_ref)
    }

    field organe_ref() -> Option<&str> {
        Some(self.organe_ref.as_str())
    }

    field uid() -> &str {
        &self.uid
    }

    field vote_refs() -> Vec<&str> {
        match &self.vote_refs {
            None => vec![],
            Some(ref vote_refs) => {
                vote_refs.vote_refs
                    .iter()
                    .map(|ref vote_ref| vote_ref.as_str())
                    .collect()
            },
        }
    }

    field votes(&executor) -> Vec<&Scrutin> {
        match &self.vote_refs {
            None => vec![],
            Some(ref vote_refs) => {
                let context = &executor.context();
                vote_refs.vote_refs
                    .iter()
                    .filter_map(|ref vote_ref| context.get_scrutin_at_uid(&vote_ref))
                    .collect()
            },
        }
    }
});

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct DecisionRecevabiliteBureau {
    #[serde(rename = "actesLegislatifs")]
    pub actes_legislatifs: Option<EmptyChoice>,
    #[serde(rename = "codeActe")]
    pub code_acte: String,
    #[serde(rename = "dateActe")]
    pub date_acte: Option<DateTime<FixedOffset>>,
    pub decision: Codier,
    #[serde(rename = "formuleDecision")]
    pub formule_decision: String,
    #[serde(rename = "libelleActe")]
    pub libelle_acte: LibelleActe,
    #[serde(rename = "organeRef")]
    pub organe_ref: String,
    pub uid: String,
}

graphql_object!(DecisionRecevabiliteBureau: Context |&self| {
    field code_acte() -> &str {
        &self.code_acte
    }

    field date_acte() -> Option<DateTime<FixedOffset>> {
        self.date_acte
    }

    field decision() -> &Codier {
        &self.decision
    }

    field formule_decision() -> &str {
        &self.formule_decision
    }

    field libelle_acte() -> &LibelleActe {
        &self.libelle_acte
    }

    field organe(&executor) -> Option<&Organe> {
        let context = &executor.context();
        context.get_organe_at_uid(&self.organe_ref)
    }

    field organe_ref() -> Option<&str> {
        Some(self.organe_ref.as_str())
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct DeclarationGouvernement {
    #[serde(rename = "actesLegislatifs")]
    pub actes_legislatifs: Option<EmptyChoice>,
    #[serde(rename = "codeActe")]
    pub code_acte: String,
    #[serde(rename = "dateActe")]
    pub date_acte: Option<DateTime<FixedOffset>>,
    #[serde(rename = "libelleActe")]
    pub libelle_acte: LibelleActe,
    #[serde(rename = "organeRef")]
    pub organe_ref: String,
    #[serde(rename = "texteAssocie")]
    pub texte_associe: String,
    #[serde(rename = "typeDeclaration")]
    pub type_declaration: Codier,
    pub uid: String,
}

graphql_object!(DeclarationGouvernement: Context |&self| {
    field code_acte() -> &str {
        &self.code_acte
    }

    field date_acte() -> Option<DateTime<FixedOffset>> {
        self.date_acte
    }

    field libelle_acte() -> &LibelleActe {
        &self.libelle_acte
    }

    field organe(&executor) -> Option<&Organe> {
        let context = &executor.context();
        context.get_organe_at_uid(&self.organe_ref)
    }

    field organe_ref() -> Option<&str> {
        Some(self.organe_ref.as_str())
    }

    field texte_associe(&executor) -> Vec<&Document> {
        let context = &executor.context();
        match context.get_document_at_uid(&self.texte_associe) {
            None => vec![],
            Some(ref document) => {
                // Generate a flat vector from document and its sub-documents (aka divisions),
                // because a tree is not adapted to GraphQL.
                document.flatten_document()
            },
        }
    }

    field texte_associe_ref() -> &str {
        &self.texte_associe
    }

    field type_declaration() -> &Codier {
        &self.type_declaration
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct DepotAccordInternational {
    #[serde(rename = "actesLegislatifs")]
    pub actes_legislatifs: Option<EmptyChoice>,
    #[serde(rename = "codeActe")]
    pub code_acte: String,
    #[serde(rename = "dateActe")]
    pub date_acte: Option<DateTime<FixedOffset>>,
    #[serde(rename = "libelleActe")]
    pub libelle_acte: LibelleActe,
    #[serde(rename = "organeRef")]
    pub organe_ref: String,
    #[serde(rename = "texteAssocie")]
    pub texte_associe: String,
    pub uid: String,
}

graphql_object!(DepotAccordInternational: Context |&self| {
    field code_acte() -> &str {
        &self.code_acte
    }

    field date_acte() -> Option<DateTime<FixedOffset>> {
        self.date_acte
    }

    field libelle_acte() -> &LibelleActe {
        &self.libelle_acte
    }

    field organe(&executor) -> Option<&Organe> {
        let context = &executor.context();
        context.get_organe_at_uid(&self.organe_ref)
    }

    field organe(&executor) -> Option<&Organe> {
        let context = &executor.context();
        context.get_organe_at_uid(&self.organe_ref)
    }

    field organe_ref() -> Option<&str> {
        Some(self.organe_ref.as_str())
    }

    field texte_associe(&executor) -> Vec<&Document> {
        let context = &executor.context();
        match context.get_document_at_uid(&self.texte_associe) {
            None => vec![],
            Some(ref document) => {
                // Generate a flat vector from document and its sub-documents (aka divisions),
                // because a tree is not adapted to GraphQL.
                document.flatten_document()
            },
        }
    }

    field texte_associe_ref() -> &str {
        &self.texte_associe
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct DepotAvisConseilEtat {
    #[serde(rename = "actesLegislatifs")]
    pub actes_legislatifs: Option<EmptyChoice>,
    #[serde(rename = "codeActe")]
    pub code_acte: String,
    #[serde(rename = "dateActe")]
    pub date_acte: Option<DateTime<FixedOffset>>,
    #[serde(rename = "libelleActe")]
    pub libelle_acte: LibelleActe,
    #[serde(rename = "organeRef")]
    pub organe_ref: String,
    #[serde(rename = "texteAssocie")]
    pub texte_associe: String,
    pub uid: String,
}

graphql_object!(DepotAvisConseilEtat: Context |&self| {
    field code_acte() -> &str {
        &self.code_acte
    }

    field date_acte() -> Option<DateTime<FixedOffset>> {
        self.date_acte
    }

    field libelle_acte() -> &LibelleActe {
        &self.libelle_acte
    }

    field organe(&executor) -> Option<&Organe> {
        let context = &executor.context();
        context.get_organe_at_uid(&self.organe_ref)
    }

    field organe_ref() -> Option<&str> {
        Some(self.organe_ref.as_str())
    }

    field texte_associe(&executor) -> Vec<&Document> {
        let context = &executor.context();
        match context.get_document_at_uid(&self.texte_associe) {
            None => vec![],
            Some(ref document) => {
                // Generate a flat vector from document and its sub-documents (aka divisions),
                // because a tree is not adapted to GraphQL.
                document.flatten_document()
            },
        }
    }

    field texte_associe_ref() -> &str {
        &self.texte_associe
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct DepotInitiative {
    #[serde(rename = "actesLegislatifs")]
    pub actes_legislatifs: Option<EmptyChoice>,
    #[serde(rename = "codeActe")]
    pub code_acte: String,
    #[serde(rename = "dateActe")]
    pub date_acte: Option<DateTime<FixedOffset>>,
    #[serde(rename = "libelleActe")]
    pub libelle_acte: LibelleActe,
    #[serde(rename = "organeRef")]
    pub organe_ref: String,
    #[serde(rename = "texteAssocie")]
    pub texte_associe: String,
    pub uid: String,
}

graphql_object!(DepotInitiative: Context |&self| {
    field code_acte() -> &str {
        &self.code_acte
    }

    field date_acte() -> Option<DateTime<FixedOffset>> {
        self.date_acte
    }

    field libelle_acte() -> &LibelleActe {
        &self.libelle_acte
    }

    field organe(&executor) -> Option<&Organe> {
        let context = &executor.context();
        context.get_organe_at_uid(&self.organe_ref)
    }

    field organe_ref() -> Option<&str> {
        Some(self.organe_ref.as_str())
    }

    field texte_associe(&executor) -> Vec<&Document> {
        let context = &executor.context();
        match context.get_document_at_uid(&self.texte_associe) {
            None => vec![],
            Some(ref document) => {
                // Generate a flat vector from document and its sub-documents (aka divisions),
                // because a tree is not adapted to GraphQL.
                document.flatten_document()
            },
        }
    }

    field texte_associe_ref() -> &str {
        &self.texte_associe
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct DepotInitiativeNavette {
    #[serde(rename = "actesLegislatifs")]
    pub actes_legislatifs: Option<EmptyChoice>,
    #[serde(rename = "codeActe")]
    pub code_acte: String,
    #[serde(rename = "dateActe")]
    pub date_acte: Option<DateTime<FixedOffset>>,
    #[serde(rename = "libelleActe")]
    pub libelle_acte: LibelleActe,
    #[serde(rename = "organeRef")]
    pub organe_ref: String,
    pub provenance: String,
    #[serde(rename = "texteAssocie")]
    pub texte_associe: String,
    pub uid: String,
}

graphql_object!(DepotInitiativeNavette: Context |&self| {
    field code_acte() -> &str {
        &self.code_acte
    }

    field date_acte() -> Option<DateTime<FixedOffset>> {
        self.date_acte
    }

    field libelle_acte() -> &LibelleActe {
        &self.libelle_acte
    }

    field organe(&executor) -> Option<&Organe> {
        let context = &executor.context();
        context.get_organe_at_uid(&self.organe_ref)
    }

    field organe_ref() -> Option<&str> {
        Some(self.organe_ref.as_str())
    }

    field provenance(&executor) -> Option<&Organe> {
        let context = &executor.context();
        context.get_organe_at_uid(&self.provenance)
    }

    field provenance_ref() -> &str {
        &self.provenance
    }

    field texte_associe(&executor) -> Vec<&Document> {
        let context = &executor.context();
        match context.get_document_at_uid(&self.texte_associe) {
            None => vec![],
            Some(ref document) => {
                // Generate a flat vector from document and its sub-documents (aka divisions),
                // because a tree is not adapted to GraphQL.
                document.flatten_document()
            },
        }
    }

    field texte_associe_ref() -> &str {
        &self.texte_associe
    }

    field uid() -> &str {
        &self.uid
    }
});

/// Extrait Fiche de synthèse n°33
/// http://www.assemblee-nationale.fr/connaissance/fiches_synthese/septembre2012/fiche_33.asp
/// Le projet est accompagné d’un décret de présentation au Parlement qui indique les
/// organes qui ont délibéré, détermine l’assemblée devant laquelle le texte est déposé
/// en premier lieu et désigne les membres du Gouvernement qui soutiendront le texte
/// devant les assemblées. Ce décret est signé par le Premier ministre et contresigné
/// par les ministres ainsi désignés. À ce stade, le Gouvernement ne peut plus modifier
/// le projet de loi que par une lettre rectificative. Cette procédure usuelle, que
/// ne prévoit aucun texte, prend la forme d’une lettre du Premier ministre rectifiant
/// directement le contenu d’un projet de loi préalablement déposé. Comme ce dernier, la
/// lettre rectificative est soumise au Conseil d’État. Elle conduit à remanier le texte
/// devant servir de base à la discussion parlementaire.
#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct DepotLettreRectificative {
    #[serde(rename = "actesLegislatifs")]
    pub actes_legislatifs: Option<EmptyChoice>,
    #[serde(rename = "codeActe")]
    pub code_acte: String,
    #[serde(rename = "dateActe")]
    pub date_acte: Option<DateTime<FixedOffset>>,
    #[serde(rename = "libelleActe")]
    pub libelle_acte: LibelleActe,
    #[serde(rename = "organeRef")]
    pub organe_ref: String,
    #[serde(rename = "texteAssocie")]
    pub texte_associe: String,
    pub uid: String,
}

graphql_object!(DepotLettreRectificative: Context |&self| {
    field code_acte() -> &str {
        &self.code_acte
    }

    field date_acte() -> Option<DateTime<FixedOffset>> {
        self.date_acte
    }

    field libelle_acte() -> &LibelleActe {
        &self.libelle_acte
    }

    field organe(&executor) -> Option<&Organe> {
        let context = &executor.context();
        context.get_organe_at_uid(&self.organe_ref)
    }

    field organe_ref() -> Option<&str> {
        Some(self.organe_ref.as_str())
    }

    field texte_associe(&executor) -> Vec<&Document> {
        let context = &executor.context();
        match context.get_document_at_uid(&self.texte_associe) {
            None => vec![],
            Some(ref document) => {
                // Generate a flat vector from document and its sub-documents (aka divisions),
                // because a tree is not adapted to GraphQL.
                document.flatten_document()
            },
        }
    }

    field texte_associe_ref() -> &str {
        &self.texte_associe
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct DepotMotionCensure {
    #[serde(rename = "actesLegislatifs")]
    pub actes_legislatifs: Option<EmptyChoice>,
    pub auteurs: ListeActeursRef,
    #[serde(rename = "codeActe")]
    pub code_acte: String,
    #[serde(rename = "dateActe")]
    pub date_acte: Option<DateTime<FixedOffset>>,
    #[serde(rename = "dateRetrait")]
    pub date_retrait: Option<EmptyChoice>,
    #[serde(rename = "libelleActe")]
    pub libelle_acte: LibelleActe,
    #[serde(rename = "organeRef")]
    pub organe_ref: String,
    #[serde(rename = "typeMotionCensure")]
    pub type_motion_censure: Codier,
    pub uid: String,
}

graphql_object!(DepotMotionCensure: Context |&self| {
    field auteurs(&executor) -> Vec<&Acteur> {
        let context = &executor.context();
        self.auteurs.acteurs_refs
            .iter()
            .filter_map(|ref acteur_ref| context.get_acteur_at_uid(&acteur_ref))
            .collect()
    }

    field auteurs_refs() -> &Vec<String> {
        &self.auteurs.acteurs_refs
    }

    field code_acte() -> &str {
        &self.code_acte
    }

    field date_acte() -> Option<DateTime<FixedOffset>> {
        self.date_acte
    }

    field libelle_acte() -> &LibelleActe {
        &self.libelle_acte
    }

    field organe(&executor) -> Option<&Organe> {
        let context = &executor.context();
        context.get_organe_at_uid(&self.organe_ref)
    }

    field organe_ref() -> Option<&str> {
        Some(self.organe_ref.as_str())
    }

    field type_motion_censure() -> &Codier {
        &self.type_motion_censure
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct DepotMotionReferendaire {
    #[serde(rename = "actesLegislatifs")]
    pub actes_legislatifs: Option<EmptyChoice>,
    pub auteurs: Option<ListeActeursRef>,
    #[serde(rename = "codeActe")]
    pub code_acte: String,
    #[serde(rename = "dateActe")]
    pub date_acte: Option<DateTime<FixedOffset>>,
    #[serde(rename = "libelleActe")]
    pub libelle_acte: LibelleActe,
    #[serde(rename = "organeRef")]
    pub organe_ref: String,
    pub uid: String,
}

graphql_object!(DepotMotionReferendaire: Context |&self| {
    field auteurs(&executor) -> Vec<&Acteur> {
        match &self.auteurs {
            None => vec![],
            Some(ref auteurs) => {
                let context = &executor.context();
                auteurs.acteurs_refs
                    .iter()
                    .filter_map(|ref acteur_ref| context.get_acteur_at_uid(&acteur_ref))
                    .collect()
            },
        }
    }

    field auteurs_refs() -> Vec<&str> {
        match &self.auteurs {
            None => vec![],
            Some(ref auteurs) =>
                auteurs.acteurs_refs
                    .iter()
                    .map(|ref acteur_ref| acteur_ref.as_str())
                    .collect(),
        }
    }

    field code_acte() -> &str {
        &self.code_acte
    }

    field date_acte() -> Option<DateTime<FixedOffset>> {
        self.date_acte
    }

    field libelle_acte() -> &LibelleActe {
        &self.libelle_acte
    }

    field organe(&executor) -> Option<&Organe> {
        let context = &executor.context();
        context.get_organe_at_uid(&self.organe_ref)
    }

    field organe_ref() -> Option<&str> {
        Some(self.organe_ref.as_str())
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct DepotRapport {
    #[serde(rename = "actesLegislatifs")]
    pub actes_legislatifs: Option<EmptyChoice>,
    #[serde(rename = "codeActe")]
    pub code_acte: String,
    #[serde(rename = "dateActe")]
    pub date_acte: Option<DateTime<FixedOffset>>,
    #[serde(rename = "libelleActe")]
    pub libelle_acte: LibelleActe,
    #[serde(rename = "organeRef")]
    pub organe_ref: String,
    #[serde(rename = "texteAdopte")]
    pub texte_adopte: Option<String>,
    #[serde(rename = "texteAssocie")]
    pub texte_associe: String,
    pub uid: String,
}

graphql_object!(DepotRapport: Context |&self| {
    field code_acte() -> &str {
        &self.code_acte
    }

    field date_acte() -> Option<DateTime<FixedOffset>> {
        self.date_acte
    }

    field libelle_acte() -> &LibelleActe {
        &self.libelle_acte
    }

    field organe(&executor) -> Option<&Organe> {
        let context = &executor.context();
        context.get_organe_at_uid(&self.organe_ref)
    }

    field organe_ref() -> Option<&str> {
        Some(self.organe_ref.as_str())
    }

    field texte_adopte(&executor) -> Vec<&Document> {
        match &self.texte_adopte {
            None => vec![],
            Some(ref texte_adopte_ref) => {
                let context = &executor.context();
                match context.get_document_at_uid(&texte_adopte_ref) {
                    None => vec![],
                    Some(ref document) => {
                        // Generate a flat vector from document and its sub-documents (aka divisions),
                        // because a tree is not adapted to GraphQL.
                        document.flatten_document()
                    },
                }
            },
        }
    }

    field texte_adopte_ref() -> Option<&str> {
        match &self.texte_adopte {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field texte_associe(&executor) -> Vec<&Document> {
        let context = &executor.context();
        match context.get_document_at_uid(&self.texte_associe) {
            None => vec![],
            Some(ref document) => {
                // Generate a flat vector from document and its sub-documents (aka divisions),
                // because a tree is not adapted to GraphQL.
                document.flatten_document()
            },
        }
    }

    field texte_associe_ref() -> &str {
        &self.texte_associe
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct DiscussionCommission {
    #[serde(rename = "actesLegislatifs")]
    pub actes_legislatifs: Option<EmptyChoice>,
    #[serde(rename = "codeActe")]
    pub code_acte: String,
    #[serde(rename = "dateActe")]
    pub date_acte: Option<DateTime<FixedOffset>>,
    #[serde(rename = "libelleActe")]
    pub libelle_acte: LibelleActe,
    #[serde(rename = "odjRef")]
    /// Référence de l'ordre du jour (à l'intérieur de la réunion référérencée par `reunion_ref`)
    pub odj_ref: Option<String>,
    #[serde(rename = "organeRef")]
    pub organe_ref: String,
    #[serde(rename = "reunionRef")]
    pub reunion_ref: Option<String>,
    pub uid: String,
}

graphql_object!(DiscussionCommission: Context |&self| {
    field code_acte() -> &str {
        &self.code_acte
    }

    field date_acte() -> Option<DateTime<FixedOffset>> {
        self.date_acte
    }

    field libelle_acte() -> &LibelleActe {
        &self.libelle_acte
    }

    field odj(&executor) -> Option<&PointOdj> {
        match (&self.odj_ref, &self.reunion_ref) {
            (Some(ref odj_ref), Some(ref reunion_ref)) => {
                let context = &executor.context();
                match context.get_reunion_at_uid(&reunion_ref) {
                    None => None,
                    Some(reunion) => {
                        match reunion.odj {
                            None => None,
                            Some(ref odj) => {
                                match odj.points_odj {
                                    None => None,
                                    Some(ref points_odj) => points_odj.points_odj
                                        .iter()
                                        .filter(|ref point_odj| point_odj.uid() == odj_ref)
                                        .nth(0)
                                }
                            },
                        }
                    },
                }
            },
            _ => None,
        }
    }

    field odj_ref() -> Option<&str> as "Référence de l'ordre du jour (à l'intérieur de la réunion référérencée par `reunion_ref`)" {
        match &self.odj_ref {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field organe(&executor) -> Option<&Organe> {
        let context = &executor.context();
        context.get_organe_at_uid(&self.organe_ref)
    }

    field organe_ref() -> Option<&str> {
        Some(self.organe_ref.as_str())
    }

    field reunion(&executor) -> Option<&Reunion> {
        match &self.reunion_ref {
            None => None,
            Some(ref reunion_ref) => {
                let context = &executor.context();
                context.get_reunion_at_uid(&reunion_ref)
            },
        }
    }

    field reunion_ref() -> Option<&str> {
        match &self.reunion_ref {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct DiscussionSeancePublique {
    #[serde(rename = "actesLegislatifs")]
    pub actes_legislatifs: Option<EmptyChoice>,
    #[serde(rename = "codeActe")]
    pub code_acte: String,
    #[serde(rename = "dateActe")]
    pub date_acte: Option<DateTime<FixedOffset>>,
    #[serde(rename = "libelleActe")]
    pub libelle_acte: LibelleActe,
    #[serde(rename = "odjRef")]
    /// Référence de l'ordre du jour (à l'intérieur de la réunion référérencée par `reunion_ref`)
    pub odj_ref: String,
    #[serde(rename = "organeRef")]
    pub organe_ref: String,
    #[serde(rename = "reunionRef")]
    pub reunion_ref: String,
    pub uid: String,
}

graphql_object!(DiscussionSeancePublique: Context |&self| {
    field code_acte() -> &str {
        &self.code_acte
    }

    field date_acte() -> Option<DateTime<FixedOffset>> {
        self.date_acte
    }

    field libelle_acte() -> &LibelleActe {
        &self.libelle_acte
    }

    field odj(&executor) -> Option<&PointOdj> {
        let context = &executor.context();
        match context.get_reunion_at_uid(&self.reunion_ref) {
            None => None,
            Some(reunion) => {
                match reunion.odj {
                    None => None,
                    Some(ref odj) => {
                        match odj.points_odj {
                            None => None,
                            Some(ref points_odj) => points_odj.points_odj
                                .iter()
                                .filter(|ref point_odj| point_odj.uid() == self.odj_ref)
                                .nth(0)
                        }
                    },
                }
            },
        }
    }

    field odj_ref() -> Option<&str> as "Référence de l'ordre du jour (à l'intérieur de la réunion référérencée par `reunion_ref`)" {
        Some(self.odj_ref.as_str())
    }

    field organe(&executor) -> Option<&Organe> {
        let context = &executor.context();
        context.get_organe_at_uid(&self.organe_ref)
    }

    field organe_ref() -> Option<&str> {
        Some(self.organe_ref.as_str())
    }

    field reunion(&executor) -> Option<&Reunion> {
        let context = &executor.context();
        context.get_reunion_at_uid(&self.reunion_ref)
    }

    field reunion_ref() -> Option<&str> {
        Some(self.reunion_ref.as_str())
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Etape {
    // Field actes_legislatifs is never None except when `Etape` has not been completed yet.
    #[serde(rename = "actesLegislatifs")]
    pub actes_legislatifs: Option<ActesLegislatifs>,
    #[serde(rename = "codeActe")]
    pub code_acte: String,
    #[serde(rename = "dateActe")]
    pub date_acte: Option<EmptyChoice>,
    #[serde(rename = "libelleActe")]
    pub libelle_acte: LibelleActe,
    #[serde(rename = "organeRef")]
    pub organe_ref: Option<String>,
    pub uid: String,
}

impl Etape {
    pub fn flatten_actes_legislatifs(&self) -> Vec<&ActeLegislatif> {
        let mut actes: Vec<&ActeLegislatif> = Vec::new();
        if let Some(ref actes_legislatifs) = self.actes_legislatifs {
            for acte in &actes_legislatifs.actes {
                actes.push(acte);
                if let ActeLegislatif::Etape(etape) = acte {
                    actes.extend(etape.flatten_actes_legislatifs());
                }
            }
        }
        actes
    }

    pub fn index_actes_legislatifs(&mut self, index: &mut i32) {
        let mut actes_indexes: Vec<i32> = Vec::new();
        if let Some(ref mut actes_legislatifs) = self.actes_legislatifs {
            for acte in &mut actes_legislatifs.actes {
                actes_indexes.push(*index);
                *index += 1;
                if let ActeLegislatif::Etape(ref mut etape) = acte {
                    etape.index_actes_legislatifs(index);
                }
            }
            actes_legislatifs.actes_indexes = actes_indexes;
        }
    }
}

graphql_object!(Etape: Context |&self| {
    field actes_legislatifs_indexes() -> Option<&Vec<i32>> {
        match self.actes_legislatifs {
            None => None,
            Some(ref actes_legislatifs) => Some(&actes_legislatifs.actes_indexes),
        }
    }

    field code_acte() -> &str {
        &self.code_acte
    }

    field libelle_acte() -> &LibelleActe {
        &self.libelle_acte
    }

    field organe(&executor) -> Option<&Organe> {
        match &self.organe_ref {
            None => None,
            Some(ref organe_ref) => {
                let context = &executor.context();
                context.get_organe_at_uid(organe_ref)
            },
        }
    }

    field organe_ref() -> Option<&str> {
        match &self.organe_ref {
            None => None,
            Some(ref s) => Some(s.as_ref()),
        }
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct EtudeImpact {
    #[serde(rename = "actesLegislatifs")]
    pub actes_legislatifs: Option<EmptyChoice>,
    #[serde(rename = "codeActe")]
    pub code_acte: String,
    #[serde(rename = "contributionInternaute")]
    pub contribution_internaute: ContributionInternaute,
    #[serde(rename = "dateActe")]
    pub date_acte: Option<DateTime<FixedOffset>>,
    #[serde(rename = "libelleActe")]
    pub libelle_acte: LibelleActe,
    #[serde(rename = "organeRef")]
    pub organe_ref: String,
    #[serde(rename = "texteAssocie")]
    pub texte_associe: String,
    pub uid: String,
}

graphql_object!(EtudeImpact: Context |&self| {
    field code_acte() -> &str {
        &self.code_acte
    }

    field contribution_internaute() -> &ContributionInternaute {
        &self.contribution_internaute
    }

    field date_acte() -> Option<DateTime<FixedOffset>> {
        self.date_acte
    }

    field libelle_acte() -> &LibelleActe {
        &self.libelle_acte
    }

    field organe(&executor) -> Option<&Organe> {
        let context = &executor.context();
        context.get_organe_at_uid(&self.organe_ref)
    }

    field organe_ref() -> Option<&str> {
        Some(self.organe_ref.as_str())
    }

    field texte_associe(&executor) -> Vec<&Document> {
        let context = &executor.context();
        match context.get_document_at_uid(&self.texte_associe) {
            None => vec![],
            Some(ref document) => {
                // Generate a flat vector from document and its sub-documents (aka divisions),
                // because a tree is not adapted to GraphQL.
                document.flatten_document()
            },
        }
    }

    field texte_associe_ref() -> &str {
        &self.texte_associe
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Clone, Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct InfoJoce {
    // TODO: Date<FixedOffset> type is not able to deserialize dates with timezone (`YYYY-MM-DD±HH:MM`).
    #[serde(rename = "dateJOCE")]
    pub date_joce: String,
    #[serde(rename = "refJOCE")]
    pub ref_joce: String,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Initiateur {
    pub acteurs: Option<ActeursInitiateur>,
    pub organes: Option<OrganesInitiateur>,
}

graphql_object!(Initiateur: Context |&self| {
    field acteurs() -> Vec<&ActeurInitiateur> {
        match &self.acteurs {
            None => vec![],
            Some(ref acteurs) =>
                acteurs.acteurs
                    .iter()
                    .collect(),
        }
    }

    field organes(&executor) -> Vec<&Organe> {
        match &self.organes {
            None => vec![],
            Some(ref organes) => {
                let context = &executor.context();
                organes.organes
                    .iter()
                    .filter_map(|ref organe| context.get_organe_at_uid(&organe.organe_ref.uid))
                    .collect()
            },
        }
    }

    field organes_refs() -> Vec<&str> {
        match &self.organes {
            None => vec![],
            Some(ref organes) =>
                organes.organes
                    .iter()
                    .map(|ref organe| organe.organe_ref.uid.as_str())
                    .collect(),
        }
    }
});

#[derive(Clone, Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct InitiateursCreationOrganeTemporaire {
    #[serde(rename = "organeRef")]
    pub organe_ref: String,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct InitiateursSaisineConseilConstit {
    #[serde(rename = "acteurRef")]
    pub acteur_ref: String,
}

#[derive(Clone, Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct LibelleActe {
    #[serde(rename = "libelleCourt")]
    pub libelle_court: Option<String>,
    #[serde(rename = "nomCanonique")]
    pub nom_canonique: String,
}

#[derive(Clone, Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct ListeActeursRef {
    #[serde(deserialize_with = "str_to_vec", rename = "acteurRef")]
    pub acteurs_refs: Vec<String>,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct MotionProcedure {
    #[serde(rename = "actesLegislatifs")]
    pub actes_legislatifs: Option<EmptyChoice>,
    #[serde(rename = "auteurMotion")]
    pub auteur_motion: String,
    #[serde(rename = "codeActe")]
    pub code_acte: String,
    #[serde(rename = "dateActe")]
    pub date_acte: Option<DateTime<FixedOffset>>,
    #[serde(rename = "dateRetrait")]
    pub date_retrait: Option<EmptyChoice>,
    #[serde(rename = "libelleActe")]
    pub libelle_acte: LibelleActe,
    #[serde(rename = "organeRef")]
    pub organe_ref: String,
    #[serde(rename = "typeMotion")]
    pub type_motion: Codier,
    pub uid: String,
    #[serde(rename = "voteRefs")]
    pub vote_refs: Option<EmptyChoice>,
}

graphql_object!(MotionProcedure: Context |&self| {
    field auteur_motion(&executor) -> Option<&Acteur> {
        let context = &executor.context();
        context.get_acteur_at_uid(&self.auteur_motion)
    }

    field auteur_motion_ref() -> &str {
        &self.auteur_motion
    }

    field code_acte() -> &str {
        &self.code_acte
    }

    field date_acte() -> Option<DateTime<FixedOffset>> {
        self.date_acte
    }

    field libelle_acte() -> &LibelleActe {
        &self.libelle_acte
    }

    field organe(&executor) -> Option<&Organe> {
        let context = &executor.context();
        context.get_organe_at_uid(&self.organe_ref)
    }

    field organe_ref() -> Option<&str> {
        Some(self.organe_ref.as_str())
    }

    field type_motion() -> &Codier {
        &self.type_motion
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct NominRapporteurs {
    #[serde(rename = "actesLegislatifs")]
    pub actes_legislatifs: Option<EmptyChoice>,
    #[serde(rename = "codeActe")]
    pub code_acte: String,
    #[serde(rename = "dateActe")]
    pub date_acte: Option<DateTime<FixedOffset>>,
    #[serde(rename = "libelleActe")]
    pub libelle_acte: LibelleActe,
    #[serde(rename = "organeRef")]
    pub organe_ref: String,
    pub rapporteurs: Rapporteurs,
    pub reunion: Option<EmptyChoice>,
    pub uid: String,
}

graphql_object!(NominRapporteurs: Context |&self| {
    field code_acte() -> &str {
        &self.code_acte
    }

    field date_acte() -> Option<DateTime<FixedOffset>> {
        self.date_acte
    }

    field libelle_acte() -> &LibelleActe {
        &self.libelle_acte
    }

    field organe(&executor) -> Option<&Organe> {
        let context = &executor.context();
        context.get_organe_at_uid(&self.organe_ref)
    }

    field organe_ref() -> Option<&str> {
        Some(self.organe_ref.as_str())
    }

    field rapporteurs() -> &Vec<Rapporteur> {
        &self.rapporteurs.rapporteurs
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct OrganeInitiateur {
    #[serde(rename = "organeRef")]
    pub organe_ref: OrganeInitiateurRef,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct OrganeInitiateurRef {
    pub uid: String,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct OrganesInitiateur {
    #[serde(deserialize_with = "map_to_vec", rename = "organe")]
    pub organes: Vec<OrganeInitiateur>,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct ProcedureAcceleree {
    #[serde(rename = "actesLegislatifs")]
    pub actes_legislatifs: Option<EmptyChoice>,
    #[serde(rename = "codeActe")]
    pub code_acte: String,
    #[serde(rename = "dateActe")]
    pub date_acte: Option<DateTime<FixedOffset>>,
    #[serde(rename = "libelleActe")]
    pub libelle_acte: LibelleActe,
    #[serde(rename = "organeRef")]
    pub organe_ref: String,
    pub uid: String,
}

graphql_object!(ProcedureAcceleree: Context |&self| {
    field code_acte() -> &str {
        &self.code_acte
    }

    field date_acte() -> Option<DateTime<FixedOffset>> {
        self.date_acte
    }

    field libelle_acte() -> &LibelleActe {
        &self.libelle_acte
    }

    field organe(&executor) -> Option<&Organe> {
        let context = &executor.context();
        context.get_organe_at_uid(&self.organe_ref)
    }

    field organe_ref() -> Option<&str> {
        Some(self.organe_ref.as_str())
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Promulgation {
    #[serde(rename = "actesLegislatifs")]
    pub actes_legislatifs: Option<EmptyChoice>,
    #[serde(rename = "codeActe")]
    pub code_acte: String,
    #[serde(rename = "codeLoi")]
    pub code_loi: String,
    #[serde(rename = "dateActe")]
    pub date_acte: Option<DateTime<FixedOffset>>,
    #[serde(rename = "infoJO")]
    pub info_jo: InfoJo,
    /// Informations sur les rectifications au Journal officiel
    #[serde(default, deserialize_with = "map_to_vec", rename = "infoJORect")]
    pub info_jo_rect: Vec<InfoJo>,
    #[serde(rename = "libelleActe")]
    pub libelle_acte: LibelleActe,
    #[serde(rename = "organeRef")]
    pub organe_ref: String,
    #[serde(rename = "referenceNOR")]
    pub reference_nor: Option<String>,
    #[serde(rename = "texteLoiRef")]
    pub texte_loi_ref: Option<String>,
    #[serde(rename = "titreLoi")]
    pub titre_loi: String,
    pub uid: String,
    #[serde(rename = "urlEcheancierLoi")]
    pub url_echeancier_loi: Option<String>,
    #[serde(rename = "urlLegifrance")]
    pub url_legifrance: Option<String>,
}

graphql_object!(Promulgation: Context |&self| {
    field code_acte() -> &str {
        &self.code_acte
    }

    field code_loi() -> &str {
        &self.code_loi
    }

    field date_acte() -> Option<DateTime<FixedOffset>> {
        self.date_acte
    }

    field info_jo() -> &InfoJo {
        &self.info_jo
    }

    field info_jo_rect() -> &Vec<InfoJo> as "Informations sur les rectifications au Journal officiel" {
        &self.info_jo_rect
    }

    field libelle_acte() -> &LibelleActe {
        &self.libelle_acte
    }

    field organe(&executor) -> Option<&Organe> {
        let context = &executor.context();
        context.get_organe_at_uid(&self.organe_ref)
    }

    field organe_ref() -> Option<&str> {
        Some(self.organe_ref.as_str())
    }

    field reference_nor() -> Option<&str> {
        match &self.reference_nor {
            None => None,
            Some(ref s) => Some(s.as_ref()),
        }
    }

    field texte_loi(&executor) -> Vec<&Document> {
        match &self.texte_loi_ref {
            None => vec![],
            Some(ref texte_loi_ref) => {
                let context = &executor.context();
                match context.get_document_at_uid(&texte_loi_ref) {
                    None => vec![],
                    Some(ref document) => {
                        // Generate a flat vector from document and its sub-documents (aka divisions),
                        // because a tree is not adapted to GraphQL.
                        document.flatten_document()
                    },
                }
            },
        }
    }

    field texte_loi_ref() -> Option<&str> {
        match &self.texte_loi_ref {
            None => None,
            Some(ref s) => Some(s.as_ref()),
        }
    }

    field titre_loi() -> &str {
        &self.titre_loi
    }

    field uid() -> &str {
        &self.uid
    }

    field url_echeancier_loi() -> Option<&str> {
        match &self.url_echeancier_loi {
            None => None,
            Some(ref s) => Some(s.as_ref()),
        }
    }

    field url_legifrance() -> Option<&str> {
        match &self.url_legifrance {
            None => None,
            Some(ref s) => Some(s.as_ref()),
        }
    }
});

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Rapporteurs {
    #[serde(default, deserialize_with = "map_to_vec", rename = "rapporteur")]
    pub rapporteurs: Vec<Rapporteur>,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct RenvoiCmp {
    #[serde(rename = "actesLegislatifs")]
    pub actes_legislatifs: Option<EmptyChoice>,
    #[serde(rename = "codeActe")]
    pub code_acte: String,
    #[serde(rename = "dateActe")]
    pub date_acte: Option<DateTime<FixedOffset>>,
    pub initiateur: Initiateur,
    #[serde(rename = "libelleActe")]
    pub libelle_acte: LibelleActe,
    #[serde(rename = "organeRef")]
    pub organe_ref: String,
    pub uid: String,
}

graphql_object!(RenvoiCmp: Context |&self| {
    field code_acte() -> &str {
        &self.code_acte
    }

    field date_acte() -> Option<DateTime<FixedOffset>> {
        self.date_acte
    }

    field initiateur() -> &Initiateur {
        &self.initiateur
    }

    field libelle_acte() -> &LibelleActe {
        &self.libelle_acte
    }

    field organe(&executor) -> Option<&Organe> {
        let context = &executor.context();
        context.get_organe_at_uid(&self.organe_ref)
    }

    field organe_ref() -> Option<&str> {
        Some(self.organe_ref.as_str())
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct RenvoiPrealable {
    #[serde(rename = "actesLegislatifs")]
    pub actes_legislatifs: Option<EmptyChoice>,
    #[serde(rename = "codeActe")]
    pub code_acte: String,
    #[serde(rename = "dateActe")]
    pub date_acte: Option<DateTime<FixedOffset>>,
    #[serde(rename = "libelleActe")]
    pub libelle_acte: LibelleActe,
    #[serde(rename = "organeRef")]
    pub organe_ref: String,
    pub uid: String,
}

graphql_object!(RenvoiPrealable: Context |&self| {
    field code_acte() -> &str {
        &self.code_acte
    }

    field date_acte() -> Option<DateTime<FixedOffset>> {
        self.date_acte
    }

    field libelle_acte() -> &LibelleActe {
        &self.libelle_acte
    }

    field organe(&executor) -> Option<&Organe> {
        let context = &executor.context();
        context.get_organe_at_uid(&self.organe_ref)
    }

    field organe_ref() -> Option<&str> {
        Some(self.organe_ref.as_str())
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct RetraitInitiative {
    #[serde(rename = "actesLegislatifs")]
    pub actes_legislatifs: Option<EmptyChoice>,
    #[serde(rename = "codeActe")]
    pub code_acte: String,
    #[serde(rename = "dateActe")]
    pub date_acte: Option<DateTime<FixedOffset>>,
    #[serde(rename = "libelleActe")]
    pub libelle_acte: LibelleActe,
    #[serde(rename = "organeRef")]
    pub organe_ref: String,
    #[serde(rename = "texteAssocie")]
    pub texte_associe: String,
    pub uid: String,
}

graphql_object!(RetraitInitiative: Context |&self| {
    field code_acte() -> &str {
        &self.code_acte
    }

    field date_acte() -> Option<DateTime<FixedOffset>> {
        self.date_acte
    }

    field libelle_acte() -> &LibelleActe {
        &self.libelle_acte
    }

    field organe(&executor) -> Option<&Organe> {
        let context = &executor.context();
        context.get_organe_at_uid(&self.organe_ref)
    }

    field organe_ref() -> Option<&str> {
        Some(self.organe_ref.as_str())
    }

    field texte_associe(&executor) -> Vec<&Document> {
        let context = &executor.context();
        match context.get_document_at_uid(&self.texte_associe) {
            None => vec![],
            Some(ref document) => {
                // Generate a flat vector from document and its sub-documents (aka divisions),
                // because a tree is not adapted to GraphQL.
                document.flatten_document()
            },
        }
    }

    field texte_associe_ref() -> &str {
        &self.texte_associe
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct SaisieComAvis {
    #[serde(rename = "actesLegislatifs")]
    pub actes_legislatifs: Option<EmptyChoice>,
    #[serde(rename = "codeActe")]
    pub code_acte: String,
    #[serde(rename = "dateActe")]
    pub date_acte: Option<DateTime<FixedOffset>>,
    #[serde(rename = "libelleActe")]
    pub libelle_acte: LibelleActe,
    #[serde(rename = "organeRef")]
    pub organe_ref: String,
    pub uid: String,
}

graphql_object!(SaisieComAvis: Context |&self| {
    field code_acte() -> &str {
        &self.code_acte
    }

    field date_acte() -> Option<DateTime<FixedOffset>> {
        self.date_acte
    }

    field libelle_acte() -> &LibelleActe {
        &self.libelle_acte
    }

    field organe(&executor) -> Option<&Organe> {
        let context = &executor.context();
        context.get_organe_at_uid(&self.organe_ref)
    }

    field organe_ref() -> Option<&str> {
        Some(self.organe_ref.as_str())
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct SaisieComFond {
    #[serde(rename = "actesLegislatifs")]
    pub actes_legislatifs: Option<EmptyChoice>,
    #[serde(rename = "codeActe")]
    pub code_acte: String,
    #[serde(rename = "dateActe")]
    pub date_acte: Option<DateTime<FixedOffset>>,
    #[serde(rename = "libelleActe")]
    pub libelle_acte: LibelleActe,
    #[serde(rename = "organeRef")]
    pub organe_ref: String,
    pub uid: String,
}

graphql_object!(SaisieComFond: Context |&self| {
    field code_acte() -> &str {
        &self.code_acte
    }

    field date_acte() -> Option<DateTime<FixedOffset>> {
        self.date_acte
    }

    field libelle_acte() -> &LibelleActe {
        &self.libelle_acte
    }

    field organe(&executor) -> Option<&Organe> {
        let context = &executor.context();
        context.get_organe_at_uid(&self.organe_ref)
    }

    field organe_ref() -> Option<&str> {
        Some(self.organe_ref.as_str())
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct SaisineConseilConstit {
    #[serde(rename = "actesLegislatifs")]
    pub actes_legislatifs: Option<EmptyChoice>,
    #[serde(rename = "casSaisine")]
    pub cas_saisine: Codier,
    #[serde(rename = "codeActe")]
    pub code_acte: String,
    #[serde(rename = "dateActe")]
    pub date_acte: Option<DateTime<FixedOffset>>,
    pub initiateurs: Option<InitiateursSaisineConseilConstit>,
    #[serde(rename = "libelleActe")]
    pub libelle_acte: LibelleActe,
    pub motif: String,
    #[serde(rename = "organeRef")]
    pub organe_ref: String,
    pub uid: String,
}

graphql_object!(SaisineConseilConstit: Context |&self| {
    field cas_saisine() -> &Codier {
        &self.cas_saisine
    }

    field code_acte() -> &str {
        &self.code_acte
    }

    field date_acte() -> Option<DateTime<FixedOffset>> {
        self.date_acte
    }

    field initiateurs(&executor) -> Option<&Acteur> {
        match &self.initiateurs {
            None => None,
            Some(ref initiateurs) => {
                let context = &executor.context();
                context.get_acteur_at_uid(&initiateurs.acteur_ref)
            },
        }
    }

    field initiateurs_ref() -> Option<&str> {
        match &self.initiateurs {
            None => None,
            Some(ref initiateurs) => Some(initiateurs.acteur_ref.as_str()),
        }
    }

    field libelle_acte() -> &LibelleActe {
        &self.libelle_acte
    }

    field motif() -> &str {
        &self.motif
    }

    field organe(&executor) -> Option<&Organe> {
        let context = &executor.context();
        context.get_organe_at_uid(&self.organe_ref)
    }

    field organe_ref() -> Option<&str> {
        Some(self.organe_ref.as_str())
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct TexteAssocie {
    #[serde(rename = "refTexteAssocie")]
    pub ref_texte_associe: String,
    #[serde(rename = "typeTexte")]
    pub type_texte: String,
}

graphql_object!(TexteAssocie: Context |&self| {
    field ref_texte_associe() -> &str {
        &self.ref_texte_associe
    }

    field texte_associe(&executor) -> Vec<&Document> {
        let context = &executor.context();
        match context.get_document_at_uid(&self.ref_texte_associe) {
            None => vec![],
            Some(ref document) => {
                // Generate a flat vector from document and its sub-documents (aka divisions),
                // because a tree is not adapted to GraphQL.
                document.flatten_document()
            },
        }
    }

    field type_texte() -> &str {
        &self.type_texte
    }
});

#[derive(Clone, Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct TexteEuropeen {
    #[serde(rename = "titreTexteEuropeen")]
    pub titre_texte_europeen: String,
    #[serde(rename = "typeTexteEuropeen")]
    pub type_texte_europeen: String,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct TextesAssocies {
    #[serde(default, deserialize_with = "map_to_vec", rename = "texteAssocie")]
    pub textes_associes: Vec<TexteAssocie>,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct VoteRefs {
    #[serde(deserialize_with = "str_to_vec", rename = "voteRef")]
    pub vote_refs: Vec<String>,
}
