use chrono::{Date, FixedOffset};
use std::cmp;

use crate::actes_legislatifs::{ActeLegislatif, ActesLegislatifs, Initiateur};
use crate::commun::{Indexation, Rapporteur, XmlNamespace};
use crate::contexts::Context;
use crate::organes::Organe;
use crate::serde_utils::map_to_vec;
use crate::textes_legislatifs::{Document, TextesLegislatifs};

#[derive(Clone, Copy, Debug, Deserialize, GraphQLEnum)]
pub enum CauseFusionDossier {
    #[serde(rename = "Dossier absorbé")]
    DossierAbsorbe,
    #[serde(rename = "Examen commun")]
    ExamenCommun,
}

#[derive(Clone, Copy, Debug, Deserialize, GraphQLEnum)]
pub enum CodeProcedureParlementaire {
    #[graphql(name = "PROCEDURE_PARLEMENTAIRE_1")]
    #[serde(rename = "1")]
    /// Projet de loi ordinaire
    ProjetLoiOrdinaire,

    #[graphql(name = "PROCEDURE_PARLEMENTAIRE_2")]
    #[serde(rename = "2")]
    /// Proposition de loi ordinaire
    PropositionLoiOrdinaire,

    #[graphql(name = "PROCEDURE_PARLEMENTAIRE_3")]
    #[serde(rename = "3")]
    /// Projet de loi de finances de l'année
    ProjetLoiFinancesAnnee,

    #[graphql(name = "PROCEDURE_PARLEMENTAIRE_4")]
    #[serde(rename = "4")]
    /// Projet de loi de financement de la sécurité sociale
    ProjetLoiFinancementSecuriteSociale,

    #[graphql(name = "PROCEDURE_PARLEMENTAIRE_5")]
    #[serde(rename = "5")]
    /// Projet ou proposition de loi organique
    ProjetOuPropositionLoiOrganique,

    #[graphql(name = "PROCEDURE_PARLEMENTAIRE_6")]
    #[serde(rename = "6")]
    /// Projet de ratification des traités et conventions
    ProjetRatificationTraitesEtConventions,

    #[graphql(name = "PROCEDURE_PARLEMENTAIRE_7")]
    #[serde(rename = "7")]
    /// Projet ou proposition de loi constitutionnelle
    ProjetOuPropositionLoiConstitutionnelle,

    #[graphql(name = "PROCEDURE_PARLEMENTAIRE_8")]
    #[serde(rename = "8")]
    /// Résolution
    Resolution,

    #[graphql(name = "PROCEDURE_PARLEMENTAIRE_9")]
    #[serde(rename = "9")]
    /// Commission d'enquête
    CommissionEnquete,

    #[graphql(name = "PROCEDURE_PARLEMENTAIRE_10")]
    #[serde(rename = "10")]
    /// Mission d'information
    MissionInformation,

    #[graphql(name = "PROCEDURE_PARLEMENTAIRE_13")]
    #[serde(rename = "13")]
    /// Engagement de la responsabilité gouvernementale
    EngagementResponsabiliteGouvernementale,

    #[graphql(name = "PROCEDURE_PARLEMENTAIRE_14")]
    #[serde(rename = "14")]
    /// Responsabilité pénale du président de la république
    ResponsabilitePenalePresidentRepublique,

    #[graphql(name = "PROCEDURE_PARLEMENTAIRE_15")]
    #[serde(rename = "15")]
    /// Immunité
    Immunite,

    #[graphql(name = "PROCEDURE_PARLEMENTAIRE_17")]
    #[serde(rename = "17")]
    /// Motion référendaire
    MotionReferendaire,

    #[graphql(name = "PROCEDURE_PARLEMENTAIRE_19")]
    #[serde(rename = "19")]
    /// Rapport d'information sans mission
    RapportInformationSansMission,

    #[graphql(name = "PROCEDURE_PARLEMENTAIRE_20")]
    #[serde(rename = "20")]
    /// Allocution du Président de l'Assemblée nationale
    AllocutionPresidentAssembleeNationale,

    #[graphql(name = "PROCEDURE_PARLEMENTAIRE_21")]
    #[serde(rename = "21")]
    /// Projet de loi de finances rectificative
    ProjetLoiFinancesRectificative,

    #[graphql(name = "PROCEDURE_PARLEMENTAIRE_22")]
    #[serde(rename = "22")]
    /// Résolution Article 34-1
    ResolutionArticle34_1,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Dossier {
    #[serde(rename = "dossierParlementaire")]
    pub dossier_parlementaire: DossierParlementaire,
}

graphql_object!(Dossier: Context |&self| {
    field dossier_parlementaire() -> &DossierParlementaire {
        &self.dossier_parlementaire
    }
});

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct DossierCommissionEnquete {
    #[serde(rename = "actesLegislatifs")]
    pub actes_legislatifs: ActesLegislatifs,
    #[serde(rename = "fusionDossier")]
    pub fusion_dossier: Option<FusionDossier>,
    pub indexation: Option<Indexation>,
    pub initiateur: Option<Initiateur>,
    pub legislature: String, // TODO: Convert to u8.
    #[serde(rename = "procedureParlementaire")]
    pub procedure_parlementaire: ProcedureParlementaire,
    #[serde(rename = "titreDossier")]
    pub titre_dossier: TitreDossier,
    pub uid: String,
}

graphql_object!(DossierCommissionEnquete: Context |&self| {
    field actes_legislatifs() -> &ActesLegislatifs {
        &self.actes_legislatifs
    }

    field fusion_dossier() -> &Option<FusionDossier> {
        &self.fusion_dossier
    }

    field indexation() -> &Option<Indexation> {
        &self.indexation
    }

    field initiateur() -> &Option<Initiateur> {
        &self.initiateur
    }

    field legislature() -> &str {
        &self.legislature
    }

    field procedure_parlementaire() -> &ProcedureParlementaire {
        &self.procedure_parlementaire
    }

    field titre_dossier() -> &TitreDossier {
        &self.titre_dossier
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct DossierInitiativeExecutif {
    #[serde(rename = "actesLegislatifs")]
    pub actes_legislatifs: ActesLegislatifs,
    #[serde(rename = "fusionDossier")]
    pub fusion_dossier: Option<FusionDossier>,
    pub indexation: Option<Indexation>,
    pub initiateur: Option<Initiateur>,
    pub legislature: String, // TODO: Convert to u8.
    #[serde(rename = "procedureParlementaire")]
    pub procedure_parlementaire: ProcedureParlementaire,
    #[serde(rename = "titreDossier")]
    pub titre_dossier: TitreDossier,
    pub uid: String,
}

graphql_object!(DossierInitiativeExecutif: Context |&self| {
    field actes_legislatifs() -> &ActesLegislatifs {
        &self.actes_legislatifs
    }

    field fusion_dossier() -> &Option<FusionDossier> {
        &self.fusion_dossier
    }

    field indexation() -> &Option<Indexation> {
        &self.indexation
    }

    field initiateur() -> &Option<Initiateur> {
        &self.initiateur
    }

    field legislature() -> &str {
        &self.legislature
    }

    field procedure_parlementaire() -> &ProcedureParlementaire {
        &self.procedure_parlementaire
    }

    field titre_dossier() -> &TitreDossier {
        &self.titre_dossier
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct DossierLegislatif {
    #[serde(rename = "actesLegislatifs")]
    pub actes_legislatifs: ActesLegislatifs,
    #[serde(rename = "fusionDossier")]
    pub fusion_dossier: Option<FusionDossier>,
    pub indexation: Option<Indexation>,
    pub initiateur: Option<Initiateur>,
    pub legislature: String, // TODO: Convert to u8.
    #[serde(rename = "PLF")]
    pub plf: Option<Plf>,
    #[serde(rename = "procedureParlementaire")]
    pub procedure_parlementaire: ProcedureParlementaire,
    #[serde(rename = "titreDossier")]
    pub titre_dossier: TitreDossier,
    pub uid: String,
}

graphql_object!(DossierLegislatif: Context |&self| {
    field actes_legislatifs() -> &ActesLegislatifs {
        &self.actes_legislatifs
    }

    field fusion_dossier() -> &Option<FusionDossier> {
        &self.fusion_dossier
    }

    field indexation() -> &Option<Indexation> {
        &self.indexation
    }

    field initiateur() -> &Option<Initiateur> {
        &self.initiateur
    }

    field legislature() -> &str {
        &self.legislature
    }

    field plf() -> Vec<&EtudePlf> {
        match &self.plf {
            None => vec![],
            Some(ref plf) => {
                plf.etudes_plf
                    .iter()
                    .collect()
            },
        }
    }

    field procedure_parlementaire() -> &ProcedureParlementaire {
        &self.procedure_parlementaire
    }

    field titre_dossier() -> &TitreDossier {
        &self.titre_dossier
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct DossierMissionControle {
    #[serde(rename = "actesLegislatifs")]
    pub actes_legislatifs: ActesLegislatifs,
    #[serde(rename = "fusionDossier")]
    pub fusion_dossier: Option<FusionDossier>,
    pub indexation: Option<Indexation>,
    pub initiateur: Option<Initiateur>,
    pub legislature: String, // TODO: Convert to u8.
    #[serde(rename = "procedureParlementaire")]
    pub procedure_parlementaire: ProcedureParlementaire,
    #[serde(rename = "titreDossier")]
    pub titre_dossier: TitreDossier,
    pub uid: String,
}

graphql_object!(DossierMissionControle: Context |&self| {
    field actes_legislatifs() -> &ActesLegislatifs {
        &self.actes_legislatifs
    }

    field fusion_dossier() -> &Option<FusionDossier> {
        &self.fusion_dossier
    }

    field indexation() -> &Option<Indexation> {
        &self.indexation
    }

    field initiateur() -> &Option<Initiateur> {
        &self.initiateur
    }

    field legislature() -> &str {
        &self.legislature
    }

    field procedure_parlementaire() -> &ProcedureParlementaire {
        &self.procedure_parlementaire
    }

    field titre_dossier() -> &TitreDossier {
        &self.titre_dossier
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct DossierMissionInformation {
    #[serde(rename = "actesLegislatifs")]
    pub actes_legislatifs: ActesLegislatifs,
    #[serde(rename = "fusionDossier")]
    pub fusion_dossier: Option<FusionDossier>,
    pub indexation: Option<Indexation>,
    pub initiateur: Option<Initiateur>,
    pub legislature: String, // TODO: Convert to u8.
    #[serde(rename = "procedureParlementaire")]
    pub procedure_parlementaire: ProcedureParlementaire,
    #[serde(rename = "titreDossier")]
    pub titre_dossier: TitreDossier,
    pub uid: String,
}

graphql_object!(DossierMissionInformation: Context |&self| {
    field actes_legislatifs() -> &ActesLegislatifs {
        &self.actes_legislatifs
    }

    field fusion_dossier() -> &Option<FusionDossier> {
        &self.fusion_dossier
    }

    field indexation() -> &Option<Indexation> {
        &self.indexation
    }

    field initiateur() -> &Option<Initiateur> {
        &self.initiateur
    }

    field legislature() -> &str {
        &self.legislature
    }

    field procedure_parlementaire() -> &ProcedureParlementaire {
        &self.procedure_parlementaire
    }

    field titre_dossier() -> &TitreDossier {
        &self.titre_dossier
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Debug, Deserialize)]
#[serde(tag = "@xsi:type")]
pub enum DossierParlementaire {
    #[serde(rename = "DossierCommissionEnquete_Type")]
    DossierCommissionEnquete(DossierCommissionEnquete),
    #[serde(rename = "DossierIniativeExecutif_Type")]
    DossierInitiativeExecutif(DossierInitiativeExecutif),
    #[serde(rename = "DossierLegislatif_Type")]
    DossierLegislatif(DossierLegislatif),
    #[serde(rename = "DossierMissionControle_Type")]
    DossierMissionControle(DossierMissionControle),
    #[serde(rename = "DossierMissionInformation_Type")]
    DossierMissionInformation(DossierMissionInformation),
    DossierResolutionAN(DossierResolutionAN),
    #[serde(rename = "")]
    DossierSansType(DossierSansType),
}

impl DossierParlementaire {
    pub fn actes_legislatifs(&self) -> &ActesLegislatifs {
        match *self {
            DossierParlementaire::DossierCommissionEnquete(DossierCommissionEnquete {
                ref actes_legislatifs,
                ..
            })
            | DossierParlementaire::DossierInitiativeExecutif(DossierInitiativeExecutif {
                ref actes_legislatifs,
                ..
            })
            | DossierParlementaire::DossierLegislatif(DossierLegislatif {
                ref actes_legislatifs,
                ..
            })
            | DossierParlementaire::DossierMissionControle(DossierMissionControle {
                ref actes_legislatifs,
                ..
            })
            | DossierParlementaire::DossierMissionInformation(DossierMissionInformation {
                ref actes_legislatifs,
                ..
            })
            | DossierParlementaire::DossierResolutionAN(DossierResolutionAN {
                ref actes_legislatifs,
                ..
            })
            | DossierParlementaire::DossierSansType(DossierSansType {
                ref actes_legislatifs,
                ..
            }) => &actes_legislatifs,
        }
    }

    pub fn index_actes_legislatifs(&mut self) {
        match *self {
            DossierParlementaire::DossierCommissionEnquete(DossierCommissionEnquete {
                ref mut actes_legislatifs,
                ..
            })
            | DossierParlementaire::DossierInitiativeExecutif(DossierInitiativeExecutif {
                ref mut actes_legislatifs,
                ..
            })
            | DossierParlementaire::DossierLegislatif(DossierLegislatif {
                ref mut actes_legislatifs,
                ..
            })
            | DossierParlementaire::DossierMissionControle(DossierMissionControle {
                ref mut actes_legislatifs,
                ..
            })
            | DossierParlementaire::DossierMissionInformation(DossierMissionInformation {
                ref mut actes_legislatifs,
                ..
            })
            | DossierParlementaire::DossierResolutionAN(DossierResolutionAN {
                ref mut actes_legislatifs,
                ..
            })
            | DossierParlementaire::DossierSansType(DossierSansType {
                ref mut actes_legislatifs,
                ..
            }) => {
                let mut actes_indexes: Vec<i32> = Vec::new();
                let mut index: i32 = 0;
                for acte in &mut actes_legislatifs.actes {
                    actes_indexes.push(index);
                    index += 1;
                    if let ActeLegislatif::Etape(ref mut etape) = acte {
                        etape.index_actes_legislatifs(&mut index);
                    }
                }
                actes_legislatifs.actes_indexes = actes_indexes;
            }
        }
    }

    pub fn distance_min_date_actes_legislatifs_futurs(
        &self,
        date: Date<FixedOffset>,
    ) -> Option<i64> {
        let mut min: Option<i64> = None;
        let actes_legislatifs = self.actes_legislatifs();
        for acte_legislatif in &actes_legislatifs.actes {
            let min_acte = acte_legislatif.distance_min_date_actes_legislatifs_futurs(date);
            min = match (min, min_acte) {
                (Some(min), Some(min_acte)) => Some(cmp::min(min, min_acte)),
                (None, Some(min_acte)) => Some(min_acte),
                _ => min,
            }
        }
        min
    }

    pub fn distance_min_date_actes_legislatifs_passes(
        &self,
        date: Date<FixedOffset>,
    ) -> Option<i64> {
        let mut min: Option<i64> = None;
        let actes_legislatifs = self.actes_legislatifs();
        for acte_legislatif in &actes_legislatifs.actes {
            let min_acte = acte_legislatif.distance_min_date_actes_legislatifs_passes(date);
            min = match (min, min_acte) {
                (Some(min), Some(min_acte)) => Some(cmp::min(min, min_acte)),
                (None, Some(min_acte)) => Some(min_acte),
                _ => min,
            }
        }
        min
    }

    // pub fn fusion_dossier(&self) -> &Option<FusionDossier> {
    //     match *self {
    //         DossierParlementaire::DossierCommissionEnquete(DossierCommissionEnquete {
    //             ref fusion_dossier,
    //             ..
    //         })
    //         | DossierParlementaire::DossierInitiativeExecutif(DossierInitiativeExecutif {
    //             ref fusion_dossier,
    //             ..
    //         })
    //         | DossierParlementaire::DossierLegislatif(DossierLegislatif {
    //             ref fusion_dossier,
    //             ..
    //         })
    //         | DossierParlementaire::DossierMissionControle(DossierMissionControle {
    //             ref fusion_dossier,
    //             ..
    //         })
    //         | DossierParlementaire::DossierMissionInformation(DossierMissionInformation {
    //             ref fusion_dossier,
    //             ..
    //         })
    //         | DossierParlementaire::DossierResolutionAN(DossierResolutionAN {
    //             ref fusion_dossier,
    //             ..
    //         })
    //         | DossierParlementaire::DossierSansType(DossierSansType {
    //             ref fusion_dossier, ..
    //         }) => &fusion_dossier,
    //     }
    // }

    // pub fn indexation(&self) -> &Option<Indexation> {
    //     match *self {
    //         DossierParlementaire::DossierCommissionEnquete(DossierCommissionEnquete {
    //             ref indexation,
    //             ..
    //         })
    //         | DossierParlementaire::DossierInitiativeExecutif(DossierInitiativeExecutif {
    //             ref indexation,
    //             ..
    //         })
    //         | DossierParlementaire::DossierLegislatif(DossierLegislatif {
    //             ref indexation, ..
    //         })
    //         | DossierParlementaire::DossierMissionControle(DossierMissionControle {
    //             ref indexation,
    //             ..
    //         })
    //         | DossierParlementaire::DossierMissionInformation(DossierMissionInformation {
    //             ref indexation,
    //             ..
    //         })
    //         | DossierParlementaire::DossierResolutionAN(DossierResolutionAN {
    //             ref indexation,
    //             ..
    //         })
    //         | DossierParlementaire::DossierSansType(DossierSansType { ref indexation, .. }) => {
    //             &indexation
    //         }
    //     }
    // }

    // pub fn initiateur(&self) -> &Option<Initiateur> {
    //     match *self {
    //         DossierParlementaire::DossierCommissionEnquete(DossierCommissionEnquete {
    //             ref initiateur,
    //             ..
    //         })
    //         | DossierParlementaire::DossierInitiativeExecutif(DossierInitiativeExecutif {
    //             ref initiateur,
    //             ..
    //         })
    //         | DossierParlementaire::DossierLegislatif(DossierLegislatif {
    //             ref initiateur, ..
    //         })
    //         | DossierParlementaire::DossierMissionControle(DossierMissionControle {
    //             ref initiateur,
    //             ..
    //         })
    //         | DossierParlementaire::DossierMissionInformation(DossierMissionInformation {
    //             ref initiateur,
    //             ..
    //         })
    //         | DossierParlementaire::DossierResolutionAN(DossierResolutionAN {
    //             ref initiateur,
    //             ..
    //         })
    //         | DossierParlementaire::DossierSansType(DossierSansType { ref initiateur, .. }) => {
    //             &initiateur
    //         }
    //     }
    // }

    // pub fn legislature(&self) -> &str {
    //     match *self {
    //         DossierParlementaire::DossierCommissionEnquete(DossierCommissionEnquete {
    //             ref legislature,
    //             ..
    //         })
    //         | DossierParlementaire::DossierInitiativeExecutif(DossierInitiativeExecutif {
    //             ref legislature,
    //             ..
    //         })
    //         | DossierParlementaire::DossierLegislatif(DossierLegislatif {
    //             ref legislature, ..
    //         })
    //         | DossierParlementaire::DossierMissionControle(DossierMissionControle {
    //             ref legislature,
    //             ..
    //         })
    //         | DossierParlementaire::DossierMissionInformation(DossierMissionInformation {
    //             ref legislature,
    //             ..
    //         })
    //         | DossierParlementaire::DossierResolutionAN(DossierResolutionAN {
    //             ref legislature,
    //             ..
    //         })
    //         | DossierParlementaire::DossierSansType(DossierSansType {
    //             ref legislature, ..
    //         }) => &legislature,
    //     }
    // }

    // pub fn procedure_parlementaire(&self) -> &ProcedureParlementaire {
    //     match *self {
    //         DossierParlementaire::DossierCommissionEnquete(DossierCommissionEnquete {
    //             ref procedure_parlementaire,
    //             ..
    //         })
    //         | DossierParlementaire::DossierInitiativeExecutif(DossierInitiativeExecutif {
    //             ref procedure_parlementaire,
    //             ..
    //         })
    //         | DossierParlementaire::DossierLegislatif(DossierLegislatif {
    //             ref procedure_parlementaire,
    //             ..
    //         })
    //         | DossierParlementaire::DossierMissionControle(DossierMissionControle {
    //             ref procedure_parlementaire,
    //             ..
    //         })
    //         | DossierParlementaire::DossierMissionInformation(DossierMissionInformation {
    //             ref procedure_parlementaire,
    //             ..
    //         })
    //         | DossierParlementaire::DossierResolutionAN(DossierResolutionAN {
    //             ref procedure_parlementaire,
    //             ..
    //         })
    //         | DossierParlementaire::DossierSansType(DossierSansType {
    //             ref procedure_parlementaire,
    //             ..
    //         }) => &procedure_parlementaire,
    //     }
    // }

    pub fn titre_dossier(&self) -> &TitreDossier {
        match *self {
            DossierParlementaire::DossierCommissionEnquete(DossierCommissionEnquete {
                ref titre_dossier,
                ..
            })
            | DossierParlementaire::DossierInitiativeExecutif(DossierInitiativeExecutif {
                ref titre_dossier,
                ..
            })
            | DossierParlementaire::DossierLegislatif(DossierLegislatif {
                ref titre_dossier,
                ..
            })
            | DossierParlementaire::DossierMissionControle(DossierMissionControle {
                ref titre_dossier,
                ..
            })
            | DossierParlementaire::DossierMissionInformation(DossierMissionInformation {
                ref titre_dossier,
                ..
            })
            | DossierParlementaire::DossierResolutionAN(DossierResolutionAN {
                ref titre_dossier,
                ..
            })
            | DossierParlementaire::DossierSansType(DossierSansType {
                ref titre_dossier, ..
            }) => &titre_dossier,
        }
    }

    pub fn uid(&self) -> &str {
        match *self {
            DossierParlementaire::DossierCommissionEnquete(DossierCommissionEnquete {
                ref uid,
                ..
            })
            | DossierParlementaire::DossierInitiativeExecutif(DossierInitiativeExecutif {
                ref uid,
                ..
            })
            | DossierParlementaire::DossierLegislatif(DossierLegislatif { ref uid, .. })
            | DossierParlementaire::DossierMissionControle(DossierMissionControle {
                ref uid,
                ..
            })
            | DossierParlementaire::DossierMissionInformation(DossierMissionInformation {
                ref uid,
                ..
            })
            | DossierParlementaire::DossierResolutionAN(DossierResolutionAN { ref uid, .. })
            | DossierParlementaire::DossierSansType(DossierSansType { ref uid, .. }) => &uid,
        }
    }
}

impl PartialEq for DossierParlementaire {
    fn eq(&self, other: &Self) -> bool {
        self.uid() == other.uid()
    }
}

// Using graphql_union because instance_resolvers of graphql_interface does not work with Juniper 0.9.2.
// graphql_interface!(DossierParlementaire: () |&self| {
graphql_union!(DossierParlementaire: Context |&self| {
    // field actes_legislatifs() -> &ActesLegislatifs {
    //     self.actes_legislatifs()
    // }

    // field fusion_dossier() -> &Option<FusionDossier> {
    //     self.fusion_dossier()
    // }

    // field indexation() -> &Option<Indexation> {
    //     self.indexation()
    // }

    // field initiateur() -> &Option<Initiateur> {
    //     self.initiateur()
    // }

    // field legislature() -> &str {
    //     self.legislature()
    // }

    // field procedure_parlementaire() -> &ProcedureParlementaire {
    //     self.procedure_parlementaire()
    // }

    // field titre_dossier() -> &TitreDossier {
    //     self.titre_dossier()
    // }

    // field uid() -> &str {
    //     self.uid()
    // }

    instance_resolvers: |_| {
        &DossierCommissionEnquete => match *self {
            DossierParlementaire::DossierCommissionEnquete(ref dossier) => Some(dossier),
            _ => None,
        },
        &DossierInitiativeExecutif => match *self {
            DossierParlementaire::DossierInitiativeExecutif(ref dossier) => Some(dossier),
            _ => None,
        },
        &DossierLegislatif => match *self {
            DossierParlementaire::DossierLegislatif(ref dossier) => Some(dossier),
            _ => None,
        },
        &DossierMissionControle => match *self {
            DossierParlementaire::DossierMissionControle(ref dossier) => Some(dossier),
            _ => None,
        },
        &DossierMissionInformation => match *self {
            DossierParlementaire::DossierMissionInformation(ref dossier) => Some(dossier),
            _ => None,
        },
        &DossierResolutionAN => match *self {
            DossierParlementaire::DossierResolutionAN(ref dossier) => Some(dossier),
            _ => None,
        },
        &DossierSansType => match *self {
            DossierParlementaire::DossierSansType(ref dossier) => Some(dossier),
            _ => None,
        },
    }
});

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct DossierResolutionAN {
    #[serde(rename = "actesLegislatifs")]
    pub actes_legislatifs: ActesLegislatifs,
    #[serde(rename = "fusionDossier")]
    pub fusion_dossier: Option<FusionDossier>,
    pub indexation: Option<Indexation>,
    pub initiateur: Option<Initiateur>,
    pub legislature: String, // TODO: Convert to u8.
    #[serde(rename = "procedureParlementaire")]
    pub procedure_parlementaire: ProcedureParlementaire,
    #[serde(rename = "titreDossier")]
    pub titre_dossier: TitreDossier,
    pub uid: String,
}

graphql_object!(DossierResolutionAN: Context |&self| {
    field actes_legislatifs() -> &ActesLegislatifs {
        &self.actes_legislatifs
    }

    field fusion_dossier() -> &Option<FusionDossier> {
        &self.fusion_dossier
    }

    field indexation() -> &Option<Indexation> {
        &self.indexation
    }

    field initiateur() -> &Option<Initiateur> {
        &self.initiateur
    }

    field legislature() -> &str {
        &self.legislature
    }

    field procedure_parlementaire() -> &ProcedureParlementaire {
        &self.procedure_parlementaire
    }

    field titre_dossier() -> &TitreDossier {
        &self.titre_dossier
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct DossierSansType {
    #[serde(rename = "actesLegislatifs")]
    pub actes_legislatifs: ActesLegislatifs,
    #[serde(rename = "fusionDossier")]
    pub fusion_dossier: Option<FusionDossier>,
    pub indexation: Option<Indexation>,
    pub initiateur: Option<Initiateur>,
    pub legislature: String, // TODO: Convert to u8.
    #[serde(rename = "procedureParlementaire")]
    pub procedure_parlementaire: ProcedureParlementaire,
    #[serde(rename = "titreDossier")]
    pub titre_dossier: TitreDossier,
    pub uid: String,
}

graphql_object!(DossierSansType: Context |&self| {
    field actes_legislatifs() -> &ActesLegislatifs {
        &self.actes_legislatifs
    }

    field fusion_dossier() -> &Option<FusionDossier> {
        &self.fusion_dossier
    }

    field indexation() -> &Option<Indexation> {
        &self.indexation
    }

    field initiateur() -> &Option<Initiateur> {
        &self.initiateur
    }

    field legislature() -> &str {
        &self.legislature
    }

    field procedure_parlementaire() -> &ProcedureParlementaire {
        &self.procedure_parlementaire
    }

    field titre_dossier() -> &TitreDossier {
        &self.titre_dossier
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct DossiersLegislatifs {
    #[serde(rename = "dossier")]
    pub dossiers: Vec<Dossier>,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct EtudePlf {
    #[serde(rename = "missionMinefi")]
    pub mission_minefi: Option<Mission>,
    #[serde(rename = "ordreCommission")]
    pub ordre_commission: String, // TODO: Convert to u32.
    #[serde(rename = "ordreDIQS")]
    pub ordre_diqs: String, // TODO: Convert to u32.
    #[serde(rename = "organeRef")]
    pub organe_ref: String,
    #[serde(default, deserialize_with = "map_to_vec", rename = "rapporteur")]
    pub rapporteurs: Vec<Rapporteur>,
    #[serde(rename = "texteAssocie")]
    pub texte_associe: Option<String>,
    pub uid: String,
}

graphql_object!(EtudePlf: Context |&self| {
    field mission_minefi() -> &Option<Mission> {
        &self.mission_minefi
    }

    field ordre_commission() -> &str {
        &self.ordre_commission
    }

    field ordre_diqs() -> &str {
        &self.ordre_diqs
    }

    field organe(&executor) -> Option<&Organe> {
        let context = &executor.context();
        context.get_organe_at_uid(&self.organe_ref)
    }

    field organe_ref() -> &str {
        &self.organe_ref
    }

    field rapporteurs() -> &Vec<Rapporteur> {
        &self.rapporteurs
    }

    field texte_associe(&executor) -> Vec<&Document> {
        match &self.texte_associe {
            None => vec![],
            Some(ref texte_associe_ref) => {
                let context = &executor.context();
                match context.get_document_at_uid(&texte_associe_ref) {
                    None => vec![],
                    Some(ref document) => {
                        // Generate a flat vector from document and its sub-documents (aka divisions),
                        // because a tree is not adapted to GraphQL.
                        document.flatten_document()
                    },
                }
            },
        }
    }

    field texte_associe_ref() -> Option<&str> {
        match &self.texte_associe {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Export {
    #[serde(rename = "dossiersLegislatifs")]
    pub dossiers_legislatifs: DossiersLegislatifs,
    #[serde(rename = "textesLegislatifs")]
    pub textes_legislatifs: TextesLegislatifs,
    #[serde(rename = "@xmlns:xsi")]
    xml_xsi: XmlNamespace,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct ExportJsonWrapper {
    pub export: Export,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct FusionDossier {
    pub cause: CauseFusionDossier,
    #[serde(rename = "dossierAbsorbantRef")]
    pub dossier_absorbant_ref: String,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Mission {
    #[serde(rename = "libelleCourt")]
    pub libelle_court: Option<String>,
    #[serde(rename = "libelleLong")]
    pub libelle_long: String,
    pub missions: Option<Missions>,
    #[serde(rename = "typeBudget")]
    pub type_budget: TypeBudget,
    #[serde(rename = "typeMission")]
    pub type_mission: TypeMission,
}

graphql_object!(Mission: Context |&self| {
    field libelle_court() -> Option<&str> {
        match &self.libelle_court {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field libelle_long() -> &str {
        &self.libelle_long
    }

    field missions() -> Vec<&MissionBase> {
        match &self.missions {
            None => vec![],
            Some(ref missions) =>
                missions.missions
                    .iter()
                    .collect(),
        }
    }

    field type_budget() -> TypeBudget {
        self.type_budget
    }

    field type_mission() -> TypeMission {
        self.type_mission
    }
});

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct MissionBase {
    #[serde(rename = "libelleCourt")]
    pub libelle_court: Option<String>,
    #[serde(rename = "libelleLong")]
    pub libelle_long: String,
    #[serde(rename = "typeBudget")]
    pub type_budget: TypeBudget,
    #[serde(rename = "typeMission")]
    pub type_mission: TypeMission,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct Missions {
    #[serde(deserialize_with = "map_to_vec", rename = "mission")]
    pub missions: Vec<MissionBase>,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Plf {
    #[serde(rename = "EtudePLF")]
    pub etudes_plf: Vec<EtudePlf>,
}

#[derive(Clone, Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct ProcedureParlementaire {
    pub code: CodeProcedureParlementaire,
    pub libelle: String,
}

#[derive(Clone, Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct TitreDossier {
    pub titre: String,
    #[serde(rename = "titreChemin")]
    pub titre_chemin: Option<String>,
    #[serde(rename = "senatChemin")]
    pub senat_chemin: Option<String>,
}

#[derive(Clone, Copy, Debug, Deserialize, GraphQLEnum)]
pub enum TypeBudget {
    #[serde(rename = "Budget annexe")]
    BudgetAnnexe,
    #[serde(rename = "Budget général")]
    BudgetGeneral,
    #[serde(rename = "Compte de commerce et d'opérations monétaires")]
    CompteDeCommerceEtDOperationsMonetaires,
    #[serde(rename = "Compte de concours financier")]
    CompteDeConcoursFinancier,
    #[serde(rename = "Compte spécial")]
    CompteSpecial,
    #[serde(rename = "Première partie")]
    PremierePartie,
}

#[derive(Clone, Copy, Debug, Deserialize, GraphQLEnum)]
pub enum TypeMission {
    #[serde(rename = "mission principale")]
    MissionPrincipale,
    #[serde(rename = "mission secondaire")]
    MissionSecondaire,
    #[serde(rename = "partie de mission")]
    PartieDeMission,
}
