#![feature(proc_macro_hygiene, decl_macro)]

extern crate chrono;
extern crate clap;
extern crate env_logger;
extern crate itertools;
#[macro_use]
extern crate juniper;
extern crate juniper_rocket;
#[macro_use]
extern crate rocket;
extern crate rocket_cors;
extern crate serde;
extern crate serde_json;
extern crate tricoteuses_api_assemblee_config as config;
extern crate tricoteuses_api_assemblee_data as data;

use chrono::prelude::*;
use clap::{App, Arg};
use config::Verbosity;
use data::{
    Acteur, Amendement, CodeTypeOrgane, Context, Document, DossierParlementaire, FonctionOrgane,
    Mandat, Organe, Reunion, Scrutin, Statut, TexteLeg, TextesEtAmendements, ALL_DATASETS,
    FONCTIONS_ORGANES,
};
use juniper::{EmptyMutation, FieldResult, RootNode};
use rocket::http::Method;
use rocket::response::{content, NamedFile};
use rocket::State;
use rocket_cors::AllowedOrigins;
use std::path::{Path, PathBuf};

struct Query;

graphql_object!(Query: Context |&self| {
    field acteur(&executor, uid: String) -> FieldResult<Option<&Acteur>> {
        Ok(executor.context().get_acteur_at_uid(&uid))
    }

    field acteurs(&executor) -> FieldResult<Vec<&Acteur>> {
        let acteurs: Vec<&Acteur> = executor.context().acteur_by_uid
            .values()
            .map(|acteur| unsafe { &*(*acteur) })
            .collect();
        Ok(acteurs)
    }

    field agenda(&executor, acteur_uid: Option<String>, date_debut: Option<String>, date_fin: Option<String>) -> FieldResult<Vec<&Reunion>> {
        let context = executor.context();
        let date_debut = date_debut.map(|date_debut| DateTime::parse_from_rfc3339(&date_debut).unwrap_or_else(|_| panic!("Invalid date & time: {}", date_debut)).date());
        let date_fin = date_fin.map(|date_fin| DateTime::parse_from_rfc3339(&date_fin).unwrap_or_else(|_| panic!("Invalid date & time: {}", date_fin)).date());
        let dates_debut_et_fin =  match (date_debut, date_fin) {
            (None, None) => None,
            (None, Some(date_fin)) => Some((date_fin, date_fin)),
            (Some(date_debut), None) => Some((date_debut, date_debut)),
            (Some(date_debut), Some(date_fin)) => Some((date_debut, date_fin)),
        };
        let mut reunions: Vec<&Reunion> = Vec::new();
        match dates_debut_et_fin {
            None => {
                match acteur_uid {
                    None => {
                        for wrapper in &context.agendas_wrappers {
                            for reunion in &wrapper.reunions.reunions {
                                reunions.push(&reunion);
                            }
                        }
                    },
                    Some(acteur_uid) => {
                        let acteur = context.get_acteur_at_uid(&acteur_uid);
                        if let Some(acteur) = acteur {
                            let organes = acteur.organes(context, None);
                            let organes_uids: Vec<&str> = organes.iter().map(|organe| organe.uid()).collect();
                            for wrapper in &context.agendas_wrappers {
                                'reunions_sans_date: for reunion in &wrapper.reunions.reunions {
                                    if let Some(ref demandeurs) = reunion.demandeurs {
                                        for demandeur in &demandeurs.acteurs {
                                            if demandeur.acteur_ref == acteur_uid {
                                                reunions.push(&reunion);
                                                continue 'reunions_sans_date;
                                            }
                                        }
                                    }
                                    if let Some(ref organe_reuni_ref) = reunion.organe_reuni_ref {
                                        for organe_uid in &organes_uids {
                                            if organe_uid == organe_reuni_ref {
                                                reunions.push(&reunion);
                                                continue 'reunions_sans_date;
                                            }
                                        }
                                    }
                                    if let Some(ref participants) = reunion.participants {
                                        if let Some(ref participants_internes) = participants.participants_internes {
                                            for participant_interne in &participants_internes.participants_internes {
                                                if participant_interne.acteur_ref == acteur_uid {
                                                    reunions.push(&reunion);
                                                    continue 'reunions_sans_date;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    },
                }
                reunions.sort_unstable_by_key(|reunion| reunion.timestamp_debut);
            },
            Some((date_debut, date_fin)) => {
                match acteur_uid {
                    None => {
                        let mut date = date_debut;
                        while date <= date_fin {
                            let date_str = format!("{}", date.format("%Y-%m-%d"));
                            if let Some(reunions_at_date) = context.get_reunions_at_date(&date_str) {
                                reunions.extend(reunions_at_date);
                            }
                            date = date.succ();
                        }
                    },
                    Some(acteur_uid) => {
                        let acteur = context.get_acteur_at_uid(&acteur_uid);
                        if let Some(acteur) = acteur {
                            let mut date = date_debut;
                            while date <= date_fin {
                                let date_str = format!("{}", date.format("%Y-%m-%d"));
                                if let Some(reunions_at_date) = context.get_reunions_at_date(&date_str) {
                                    let organes = acteur.organes(context, Some(&date_str));
                                    let organes_uids: Vec<&str> = organes.iter().map(|organe| organe.uid()).collect();
                                    'reunions_avec_date: for reunion in reunions_at_date {
                                        if let Some(ref demandeurs) = reunion.demandeurs {
                                            for demandeur in &demandeurs.acteurs {
                                                if demandeur.acteur_ref == acteur_uid {
                                                    reunions.push(&reunion);
                                                    continue 'reunions_avec_date;
                                                }
                                            }
                                        }
                                        if let Some(ref organe_reuni_ref) = reunion.organe_reuni_ref {
                                            for organe_uid in &organes_uids {
                                                if organe_uid == organe_reuni_ref {
                                                    reunions.push(&reunion);
                                                    continue 'reunions_avec_date;
                                                }
                                            }
                                        }
                                        if let Some(ref participants) = reunion.participants {
                                            if let Some(ref participants_internes) = participants.participants_internes {
                                                for participant_interne in &participants_internes.participants_internes {
                                                    if participant_interne.acteur_ref == acteur_uid {
                                                        reunions.push(&reunion);
                                                        continue 'reunions_avec_date;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                date = date.succ();
                            }
                        }
                    },
                }
            },
        }
        Ok(reunions)
    }

    field agenda_dossier_legislatif(&executor, id: String) -> FieldResult<Vec<&Reunion>> {
        // let mut reunions: Vec<&Reunion> = Vec::new();
        // for wrapper in &executor.context().agendas_wrappers {
        //     for reunion in &wrapper.reunions.reunions {
        //         if let Some(ref odj) = reunion.odj {
        //             if let Some(ref points_odj) = odj.points_odj {
        //                 for point_odj in &points_odj.points_odj {
        //                     if let Some(ref dossiers_legislatifs_refs) = point_odj.dossiers_legislatifs_refs() {
        //                         for dossier_ref in &dossiers_legislatifs_refs.dossiers_refs {
        //                             if dossier_ref == &id {
        //                                 reunions.push(&reunion);
        //                             }
        //                         }
        //                     }
        //                 }
        //             }
        //         }
        //     }
        // }
        // Ok(reunions)
        let mut reunions: Vec<&Reunion> = Vec::new();
        if let Some(reunions_for_id) = executor.context().get_reunions_at_dossier_uid(&id) {
            // for reunion in reunions {
            //     found_reunions.push(reunion);
            // }
            reunions.extend(reunions_for_id)
        }
        Ok(reunions)
    }

    field amendement(&executor, id: String as "Identifiant (uid) de l'amendement recherché") -> FieldResult<Option<&Amendement>> as "Rechercher un amendement par son identifiant" {
        Ok(executor.context().get_amendement_at_uid(&id))
    }

    field amendements(&executor, since: Option<String> as "Date de départ de la recherche des amendements, au format YYYY-MM-DD", until: Option<String> as "Date limite de la recherche des amendements, au format YYYY-MM-DD") -> FieldResult<Vec<&Amendement>> as "Obtenir la liste des amendements, optionnellement en fournissant une date de départ et une date de fin pour la recherche."{
        let mut amendements: Vec<&Amendement> = Vec::new();
        let parse_from_str = NaiveDate::parse_from_str;

        match since {
            Some(since_ref) => {

                let tmp_since_ref = parse_from_str(&since_ref,"%Y-%m-%d").unwrap();
                let timestamp_since_ref = NaiveDateTime::timestamp(&tmp_since_ref.and_hms(00,00,00));

                match until {

                    Some(until_ref) => {

                        let tmp_until_ref = parse_from_str(&until_ref,"%Y-%m-%d").unwrap();
                        let timestamp_until_ref = NaiveDateTime::timestamp(&tmp_until_ref.and_hms(00,00,00));

                        for wrapper in &executor.context().amendements_wrappers{
                            for texte_leg in &wrapper.textes_et_amendements{
                                for txtleg in &texte_leg.texte_leg{
                                    for amds in &txtleg.amendements{
                                        for amd in &amds.amendements{
                                            match &amd.date_depot {
                                                None => (),
                                                Some(ref date_depot) => {
                                                    let tmp_amd = parse_from_str(&date_depot, "%Y-%m-%d").unwrap();
                                                    let timestamp_amd = NaiveDateTime::timestamp(&tmp_amd.and_hms(00,00,00));
                                                    if timestamp_since_ref <= timestamp_amd && timestamp_amd <= timestamp_until_ref {
                                                        amendements.push(&amd);
                                                    }
                                                },
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    },
                    None => {
                        for wrapper in &executor.context().amendements_wrappers{
                            for texte_leg in &wrapper.textes_et_amendements{
                                for txtleg in &texte_leg.texte_leg{
                                    for amds in &txtleg.amendements{
                                        for amd in &amds.amendements{
                                            match &amd.date_depot {
                                                None => (),
                                                Some(ref date_depot) => {
                                                    let tmp_amd = parse_from_str(&date_depot,"%Y-%m-%d").unwrap();
                                                    let timestamp_amd = NaiveDateTime::timestamp(&tmp_amd.and_hms(00,00,00));
                                                    if timestamp_since_ref <= timestamp_amd {
                                                        amendements.push(&amd);
                                                    }
                                                },
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            None => {
                match until {
                    Some(until_ref) => {

                        let tmp_until_ref = parse_from_str(&until_ref,"%Y-%m-%d").unwrap();
                        let timestamp_until_ref = NaiveDateTime::timestamp(&tmp_until_ref.and_hms(00,00,00));

                        for wrapper in &executor.context().amendements_wrappers{
                            for texte_leg in &wrapper.textes_et_amendements{
                                for txtleg in &texte_leg.texte_leg{
                                    for amds in &txtleg.amendements{
                                        for amd in &amds.amendements{
                                            match &amd.date_depot {
                                                None => (),
                                                Some(ref date_depot) => {
                                                    let tmp_amd = parse_from_str(&date_depot,"%Y-%m-%d").unwrap();
                                                    let timestamp_amd = NaiveDateTime::timestamp(&tmp_amd.and_hms(00,00,00));
                                                    if timestamp_until_ref >= timestamp_amd {
                                                        amendements.push(&amd);
                                                    }
                                                },
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    },
                    None => {
                        for wrapper in &executor.context().amendements_wrappers{
                            for texte_leg in &wrapper.textes_et_amendements{
                                for txtleg in &texte_leg.texte_leg{
                                    for amds in &txtleg.amendements{
                                        for amd in &amds.amendements{
                                            amendements.push(&amd);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        Ok(amendements)
    }

    field amendements_texte_legislatif(
        &executor,
        uid: String as "Identifiant du texte législatif recherché",
    ) -> FieldResult<Option<&TexteLeg>> as "Rechercher les amendements d'un texte législatif par son identifiant" {
        Ok(executor.context().get_texte_leg_at_uid(&uid))
    }

    field deputes(&executor, date: Option<String>) -> FieldResult<Vec<&Acteur>> {
        let date = match date {
            None => None,
            Some(ref date) => Some(date.as_ref()),
        };
        let mut deputes: Vec<&Acteur> = Vec::new();

        for acteur in executor.context().acteur_by_uid
            .values()
            .map(|acteur| unsafe { &*(*acteur) })
        {
            for mandat in acteur.mandats(date) {
                if let Mandat::MandatParlementaire(mandat_parlementaire) = mandat {
                    if mandat_parlementaire.type_organe == CodeTypeOrgane::Assemblee {
                        deputes.push(&acteur);
                        break; // A deputé can have several mandats, for example "membre" & "secrétaire".
                    }
                }
            }
        }
        Ok(deputes)
    }

    field document(&executor, uid: String) -> FieldResult<Vec<&Document>> {
        match executor.context().get_document_at_uid(&uid) {
            None => Ok(vec![]),
            Some(ref document) => {
                // Generate a flat vector from document and its sub-documents (aka divisions),
                // because a tree is not adapted to GraphQL.
                Ok(document.flatten_document())
            },
        }
    }

    field documents(&executor) -> FieldResult<Vec<&Document>> {
        let mut documents: Vec<&Document> = Vec::new();
        for wrapper in &executor.context().dossiers_legislatifs_wrappers {
            for document in &wrapper.export.textes_legislatifs.documents {
                // TODO: Document should be flatten?
                documents.push(&document);
            }
        }
        Ok(documents)
    }

    field dossier_parlementaire(&executor, uid: String) -> FieldResult<Option<&DossierParlementaire>> {
        Ok(executor.context().get_dossier_parlementaire_at_uid(&uid))
    }

    field dossiers_parlementaires(&executor, legislature: i32) -> FieldResult<Vec<&DossierParlementaire>> {
        assert_eq!(legislature, 15);
        let mut dossiers_parlementaires: Vec<&DossierParlementaire> = Vec::new();
        for wrapper in &executor.context().dossiers_legislatifs_wrappers {
            for dossier in &wrapper.export.dossiers_legislatifs.dossiers {
                dossiers_parlementaires.push(&dossier.dossier_parlementaire);
            }
        }
        Ok(dossiers_parlementaires)
    }

    field dossiers_parlementaires_futurs(&executor, date: String) -> FieldResult<Vec<&DossierParlementaire>> {
        let date_time = DateTime::parse_from_rfc3339(&date).unwrap_or_else(|_| panic!("Invalid date & time: {}", date));
        let date = date_time.date();
        let mut dossiers_parlementaires: Vec<&DossierParlementaire> = Vec::new();
        for wrapper in &executor.context().dossiers_legislatifs_wrappers {
            for dossier in &wrapper.export.dossiers_legislatifs.dossiers {
                let dossier_parlementaire = &dossier.dossier_parlementaire;
                match dossier_parlementaire.distance_min_date_actes_legislatifs_futurs(date) {
                    None => (),
                    Some(distance) => dossiers_parlementaires.push(dossier_parlementaire),
                }
            }
        }
        Ok(dossiers_parlementaires)
    }

    field dossiers_parlementaires_par_cles(&executor, segment: Option<String>, signet_senat: Option<String>) -> FieldResult<Vec<&DossierParlementaire>> {
        let context = executor.context();
        let mut dossiers_parlementaires: Vec<&DossierParlementaire> = Vec::new();
        match segment {
            None => (),
            Some(ref segment) => {
                match context.get_dossier_parlementaire_at_segment(segment) {
                    None => (),
                    Some(ref dossier_parlementaire) => {
                        if !dossiers_parlementaires.contains(dossier_parlementaire) {
                            dossiers_parlementaires.push(dossier_parlementaire);
                        }
                    },
                }
            },
        }
        match signet_senat {
            None => (),
            Some(ref signet_senat) => {
                match context.get_dossier_parlementaire_at_signet_senat(signet_senat) {
                    None => (),
                    Some(ref dossier_parlementaire) => {
                        if !dossiers_parlementaires.contains(dossier_parlementaire) {
                            dossiers_parlementaires.push(dossier_parlementaire);
                        }
                    },
                }
            },
        }
        Ok(dossiers_parlementaires)
    }

    field dossiers_parlementaires_prochains(&executor, date: String) -> FieldResult<Vec<&DossierParlementaire>> {
        let date_time = DateTime::parse_from_rfc3339(&date).unwrap_or_else(|_| panic!("Invalid date & time: {}", date));
        let date = date_time.date();
        let mut dossiers_parlementaires: Vec<&DossierParlementaire> = Vec::new();
        for wrapper in &executor.context().dossiers_legislatifs_wrappers {
            for dossier in &wrapper.export.dossiers_legislatifs.dossiers {
                let dossier_parlementaire = &dossier.dossier_parlementaire;
                match dossier_parlementaire.distance_min_date_actes_legislatifs_futurs(date) {
                    Some(distance) if distance <= 100 => dossiers_parlementaires.push(dossier_parlementaire),
                    _ => (),
                }
            }
        }
        Ok(dossiers_parlementaires)
    }

    field dossiers_parlementaires_recents(&executor, date: String) -> FieldResult<Vec<&DossierParlementaire>> {
        let date_time = DateTime::parse_from_rfc3339(&date).unwrap_or_else(|_| panic!("Invalid date & time: {}", date));
        let date = date_time.date();
        let mut dossiers_parlementaires: Vec<&DossierParlementaire> = Vec::new();
        for wrapper in &executor.context().dossiers_legislatifs_wrappers {
            for dossier in &wrapper.export.dossiers_legislatifs.dossiers {
                let dossier_parlementaire = &dossier.dossier_parlementaire;
                match dossier_parlementaire.distance_min_date_actes_legislatifs_passes(date) {
                    Some(distance) if distance <= 100 => dossiers_parlementaires.push(dossier_parlementaire),
                    _ => (),
                }
            }
        }
        Ok(dossiers_parlementaires)
    }

    field fonctions_organes(&executor) -> FieldResult<&[&FonctionOrgane]> {
        Ok(FONCTIONS_ORGANES)
    }

    field legislature(&executor) -> FieldResult<i32> {
        Ok(15)
    }

    field organe(&executor, uid: String) -> FieldResult<Option<&Organe>> {
        Ok(executor.context().get_organe_at_uid(&uid))
    }

    field organes(&executor) -> FieldResult<Vec<&Organe>> {
        let organes: Vec<&Organe> = executor.context().organe_by_uid
            .values()
            .map(|organe| unsafe { &*(*organe) })
            .collect();
        Ok(organes)
    }

    field reunion(&executor, uid: String) -> FieldResult<Option<&Reunion>> {
        Ok(executor.context().get_reunion_at_uid(&uid))
    }

    field scrutin(&executor, uid: String) -> FieldResult<Option<&Scrutin>> {
        Ok(executor.context().get_scrutin_at_uid(&uid))
    }

    field scrutins(&executor) -> FieldResult<Vec<&Scrutin>> {
        let mut scrutins: Vec<&Scrutin> = Vec::new();
        for wrapper in &executor.context().scrutins_wrappers {
            for scrutin in &wrapper.scrutins.scrutins {
                scrutins.push(&scrutin);
            }
        }
        Ok(scrutins)
    }

    field statut(&executor) -> FieldResult<&Statut> {
        Ok(&executor.context().statut)
    }

    field textes_et_amendements(&executor) -> FieldResult<Vec<&TextesEtAmendements>> {
        let mut textes_et_amendements: Vec<&TextesEtAmendements> = Vec::new();
        for wrapper in &executor.context().amendements_wrappers{
            for texte_leg in &wrapper.textes_et_amendements {
                textes_et_amendements.push(&texte_leg);
            }
        }
        Ok(textes_et_amendements)
    }
});

type Schema = RootNode<'static, Query, EmptyMutation<Context>>;

#[get("/")]
fn graphiql() -> content::Html<String> {
    juniper_rocket::graphiql_source("/graphql")
}

#[get("/assets/<file_path..>")]
fn assets(file_path: PathBuf, data_dir: State<PathBuf>) -> Option<NamedFile> {
    NamedFile::open(data_dir.join(&file_path)).ok()
}

#[get("/graphql?<request>")]
fn get_graphql_handler(
    context: State<Context>,
    request: juniper_rocket::GraphQLRequest,
    schema: State<Schema>,
) -> juniper_rocket::GraphQLResponse {
    request.execute(&schema, &context)
}

#[post("/graphql", data = "<request>")]
fn post_graphql_handler(
    context: State<Context>,
    request: juniper_rocket::GraphQLRequest,
    schema: State<Schema>,
) -> juniper_rocket::GraphQLResponse {
    request.execute(&schema, &context)
}

fn main() {
    let matches = App::new("Tricoteuses API Assemblée")
        .version(env!("CARGO_PKG_VERSION"))
        .author("Emmanuel Raviart <emmanuel@raviart.com>")
        .about("GraphQL API to access open data of French Assemblée nationale")
        .arg(
            Arg::with_name("config")
                .short("c")
                .long("config")
                .value_name("FILE")
                .help("Sets a custom config file")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("test")
                .short("t")
                .long("test")
                .help("Only test data parsing; don't launch web server"),
        )
        .arg(
            Arg::with_name("v")
                .short("v")
                .multiple(true)
                .help("Sets the level of verbosity"),
        )
        .get_matches();

    let config_file_path = Path::new(matches.value_of("config").unwrap_or("../Config.toml"));
    let config = config::load(config_file_path);

    let verbosity = match matches.occurrences_of("v") {
        0 => Verbosity::Verbosity0,
        1 => Verbosity::Verbosity1,
        2 | _ => Verbosity::Verbosity2,
    };

    let config_dir = config_file_path.parent().unwrap();
    let data_dir = config_dir.join(&config.data.dir);

    let context = data::load(&config, &config_dir, &ALL_DATASETS, &verbosity);

    if matches.is_present("test") {
        // Don't launch web server in test mode.
        return;
    }

    // Start Rocket web server.
    let (allowed_origins, failed_origins) = AllowedOrigins::some(
        config
            .api
            .allowed_origins
            .iter()
            .map(String::as_ref)
            .collect::<Vec<&str>>()
            .as_slice(),
    );
    assert!(failed_origins.is_empty());
    let cors = rocket_cors::Cors {
        allowed_origins,
        allowed_methods: vec![Method::Get, Method::Post]
            .into_iter()
            .map(From::from)
            .collect(),
        allow_credentials: true,
        ..Default::default()
    };
    rocket::ignite()
        .attach(cors)
        .manage(context)
        .manage(data_dir)
        .manage(Schema::new(Query, EmptyMutation::<Context>::new()))
        .mount(
            "/",
            routes![assets, get_graphql_handler, graphiql, post_graphql_handler],
        )
        .launch();
}
